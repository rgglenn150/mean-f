process.env.NODE_ENV = 'test';

let UserController = require('../api/user/user.controller');
let AuthController = require('../auth/auth.controller');
let User = require('../api/user/user.model');
let Bluebird = require('bluebird');

let testUser = {
    email: "testinguser@theseanthomas.com",
    password: "password123",
    firstName: "Sean",
    lastName: "Thomas"
};



exports.getToken = AuthController.getToken;  //not an async function, pass in user

exports.asynctest = function(){ }

exports.getTestingUserData = function(){
    return testUser;
}

exports.getUser = async function(){
    return User.findOne({ email: testUser.email });
    // return User.updateOne({ email: testUser.email }, { $set: testUser }, { upsert: true});
}


/* These functions are run once every time "npm test" is ran */
exports.seedTestingData = async function(){
    await User.deleteMany({});
    await UserController.registerUser(testUser);
}

exports.removeTestingData = function(){
    return User.deleteMany({});
}