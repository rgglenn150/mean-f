//tests
process.env.NODE_ENV = 'test';
let config = require('../config/environment');
let TestService = require("./test.service");
let ConnectController = require('../utils/connect');
let app = require('../lib/server')(config);

    before(async () => {
        console.log('calling before all');
        try {
            await ConnectController.connect();
            await TestService.seedTestingData();
        } catch (e) {
            console.log(e);
        }

        //expect(result).to.be.null;
        /*await loadFixture('user');
        user = await User.findOne({email: authUser.email});
        expect(user).to.not.be.null;*/
    });

    after(async () => {
        try {
            await TestService.removeTestingData();
            await ConnectController.disconnect();
            console.log('disconnected');
        } catch (e) {
            console.log(e);
        } finally {
            app.close();
        }
    });


    //------------------ Unit tests ---------------


    //----------------- USER ----------------------
    require('../api/user/user.spec')(app);

    //----------------- USER FRIEND ---------------
    require('../api/user/user-friend.spec')(app);

    //----------------- USER SUBACCOUNT ---------------
    require('../api/user/user-subaccount/user-subaccount.spec')(app);


    //----------------- AUTH ----------------------
    require('../auth/auth.spec')(app);

    //----------------- Campaign ------------------
    require('../api/campaign/campaign.spec')(app);

    //----------------- Event ---------------------
    require('../api/event/event/event.spec')(app);
    require('../api/event/event-participant/event-participant.spec')(app);
    require('../api/event/event-promoter/event-promoter.spec')(app);
    require('../api/event/event-invite-pending/event-invite-pending.spec')(app);



    //----------------- BADGES --------------------
    require('../api/badge/badge.spec')(app);

    //----------------- NOTIFICATIONS -------------
    require('../api/notification/notification.spec')(app);
