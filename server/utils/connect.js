var config = require('../config/environment');
const mongoose = require('mongoose');
var Bluebird = require('bluebird');


//exports.connectMongo = connectMongo;
//exports.connectMongoApp = connectToMongoApp; //We could of used mongoose here but the relay controller code was using the mongo client instead of mongoose so I have to connec tthis way

function connect(options) {
    options = options || config.mongo.options || {};
    return mongoose.connect(config.mongo.uri, options).then(() => {
        console.log('Finished Connecting to mongodbs on ' + process.env.NODE_ENV);
    }).catch((error) => {
        console.log('ServerError ', error, error.stack);
        return setTimeout(connect, 1000);
    });
}

function disconnect() {
    //return Bluebird.fromCallback(function(cb) {
        //return mongoose.disconnect(cb);
    return mongoose.disconnect();
    //});
}

module.exports = {
    connect: connect,
    disconnect: disconnect
}

/*

function connectMongo(options) {
  options = options || {};
  return Bluebird.all([
      connectToMongoRelay()
    ])
    .then(function() {
      console.log('Finished Connecting to dbs');
    })
    .catch(function(error) {
      console.log('ServerError ', error, error.stack);
      return setTimeout(connectMongo, 1000);
    });
}

function connectToMongoRelay() {
  return Bluebird.fromCallback(function(cb) {
    MongoClient.connect(config.mongoRelay.uri, cb);
  }).then(function(db) {
    config.mongoRelay.db = db;
  });
}

function connectToMongoApp() {
  return Bluebird.fromCallback(function(cb) {
    MongoClient.connect(config.mongo.uri, cb);
  }).then(function(db) {
    config.dbApp = db;
  });
}

function connectToRabbitMq(){
  return Bluebird.fromCallback(function(cb) {
    var parsedurl = url.parse(config.rabbitMqUrl);
    amqp.connect(config.rabbitMqUrl, { servername: parsedurl.hostname }, cb);
  }).then(function(conn){
    config.rabbitMq = conn;

  });
}



function disconnectMongoRelay() {
  return Bluebird.fromCallback(function(cb) {
    config.mongoRelay.db.close(cb);
  });
}

function disconnectRabbitMq() {
  return Bluebird.fromCallback(function(cb) {
    config.rabbitMq.close(cb);
  });
}*/

