var config = require('./environment');
var bodyParser = require('body-parser');

module.exports = (app) => {


  //cors support
  app.use((req, res, next) => {
      res.header("Access-Control-Allow-Origin","*");
      res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
      next();
  });

	//app.engine('html', require('ejs').renderFile);
  //app.set('view engine', 'html');
  app.use(bodyParser.json({limit: '500kb'}));

  if ('production' === config.env) {
    app.use(function(req, res, next) {
      if (req.headers['x-forwarded-proto'] != 'https') {
        res.redirect('https://' + req.get('host') + req.url);
      } else {
        next();
      }
    });
  }else {
  	//Do development stuff
  }

 

}