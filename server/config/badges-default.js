//Default badges are seeded

module.exports = {
    badgesDefault: [{
        name: 'Star Strong',
        svg: 'Badge1.svg',
        bgColor: '#A0C2BB',
        text: {
            color: '#FFF',
            font: 'Oswald',
            width: 150,
            height: 150,
            fontSize: 22,
            position: {x: 125, y: 100},
            name: 'Badge Name'
        },
        image: {
            path: '/assets/img/badges/Badge1.png',
            width: 150,
            height: 150,
            position: {x: 100, y: 50}
        }
    },{
        name: 'Super Shield',
        svg: 'Badge2.svg',
        bgColor: '#9BAAB9',
        text: {
            color: '#FFF',
            font: 'Oswald',
            width: 150,
            height: 150,
            fontSize: 22,
            position: {x: 125, y: 100},
            name: 'Badge Name'
        },
        image: {
            path: '/assets/img/badges/Badge3.png',
            width: 150,
            height: 146,
            position: {x: 100, y: 50}
        }
    },{
        name: 'Stellar Stripe',
        svg: 'Badge4.svg',
        bgColor: '#6E6F71',
        text: {
            color: '#FFF',
            font: 'Oswald',
            width: 150,
            height: 150,
            fontSize: 22,
            position: {x: 125, y: 100},
            name: 'Badge Name'
        },
        image: {
            path: '/assets/img/badges/Badge6.png',
            width: 279,
            height: 156,
            position: {x: 100, y: 50}
        }
    },{
        name: 'Gambling Goodness',
        svg: 'Badge5.svg',
        bgColor: '#495867',
        text: {
            color: '#485868',
            font: 'Oswald',
            width: 150,
            height: 150,
            fontSize: 22,
            position: { x: 125, y: 100 },
            name: 'Badge Name'
        },
        image: {
            path: '/assets/img/badges/Badge7.png',
            width: 150,
            height: 150,
            position: { x: 100, y: 50 }
        }
    },{
        name: 'Seal Of Gilgamesh',
        svg: 'Badge6.svg',
        bgColor: '#495867',
        text: {
            color: '#485868',
            font: 'Oswald',
            width: 150,
            height: 150,
            fontSize: 22,
            position: { x: 125, y: 100 },
            name: 'Badge Name'
        },
        image: {
            path: '/assets/img/badges/Badge8.png',
            width: 150,
            height: 150,
            position: { x: 100, y: 50 }
        }
    },{
        name: 'Fantastic Factory', //missing
        svg: 'Badge9.svg',
        bgColor: '#F2C66D',
        text: {
            color: '#FFF',
            font: 'Oswald',
            width: 150,
            height: 150,
            fontSize: 22,
            position: { x: 125, y: 100 },
            name: 'Badge Name'
        },
        image: {
            path: '/assets/img/badges/Badge9.png',
            width: 150,
            height: 150,
            position: { x: 100, y: 50 }
        }
    },{
        name: 'Wild Wilderness',
        svg: 'Badge7.svg',
        bgColor: '#FBE6E1',
        text: {
            color: '#FFF',
            font: 'Oswald',
            width: 150,
            height: 150,
            fontSize: 22,
            position: { x: 125, y: 100 },
            name: 'Badge Name'
        },
        image: {
            path: '/assets/img/badges/Badge10.png',
            width: 150,
            height: 150,
            position: { x: 100, y: 50 }
        }
    },{
        name: 'Ruling Ringer',
        svg: 'Badge8.svg',
        bgColor: '#F3A471',
        text: {
            color: '#FFF',
            font: 'Oswald',
            width: 150,
            height: 150,
            fontSize: 22,
            position: { x: 125, y: 100 },
            name: 'Badge Name'
        },
        image: {
            path: '/assets/img/badges/Badge11.png',
            width: 150,
            height: 150,
            position: { x: 100, y: 50 }
        }
    },{
        name: 'Wings Of Liberty',
        svg: 'Badge9.svg',
        bgColor: '#A0C2BB',
        text: {
            color: '#FFF',
            font: 'Oswald',
            width: 150,
            height: 150,
            fontSize: 22,
            position: { x: 125, y: 100 },
            name: 'Badge Name'
        },
        image: {
            path: '/assets/img/badges/Badge12.png',
            width: 150,
            height: 150,
            position: { x: 100, y: 50 }
        }
    },{
        name: 'MGM Grand',
        svg: 'Badge10.svg',
        bgColor: '#495867',
        text: {
            color: '#FFF',
            font: 'Oswald',
            width: 150,
            height: 150,
            fontSize: 22,
            position: { x: 125, y: 100 },
            name: 'Badge Name'
        },
        image: {
            path: '/assets/img/badges/Badge13.png',
            width: 150,
            height: 150,
            position: { x: 100, y: 50 }
        }
    },{
        name: 'Wings Of Liberty',
        svg: 'Badge1.svg',
        bgColor: '#A0C2BB',
        text: {
            color: '#FFF',
            font: 'Oswald',
            width: 150,
            height: 150,
            fontSize: 22,
            position: { x: 125, y: 100 },
            name: 'Badge Name'
        },
        image: {
            path: '/assets/img/badges/Badge1.png',
            width: 150,
            height: 150,
            position: { x: 100, y: 50 }
        }
    },{
        name: 'OCTO-mazing', //missing
        svg: 'Badge14.svg',
        bgColor: '#F2C66D',
        text: {
            color: '#FFF',
            font: 'Oswald',
            width: 150,
            height: 150,
            fontSize: 22,
            position: { x: 125, y: 100 },
            name: 'Badge Name'
        },
        image: {
            path: '/assets/img/badges/Badge14.png',
            width: 150,
            height: 150,
            position: { x: 100, y: 50 }
        }
    },{
        name: 'Glory Days',
        svg: 'Badge3.svg',
        bgColor: '#9BAAB9',
        text: {
            color: '#FFF',
            font: 'Oswald',
            width: 150,
            height: 150,
            fontSize: 22,
            position: { x: 125, y: 100 },
            name: 'Badge Name'
        },
        image: {
            path: '/assets/img/badges/Badge15.png',
            width: 150,
            height: 150,
            position: { x: 100, y: 50 }
        }
    },{
        name: 'Big Bling',
        svg: 'Badge16.svg',
        bgColor: '#F3A471',
        text: {
            color: '#FFF',
            font: 'Oswald',
            width: 150,
            height: 150,
            fontSize: 22,
            position: { x: 125, y: 100 },
            name: 'Badge Name'
        },
        image: {
            path: '/assets/img/badges/Badge16.png',
            width: 150,
            height: 150,
            position: { x: 100, y: 50 }
        }
    }]
}