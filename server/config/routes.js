/**
 * Main application routes
 */

'use strict';

//var errors = require('./components/errors');

module.exports = (app) => {

  app.use('/api/user', require('../api/user'));

  app.use('/api/campaign', require('../api/campaign'));

  app.use('/api/event', require('../api/event'));

  app.use('/api/badge', require('../api/badge'));

  app.use('/api/notification', require('../api/notification'));

  app.use('/api/email', require('../api/email'));

  app.use('/api/organization',require('../api/organization'));

  //app.use('/api/event', require('../api/event'));

  app.use('/auth', require('../auth'));

  app.use('/sub-account',require('../api/user/user-subaccount'));





  //app.use('/auth', require('../auth')/;

  // All undefined asset or api routes should return a 404
  //app.route('/:url(api|auth|components|app|bower_components|assets)/*')
  // .get(errors[404]);

  // All other routes should redirect to the index.html
  /*app.route('/*')
    .get((req, res) =>{
      res.sendfile(app.get('appPath') + '/index.html');
    });*/
};
