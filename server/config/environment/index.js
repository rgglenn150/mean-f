'use strict';

var path = require('path');
var _ = require('lodash');

// All configurations will extend these options
// ============================================
var all = {
  env: process.env.NODE_ENV || 'development',

  // Root path of server
  root: path.normalize(__dirname + '/..'),

  // Should we populate the DB with sample data?
  seedDB: true,

  // Secret for session, you will want to change this and make it an environment variable
  sessionSecret: '**fjdkfjds_2943823jfkdkd_dsDFKFKFK',
  resetTokenExpiresMinutes: '600', //for the reset token when reseting your password
  expiresInMinutes: '10080',
  companyEmail: 'bill@bringonthegood.com',
  companyPassword: 'seanisthegreatest',
  systemEmail: "admin@bringonthegood.com",
  systemName: 'Whoa | Word Of Action',
  developerEmail: "sean.alan.thomas@gmail.com",
  developerFirstName: "Sean",
  developerLastName: "Thomas",
  companyName: "Bring On The Good",
  sendgridApi: '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',
  userRoles: ['guest', 'user', 'admin', 'superadmin'],
  accessKeyId: 'AKIAJNUAAQ4UDZ74AY2A',
  secretAccessKey: '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',
  bucket: 'badgekeep-web-api'
};


module.exports = _.merge(
  all,
  require('./' + process.env.NODE_ENV + '.js') || {},
  require('../badges-default.js')
    );
