'use strict';

// Production specific configuration
// =================================
module.exports = {
    protocal: 'https://',
    host: 'badges-production.herokuapp.com',
    ip: process.env.IP || undefined, // Server IP
    port: process.env.PORT || 8080,
    mongo:{
      uri: 'mongodb://badgekeep:SuperDuper$$$2018@botg-shard-00-00-r4ziq.mongodb.net:27017,botg-shard-00-01-r4ziq.mongodb.net:27017,botg-shard-00-02-r4ziq.mongodb.net:27017/production?ssl=true&replicaSet=BOTG-shard-0&authSource=admin&retryWrites=true',
      options: {
          useNewUrlParser: true,
          autoIndex: true, // Don't build indexes
          useCreateIndex: true,
          reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
          reconnectInterval: 500, // Reconnect every 500ms
          poolSize: 10, // Maintain up to 10 socket connections
          // If not connected, return errors immediately rather than waiting for reconnect
          bufferMaxEntries: 0,
          connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
          socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
          family: 4 // Use IPv4, skip trying IPv6
      }
    },
    facebookAppId: '914525758657957',
    linkedinClientId: '78hopnrllco3at',
    linkedinSecret: 'IBO87U73oiX6zVQd',
    linkedinRedirectUrl: 'https://badgesapp.herokuapp.com/linkedin',
    frontEndApp: 'https://badgesapp.herokuapp.com'
};


