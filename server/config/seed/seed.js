let User = require('../../api/user/user.model');
let BadgeDefault = require('../../api/badge/badge-default.model');
let fs = require('fs');
let reflectMap = require('../../api/utils/reflect-map');

function seedUser(config) {
    return User.findOne({ email: config.developerEmail }).then((user) => {
        if (!user) {
            let user = {
                email: config.developerEmail,
                password: config.companyPassword,
                firstName: config.developerFirstName,
                lastName: config.developerLastName
            };
            return User.create(user);
        }
    });
}

async function readSvg(path){
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf8', function (err, contents) {
            if(err) reject(err);
            else
                resolve(contents);
        });
    });
}

async function seedBadgesDefault(config){
    return BadgeDefault.countDocuments().then((count)=>{
        if(count < 1) {
            //let promises = [];

            return reflectMap(config.badgesDefault, (badge) => {
                return BadgeDefault.create(badge);
                //return readSvg( __dirname + '/Badges_SVG/'+ badge.svg).then((svgCode) => {
                    //badge.svg = svgCode;
                    //return promises.push(BadgeDefault.create(badge));
                //});
                //promises.push(BadgeDefault.create(badge));
            });
            //return Promise.all(promises);
        }
    });
}

function seedHandler(config) {
    if(config.seedDB) {
        return Promise.all([
            seedUser(config),
            seedBadgesDefault(config)
        ]);
    }
}

module.exports = seedHandler;