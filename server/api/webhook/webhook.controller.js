var config = require('../../config/environment');
var Bluebird = require('bluebird');
var request = require('request');

function integrateZapier(payload, zapierHook){
    return new Bluebird(function (resolve, reject) {
        if(config.env === 'production') {
            request.post({json: payload, url: zapierHook}, function (error, response, body) {
                if (!error && error != null) {
                    console.log(error);
                    return reject(body);
                } else {
                    return resolve(body);
                }
            });
        }else{ return resolve({}); }
    });
}


function alertUpdate(message){
    return integrateZapier({
        message: message
    }, config.zapierHookPositionUpdateAlert);
}

function alertCreateLimits(position) {
    return integrateZapier({
        position: position
    }, config.zapierCreateLimits);
}

function closePosition(message) {
    return integrateZapier({
        message: message
    }, config.zapierClosePosition);
}


module.exports = {
    integrateZapier: integrateZapier,
    alertUpdate: alertUpdate,
    alertCreateLimits: alertCreateLimits,
    closePosition: closePosition
}