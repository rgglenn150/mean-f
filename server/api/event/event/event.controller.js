let config = require('../../../config/environment/index');
let Event = require('./event.model');
let utils = require('../../utils/utils');
let BadgeUploadCtrl = require('../../badge/badge-upload.controller');
let moment = require('moment');
let EventParticipant = require('../event-participant/event-participant.model');
let EventPromoter = require('../event-promoter/event-promoter.model');
let EventInvitePending = require('../event-invite-pending/event-invite-pending.model');
let EmailCtrl = require('../../email/email.controller');
let User = require('../../user/user.model');
var validationError = function (res, err) {
    return res.status(422).json(err);
};

exports.index = function (req, res) {
    // TODO if the functionality of a super user is involved, then we can unit test this. Leaving it out for now so we can focus on delivery of routes.
    /*if(AuthMiddleware.hasRoleSerial('superadmin', req.user) && req.query.user){
        filter = req.query.user ? { user: ObjectID(req.query.user) } : {};
    }*/

    let filter = {};
    if (req.query.filter === 'user') {
        filter.user = req.user._id;
    }
    if (req.query.company) {
        filter.company = req.query.company;
    }

 
    return Event.find({
        $or: [{
            user: req.user._id
        }, {
            user: {
                $ne: req.user._id
            },
           
            status: 'active'
        }]
    }).then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};

exports.search = async (req, res) => {

    const searchQuery = req.query.search ? formatSearch(req.query.search, req.query.field) : null;

    let filter = {};
    if (req.query.filter === 'user') {
        filter.user = req.user._id;
    }
    if (req.query.company) {
        filter.company = req.query.company;
    }


    filter = {
        ...filter,
        ...searchQuery
    };
    return Event.find(filter).then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};


let formatSearch = (search, field = null) => {


    let query = {};
    search = search.replace('\t', '');
    if (field === 'name') {
        query.name = {
            $regex: search,
            $options: 'i'
        }
    } else if (field === 'date') {

        query.expireDate = {
            "$gte": new Date(search)
        }

    }
    return query;
}

exports.publicEvents = function (req, res) {
    
    return Event.find({
            makePrivate: false,
            status: 'active'
    }).then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};

/*

    Security function:
    We must only deselect values like hashedPassword, version and salt for security
 */
function checkPopulate(results) {
    let allowedFields = ['badges', 'company', 'user'];
    return results
        .filter(e => allowedFields.includes(e))
        .map(e =>
            e === 'user' ? {
                path: e,
                select: '-__v -salt -hashedPassword'
            } : {
                path: e
            }
        )
        .map(e => {
            return e.path === 'badges' ? {
                path: e.path,
                populate: {
                    path: 'user'
                }
            } : e
        });
}

exports.show = function (req, res) {
    let promise = Event.findOne({
        _id: req.params.id
    });
    let populateValues = req.query.populate ? req.query.populate.split(",") : [];

    if (populateValues.length > 0) {
        populateValues = checkPopulate(populateValues); //Security check
        promise = promise.populate(populateValues);
    }

    return promise.then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};

exports.create = function (req, res) {
    if (!req.body.user) {
        req.body.user = req.user._id;
    }
    return Event.create(req.body).then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};

function _getImage(req, eventId) {
    if (req.body.image && req.body.image.indexOf('data:image/png;base64') !== -1) {
        return BadgeUploadCtrl.uploadEventBadge(req.body.image, eventId);
    } else {
        return Promise.resolve(req.body.image ? req.body.image : '');
    }
}

function getLaunchDate(req) {
    return req.body.launchDate ? moment(req.body.launchDate).startOf('day').toISOString() : null;
}

function getExpireDate(req) {
    return req.body.launchDate && req.body.timeFrame ? moment(req.body.launchDate).add(req.body.timeFrame, 'days').startOf('day').toISOString() : req.body.expireDate;
}

const updateEvent = (req) => {
    req.body.user = req.user._id;
    req.body.makePrivate = !!req.body.makePrivate; //if a string gets passed in or null
    req.body.turnCommentsOff = !!req.body.turnCommentsOff; //if a string gets passed in or null
    req.body.turnEmailNotificationsOff = !!req.body.turnEmailNotificationsOff; //if a string gets passed in or null
    req.body.launchDate = getLaunchDate(req);
    req.body.expireDate = getExpireDate(req);
    req.body.timeFrame = req.body.timeFrame ? req.body.timeFrame : 1;

    return _getImage(req, req.params.id).then((destPath) => {
        req.body.image = destPath;
        return utils.updateObject(Event, req.params.id, req.body);
    });
}


exports.update = function (req, res) {

    req.body.user = req.user._id;
    req.body.makePrivate = !!req.body.makePrivate; //if a string gets passed in or null
    req.body.turnCommentsOff = !!req.body.turnCommentsOff; //if a string gets passed in or null
    req.body.turnEmailNotificationsOff = !!req.body.turnEmailNotificationsOff; //if a string gets passed in or null
    req.body.launchDate = getLaunchDate(req);
    req.body.expireDate = getExpireDate(req);
    req.body.timeFrame = req.body.timeFrame ? req.body.timeFrame : 1;

    return updateEvent(req).then(() => {
        return Event.findOne({
            _id: req.params.id
        })
    }).then((data) => {
        return res.json(data)
    }).catch((e) => {
        console.log(e);

        return res.sendStatus(500);
    });
};

const inviteParticipant = (invite) => {
    return User.findOne({
        _id: invite.participant
    }).then((user) => {
        return EmailCtrl.inviteParticipant(user.email, invite.event)
    });
};

const invitePromoter = (invite) => {
    return User.findOne({
        _id: invite.promoter
    }).then((user) => {
        return EmailCtrl.invitePromoter(user.email, invite.event)
    });
};

/*

makeLive - removes the

 */

exports.makeLive = function (req, res) {
    req.body.status = 'active';
    return updateEvent(req).then(() => {
        return EventParticipant.find({
            event: req.params.id,
            inviteSent: false
        }).then((invites) => {
            let inviteMap = invites.map(p => inviteParticipant(p));
            return Promise.all(inviteMap).then(() => {
                return EventParticipant.updateMany({
                    event: req.params.id
                }, {
                    $set: {
                        inviteSent: true
                    }
                });
            });
        });
    }).then(() => {
        return EventPromoter.find({
            event: req.params.id,
            inviteSent: false
        }).then((invites) => {
            let inviteMap = invites.map(p => invitePromoter(p));
            return Promise.all(inviteMap).then(() => {
                return EventPromoter.updateMany({
                    event: req.params.id
                }, {
                    $set: {
                        inviteSent: true
                    }
                });
            });
        });
    }).then(() => {
        return Event.findOne({
            _id: req.params.id
        });
    }).then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.error(e);
        return res.status(500).json(e);
    });
};


exports.destroy = function (req, res) {
    return Event.deleteOne({
        _id: req.params.id
    }).then(() => {
        return res.sendStatus(200);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
}

exports.updateParticipants = function (eventId) {
    return EventParticipant.estimatedDocumentCount({
        event: eventId
    }).then((count) => {
        return utils.updateObject(Event, eventId, {
            participants: count
        });
    });
}

exports.updatePromoters = function (eventId) {
    return EventPromoter.estimatedDocumentCount({
        event: eventId
    }).then((count) => {
        return utils.updateObject(Event, eventId, {
            promoters: count
        });
    });
}