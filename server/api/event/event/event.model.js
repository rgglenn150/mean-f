'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
let moment = require('moment');
/*

 badgesCached - these are badges that are not yet created but they are saved here until the event is ready to be made live. When the event goes live, each badge is created from the badgesCache, then the _id's are stored in the 'bagdes' field. After, badgesCache can be removed or ignored

 */


var EventSchema = new Schema({
    name: String,
    description: String,
    location: String,
    company: {
        type: Schema.ObjectId,
        ref: 'Company'
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    badges: [{
        type: Schema.ObjectId,
        ref: 'Badge'
    }],
    status: {
        type: String,
        enum: ['active', 'draft', 'inactive'],
        default: 'draft'
    },
    badgesCached: [], // refer above for reference
    recurrence: {
        type: String,
        enum: ['just once', 'daily', 'weekly', 'monthly', 'yearly'],
        default: 'just once'
    },
    category: String,
    timeFrame: Number, //days
    rulesets: String, //TODO will be programmed in detail later
    created: {
        type: Date,
        default: Date.now
    }, //when the event was first created
    makeLiveDate: {
        type: Date
    }, //when the make live date was
    launchDate: {
        type: Date
    }, //when the launchDate for recurring events happen
    expireDate: {
        type: Date
    },
    makePrivate: {
        type: Boolean,
        default: false
    },
    turnCommentsOff: {
        type: Boolean,
        default: false
    },
    turnEmailNotificationsOff: {
        type: Boolean,
        default: false
    },
    image: String,
    participants: {
        type: Number,
        default: 0
    },
    promoters: {
        type: Number,
        default: 0
    },
    settings: {},
});

module.exports = mongoose.model('Event', EventSchema);