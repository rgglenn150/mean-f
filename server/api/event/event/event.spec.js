var config = require('../../../config/environment/index');
var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();
var CampaignController = require('./event.controller');
let TestingService = require('../../../test/test.service')
let request = require('supertest');
let Company = require('../../company/company.model');
//let app = require('../lib/server')(config);
let Event = require('./event.model');

module.exports = function(app) {

    describe('Event Tests', () => {
        let user, token, testingUserData, testingCampaign, testingCampaign2;
        let event = {
            name: "The Greatest Showman Event",
            description: "A testing event"
        };
        let company = {
            name: "The Greatest Showman",
            image: "image.com",
            description: "A testing company"
        };

        before(async () => {
            /*await loadFixture('user');
            user = await User.findOne({email: authUser.email});
            expect(user).to.not.be.null;*/
            user = await TestingService.getUser();
            company.user = user._id;
            expect(user).to.not.be.null;
            token = TestingService.getToken(user);
            expect(token).to.not.be.null;
            testingUserData = TestingService.getTestingUserData();
            company = await Company.create(company);
            testingCampaign = await Event.create(event);
            event.user = user._id;
            await Event.create(event);
            event.company = company._id;
            testingCampaign2 = await Event.create(event);

        });

        after(async () => {
            await Company.deleteMany({});
            await Event.deleteMany({});
        });

        describe('POST /api/event', () => {
            it('Should create a event with a 200 response', async () => {
                const response = await request(app).post('/api/event')
                    .set('Authorization', 'Bearer ' + token)
                    .send(event);
                expect(response).to.have.property('status').equal(200);
            });
        });

        describe('GET /api/event', () => {
            it('It Should get all campaigns by authenticated user', async () => {
                const response = await request(app).get('/api/event?filter=user')
                    .set('Authorization', 'Bearer ' + token)
                //.send();

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('array');
                expect(response.body).to.have.lengthOf(3);
            });

            /* it('Should get all campaigns by company', async () => {
                const response = await request(app).get('/api/event?company=' + company._id)
                    .set('Authorization', 'Bearer ' + token)
                //.send();
                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('array');
                expect(response.body).to.have.lengthOf(2);
            }); */

        });

        describe('GET /api/event/:id', () => {
            it('Should get event by id', async () => {
                const response = await request(app).get('/api/event/' + testingCampaign2._id)
                    .set('Authorization', 'Bearer ' + token)
                //.send();

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('name').equal(event.name);
                expect(response.body).to.have.property('description').equal(event.description);
                expect(response.body).to.have.property('company').equal(company._id.toString());
                expect(response.body).to.have.property('user').equal(user._id.toString());
            });
        });

        describe('PUT /api/event/makeLive/:id', () => {
            it('Should make an event live by id', async () => {
                testingCampaign.description = 'changing attributes';
                const response = await request(app).put('/api/event/makeLive/' + testingCampaign._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send(testingCampaign);


                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('description');
                expect(response.body.description).to.equal('changing attributes');

            });
        });

        describe('PUT /api/event/:id', () => {
            it('Should update a event by id', async () => {
                const response = await request(app).put('/api/event/' + testingCampaign._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({description: 'worked'});

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('description');
                expect(response.body.description).to.equal('worked');

            });
        });

        describe('DEL /api/event/:id', () => {
            it('Should get event by id', async () => {
                const response = await request(app).del('/api/event/' + testingCampaign._id)
                    .set('Authorization', 'Bearer ' + token)
                //.send();

                expect(response).to.have.property('status').equal(200);
            });
        });

    });
};
