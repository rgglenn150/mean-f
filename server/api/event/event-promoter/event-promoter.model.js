'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var EventPromoterSchema = new Schema({
    event: {
        type: Schema.ObjectId,
        ref: 'Event',
        required: true
    },
    promoter: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    accepted: { type: Boolean, default: false},
    inviteSent: { type: Boolean, default: false}
});

EventPromoterSchema.index({ event: 1 });
EventPromoterSchema.index({ event: 1, promoter: 1 }, { unique: true });

module.exports = mongoose.model('EventPromoter', EventPromoterSchema);
