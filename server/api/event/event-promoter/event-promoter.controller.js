let EventPromoter = require('./event-promoter.model');
let EventCtrl = require('../event/event.controller');
let utils = require('../../utils/utils');

function checkPopulate(results){
    let allowedFields = ['promoter'];
    return results
        .filter(e => allowedFields.includes(e))
        .map(e =>
            e === 'participant' ||
            e === 'user' ||
            e === 'promoter' ? { path:e, select: '-__v -salt -hashedPassword'} : {path:e}
        );
}

exports.findByEvent = function(req, res) {
    let promise = EventPromoter.find({ event: req.query.event });

    let populateValues = req.query.populate ? req.query.populate.split(",") : [];
    if(populateValues.length > 0) {
        populateValues = checkPopulate(populateValues); //Security check
        promise = promise.populate(populateValues);
    }
    return promise.then((result) => {
        delete result['__v'];
        //return res.json(result.map(p => p.promoter));
        return res.json(result);
    }).catch((e) => {
        console.log(e)
        return res.status(500).json({message: 'failed', error:e.message})
    });
}

exports.create = function(req, res) {
    return EventPromoter.create({ event: req.body.event, promoter: req.body.promoter })
        .then((result) => {
            delete result['__v'];
            return res.json(result);
        }).then(()=>{
            return EventCtrl.updatePromoters(req.body.event);
        }).catch((e) => {
            return res.status(500).json({message: 'failed', error:e.message})
        });
}

exports.update = function(req, res) {
    let result;
    return utils.updateObject(EventPromoter, req.params.id, req.body)
        .then((data) => {
            result = data;
            delete result['__v'];
            return res.json(result);
        }).then(()=>{
            return EventCtrl.updateParticipants(result.event);
        }).catch((e) => {
            return res.status(500).json({message: 'failed', error:e.message})
        });
}

exports.remove = function(req, res) {
    return EventPromoter.deleteMany({ event: req.query.event, promoter: req.query.promoter })
        .then(() => {
            return res.sendStatus(200);
        }).then(()=>{
            return EventCtrl.updatePromoters(req.query.event);
        }).catch((e) => {
            return res.status(500).json({message: 'failed', error:e.message})
        });
}
