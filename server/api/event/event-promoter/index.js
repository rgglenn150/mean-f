'use strict';

let express = require('express');
let controller = require('./event-promoter.controller');
let auth = require('../../../auth/auth.middleware');

let router = express.Router();

//Event participant and event promoter api calls
router.get('/', controller.findByEvent);
router.post('/', auth.checkAuthenticated, controller.create);
router.put('/:id', auth.checkAuthenticated, controller.update);
router.delete('/', auth.checkAuthenticated, controller.remove);

module.exports = router;