var config = require('../../../config/environment/index');
var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();
let TestingService = require('../../../test/test.service')
let request = require('supertest');
let Company = require('../../company/company.model');
let Event = require('../event/event.model');
let EventPromoter = require('./event-promoter.model');
let User = require('../../user/user.model');

module.exports = function(app) {

    describe('Event Promoter Tests', () => {
        let user, token, testingUserData, testingCampaign, testingCampaign2, participant, promoter, eventPromoter;
        let event = {
            name: "The Greatest Showman Event",
            description: "A testing event"
        };
        let company = {
            name: "The Greatest Showman",
            image: "image.com",
            description: "A testing company"
        };

        before(async () => {
            user = await TestingService.getUser();
            company.user = user._id;
            expect(user).to.not.be.null;
            token = TestingService.getToken(user);
            expect(token).to.not.be.null;
            testingUserData = TestingService.getTestingUserData();
            company = await Company.create(company);
            testingCampaign = await Event.create(event);
            event.user = user._id;
            await Event.create(event);
            event.company = company._id;
            testingCampaign2 = await Event.create(event);
            participant = await User.create({ firstName: 'friend', lastName:'foo', email:'friend32211@bringonthegood.com', password:'dumb' });
            expect(participant).to.not.be.null;
            promoter = await User.create({ firstName: 'friend', lastName:'foo', email:'friend948761@bringonthegood.com', password:'dumb' });
            expect(promoter).to.not.be.null;
        });

        after(async () => {
            await Company.deleteMany({});
            await Event.deleteMany({});
            await EventPromoter.deleteMany({});
        });

        describe('POST /api/event/promoter', () => {
            it('Should create a event promoter with a 200 response', async () => {
                const response = await request(app).post('/api/event/promoter')
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        event: testingCampaign._id,
                        promoter: promoter._id
                    });
                expect(response).to.have.property('status').equal(200);
                assert(!response['__v'], '__v version should not be included in response');
            });
        });

        describe('GET /api/event/promoter', () => {
            it('Should get promoters with a 200 response', async () => {
                const response = await request(app).get('/api/event/promoter?event=' + testingCampaign._id)
                    .set('Authorization', 'Bearer ' + token);
                expect(response).to.have.property('status').equal(200);
                assert(!response['__v'], '__v version should not be included in response');
                expect(response.body).to.be.an('array');
                expect(response.body).to.have.lengthOf(1);
            });
        });

        describe('GET /api/event/promoter', () => {
            it('Should get promoters by event and populate each participants with a 200 response', async () => {
                const response = await request(app).get('/api/event/promoter?event=' + testingCampaign._id + '&populate=promoter')
                    .set('Authorization', 'Bearer ' + token);
                expect(response).to.have.property('status').equal(200);
                assert(!response['__v'], '__v version should not be included in response');
                expect(response.body).to.be.an('array');
                expect(response.body).to.have.lengthOf(1);
                expect(response.body[0]).to.have.property('promoter');
                expect(response.body[0]['promoter']).to.have.property('email');
                expect(response.body[0]['promoter']).to.have.property('_id');
                eventPromoter = response.body;
            });
        });

        describe('PUT /api/event/promoter/:id', () => {
            it('Should update an event promoter with a 200 response', async () => {
                const response = await request(app).put('/api/event/promoter/' + eventPromoter[0]._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        accepted: true
                    });
                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('accepted').equal(true);
                assert(!response['__v'], '__v version should not be included in response');
            });
        });

        describe('DEL /api/event/participant', () => {
            it('Should delete an event promoter by event id and participant id', async () => {
                const response = await request(app).del('/api/event/promoter?event=' + testingCampaign._id + '&participant=' + participant._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        event: testingCampaign._id,
                        promoter: promoter._id
                    });
                //.send();

                expect(response).to.have.property('status').equal(200);
            });
        });

    });
};