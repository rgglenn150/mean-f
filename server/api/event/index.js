'use strict';

let express = require('express');
let controller = require('./event/event.controller');
let auth = require('../../auth/auth.middleware');

let router = express.Router();

router.use('/invite', require('./event-invite-pending'));

router.use('/participant', require('./event-participant'));

router.use('/promoter', require('./event-promoter'));

router.put('/makeLive/:id', auth.checkAuthenticated, controller.makeLive);


router.get('/search',controller.search);

//Event api calls
router.get('/', auth.checkAuthenticated, controller.index);
router.get('/public',controller.publicEvents);
router.get('/:id', controller.show);
router.post('/', auth.checkAuthenticated, controller.create);
router.put('/:id', auth.checkAuthenticated, controller.update);
router.delete('/:id', auth.checkAuthenticated, controller.destroy);

module.exports = router;