'use strict';

let express = require('express');
let controller = require('./event-invite-pending.controller');
let auth = require('../../../auth/auth.middleware');

let router = express.Router();

//Event participant and event promoter api calls
router.get('/accept/:id', auth.checkAuthenticated, controller.accept);
router.get('/deny/:id', auth.checkAuthenticated, controller.deny);

router.get('/', auth.checkAuthenticated, controller.findByEvent);
router.post('/', auth.checkAuthenticated, controller.create);
router.delete('/', auth.checkAuthenticated, controller.remove);

module.exports = router;