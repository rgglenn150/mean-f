let EventInvitePending = require('./event-invite-pending.model');
let EventParticipant = require('../event-participant/event-participant.model');
let EventPromoter = require('../event-promoter/event-promoter.model');

function checkPopulate(results) {
    let allowedFields = ['user'];
    return results
        .filter(e => allowedFields.includes(e))
        .map(e =>
            e === 'participant' ||
            e === 'user' ||
            e === 'promoter' ? { path:e, select: '-__v -salt -hashedPassword'} : {path:e}
        );
}

exports.findByEvent = function(req, res) {
    let promise = EventInvitePending.find({ event: req.query.event });

    let populateValues = req.query.populate ? req.query.populate.split(",") : [];
    if(populateValues.length > 0) {
        populateValues = checkPopulate(populateValues); //Security check
        promise = promise.populate(populateValues);
    }

    return promise.then((result) => {
        delete result['__v'];
        return res.json(result);
    }).catch((e) => {
        console.log(e)
        return res.status(500).json(e);
    });
}

exports.create = function(req, res) {
    return EventInvitePending.create({ event: req.body.event, user: req.body.user, type: req.body.type })
        .then((result) => {
            delete result['__v'];
            return res.json(result);
        }).catch((e) => {
            return res.status(500).json(e);
        });
}

exports.remove = function(req, res) {
    return EventInvitePending.deleteMany({ event: req.query.event, user: req.query.user })
        .then((d) => {
            res.type('text/plain');
            return res.sendStatus(200);
        }).catch((e) => {
            return res.status(500).json(e);
        });
}

//TODO unit test
function acceptOrDeny(id, accepted) {
    return EventInvitePending.findOne({ _id: req.params.id })
        .then((invite) => {
            if (!invite) {
                return res.status(500).json({ message: 'Could not find invite'});
            } else {
                return EventInvitePending.update({_id: req.params.id}, { $set: { accepted: accepted }}).then(()=> {
                    if (accepted) {
                        return invite.type === 'participant'
                            ? EventParticipant({event: invite.event, participant: invite.user})
                            : EventPromoter({event: invite.event, participant: invite.user});
                    }
                });
            }
        }).catch((e) => {
            return res.status(500).json(e);
        });
}

exports.accept = function(req, res) {
    return acceptOrDeny(req.params.id, true, res);
}

exports.deny = function(req, res) {
    return acceptOrDeny(req.params.id, false, res);
}

exports.sendInviteParticipant = function(req, res){

}

exports.sendInvitePromoter = function(req, res){

}