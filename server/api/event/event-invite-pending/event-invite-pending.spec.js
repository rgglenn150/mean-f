var config = require('../../../config/environment/index');
var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();
let TestingService = require('../../../test/test.service')
let request = require('supertest');
let Company = require('../../company/company.model');
let Event = require('../event/event.model');
let EventInvitePending = require('./event-invite-pending.model');
let User = require('../../user/user.model');

module.exports = function(app) {

    describe('Event Invite Pending Tests', () => {
        let user, token, testingUserData, testingCampaign, testingCampaign2, participant, promoter;
        let event = {
            name: "The Greatest Showman Event",
            description: "A testing event"
        };
        let company = {
            name: "The Greatest Showman",
            image: "image.com",
            description: "A testing company"
        };

        before(async () => {
            user = await TestingService.getUser();
            company.user = user._id;
            expect(user).to.not.be.null;
            token = TestingService.getToken(user);
            expect(token).to.not.be.null;
            testingUserData = TestingService.getTestingUserData();
            company = await Company.create(company);
            testingCampaign = await Event.create(event);
            event.user = user._id;
            await Event.create(event);
            event.company = company._id;
            testingCampaign2 = await Event.create(event);
            participant = await User.create({ firstName: 'friend', lastName:'foo', email:'friend9200@bringonthegood.com', password:'dumb' });
            expect(participant).to.not.be.null;
            promoter = await User.create({ firstName: 'friend', lastName:'foo', email:'friend527140@bringonthegood.com', password:'dumb' });
            expect(promoter).to.not.be.null;
        });

        after(async () => {
            await Company.deleteMany({});
            await Event.deleteMany({});
            await EventInvitePending.deleteMany({});
        });

        describe('POST /api/event/invite', () => {
            it('Should create a promoter event invite with a 200 response', async () => {
                const response = await request(app).post('/api/event/invite')
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        event: testingCampaign._id,
                        user: promoter._id,
                        type: 'promoter'
                    });
                expect(response).to.have.property('status').equal(200);
                expect(response.body).to.have.property('user').equal(promoter._id.toString());
                assert(!response['__v'], '__v version should not be included in response');
            });
        });

        describe('POST /api/event/invite', () => {
            it('Should create a participant event invite with a 200 response', async () => {
                const response = await request(app).post('/api/event/invite')
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        event: testingCampaign._id,
                        user: participant._id,
                        type: 'participant'
                    });
                expect(response).to.have.property('status').equal(200);
                assert(!response['__v'], '__v version should not be included in response');
            });
        });

        describe('POST /api/event/invite', () => {
            it('Should fail when creating a participant invite to an event with type that is not participant, expect 500 response', async () => {
                const response = await request(app).post('/api/event/invite')
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        event: testingCampaign._id,
                        user: participant._id,
                        type: 'invite'
                    });
                expect(response).to.have.property('status').equal(500);
            });
        });

        describe('GET /api/event/invite', () => {
            it('Should get event invites with a 200 response', async () => {
                const response = await request(app).get('/api/event/invite?event=' + testingCampaign._id)
                    .set('Authorization', 'Bearer ' + token);
                expect(response).to.have.property('status').equal(200);
                assert(!response['__v'], '__v version should not be included in response');
                expect(response.body).to.be.an('array');
                expect(response.body).to.have.lengthOf(2);
            });
        });

        describe('GET /api/event/invite', () => {
            it('Should get participants by event and populate each participants with a 200 response', async () => {
                const response = await request(app).get('/api/event/invite?event=' + testingCampaign._id + '&populate=user')
                    .set('Authorization', 'Bearer ' + token);
                expect(response).to.have.property('status').equal(200);
                assert(!response['__v'], '__v version should not be included in response');
                expect(response.body).to.be.an('array');
                expect(response.body).to.have.lengthOf(2);
                expect(response.body[0]).to.have.property('user');
                expect(response.body[0]['user']).to.have.property('_id');
            });
        });

        describe('DEL /api/event/participant', () => {
            it('Should delete an event participant by event id and participant id', async () => {
                const response = await request(app).del('/api/event/invite?event=' + testingCampaign._id + '&user=' + participant._id)
                    .set('Authorization', 'Bearer ' + token)
                //.send();

                expect(response).to.have.property('status').equal(200);
            });
        });

    });
};