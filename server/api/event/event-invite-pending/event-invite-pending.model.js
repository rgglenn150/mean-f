'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/* Tells you who's pending for the event */

var EventInvitePendingSchema = new Schema({
    event: {
        type: Schema.ObjectId,
        ref: 'Event',
        required: true
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    type: { type: String, enum: ['participant', 'promoter'], default:'participant'},
    created: {type: Date, default: Date.now}
});
//EventInvitePendingSchema.index({ event: 1, user: 1, type: 1});
EventInvitePendingSchema.index({ event: 1 });
EventInvitePendingSchema.index({ event: 1, user: 1, type: 1 }, { unique: true });

module.exports = mongoose.model('EventInvitePending', EventInvitePendingSchema);
