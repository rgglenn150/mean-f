'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var EventParticipantSchema = new Schema({
    event: {
        type: Schema.ObjectId,
        ref: 'Event',
        required: true
    },
    participant: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    accepted: { type: Boolean, default: false},
    inviteSent: { type: Boolean, default: false}
});
EventParticipantSchema.index({ event: 1 });
EventParticipantSchema.index({ event: 1, participant: 1}, { unique: true });

module.exports = mongoose.model('EventParticipant', EventParticipantSchema);
