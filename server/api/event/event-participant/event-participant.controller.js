let EventParticipant = require('./event-participant.model');
let EventCtrl = require('../event/event.controller');
let utils = require('../../utils/utils');

function checkPopulate(results) {
    let allowedFields = ['participant'];
    return results
        .filter(e => allowedFields.includes(e))
        .map(e =>
            e === 'participant' ||
            e === 'user' ||
            e === 'promoter' ? { path:e, select: '-__v -salt -hashedPassword'} : {path:e}
        );
}

exports.findByEvent = function(req, res) {
    let promise = EventParticipant.find({ event: req.query.event });

    let populateValues = req.query.populate ? req.query.populate.split(",") : [];
    if(populateValues.length > 0) {
        populateValues = checkPopulate(populateValues); //Security check
        promise = promise.populate(populateValues);
    }
    return promise.then((result) => {
            delete result['__v'];
            //return res.json(result.map(p => p.participant));
            return res.json(result);
        }).catch((e) => {
            console.log(e)
            return res.status(500).json(e);
        });
}

exports.create = function(req, res) {
    return EventParticipant.create({ event: req.body.event, participant: req.body.participant })
        .then((result) => {
            delete result['__v'];
            return res.json(result);
        }).then(()=>{
            return EventCtrl.updateParticipants(req.body.event);
        }).catch((e) => {
            return res.status(500).json(e);
        });
}

exports.update = function(req, res) {
    let result;
    return utils.updateObject(EventParticipant, req.params.id, req.body)
        .then((data) => {
            result = data;
            delete result['__v'];
            return res.json(result);
        }).then(()=>{
            return EventCtrl.updateParticipants(result.event);
        }).catch((e) => {
            return res.status(500).json(e);
        });
}

exports.remove = function(req, res) {
    return EventParticipant.deleteMany({ event: req.query.event, participant: req.query.participant })
        .then(()=>{
            return EventCtrl.updateParticipants(req.query.event);
        })
        .then((d) => {
            res.type('text/plain');
            return res.sendStatus(200);
        })
        .catch((e) => {
            return res.status(500).json(e);
        })
}