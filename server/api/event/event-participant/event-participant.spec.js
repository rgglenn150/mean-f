var config = require('../../../config/environment/index');
var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();
let TestingService = require('../../../test/test.service')
let request = require('supertest');
let Company = require('../../company/company.model');
let Event = require('../event/event.model');
let EventParticipant = require('./event-participant.model');
let User = require('../../user/user.model');

module.exports = function(app) {

    describe('Event Participant Tests', () => {
        let user, token, testingUserData, testingCampaign, testingCampaign2, participant, promoter, eventParticipant;
        let event = {
            name: "The Greatest Showman Event",
            description: "A testing event"
        };
        let company = {
            name: "The Greatest Showman",
            image: "image.com",
            description: "A testing company"
        };

        before(async () => {
            user = await TestingService.getUser();
            company.user = user._id;
            expect(user).to.not.be.null;
            token = TestingService.getToken(user);
            expect(token).to.not.be.null;
            testingUserData = TestingService.getTestingUserData();
            company = await Company.create(company);
            testingCampaign = await Event.create(event);
            event.user = user._id;
            await Event.create(event);
            event.company = company._id;
            testingCampaign2 = await Event.create(event);
            participant = await User.create({ firstName: 'friend', lastName:'foo', email:'friend99100@bringonthegood.com', password:'dumb' });
            expect(participant).to.not.be.null;
            promoter = await User.create({ firstName: 'friend', lastName:'foo', email:'friend15605140@bringonthegood.com', password:'dumb' });
            expect(promoter).to.not.be.null;
        });

        after(async () => {
            await Company.deleteMany({});
            await Event.deleteMany({});
            await EventParticipant.deleteMany({});
        });

        describe('POST /api/event/participant', () => {
            it('Should create an event participant with a 200 response', async () => {
                const response = await request(app).post('/api/event/participant')
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        event: testingCampaign._id,
                        participant: participant._id
                    });
                expect(response).to.have.property('status').equal(200);
                assert(!response['__v'], '__v version should not be included in response');
            });
        });

        describe('GET /api/event/participant', () => {
            it('Should get participants with a 200 response', async () => {
                const response = await request(app).get('/api/event/participant?event=' + testingCampaign._id)
                    .set('Authorization', 'Bearer ' + token);
                expect(response).to.have.property('status').equal(200);
                assert(!response['__v'], '__v version should not be included in response');
                expect(response.body).to.be.an('array');
                expect(response.body).to.have.lengthOf(1);
            });
        });

        describe('GET /api/event/participant', () => {
            it('Should get participants by event and populate each participants with a 200 response', async () => {
                const response = await request(app).get('/api/event/participant?event=' + testingCampaign._id + '&populate=participant')
                    .set('Authorization', 'Bearer ' + token);
                expect(response).to.have.property('status').equal(200);
                assert(!response['__v'], '__v version should not be included in response');
                expect(response.body).to.be.an('array');
                expect(response.body).to.have.lengthOf(1);
                expect(response.body[0]).to.have.property('participant');
                expect(response.body[0]['participant']).to.have.property('email');
                expect(response.body[0]['participant']).to.have.property('_id');
                eventParticipant = response.body;
            });
        });

        describe('PUT /api/event/participant/:id', () => {
            it('Should update an event participant with a 200 response', async () => {
                const response = await request(app).put('/api/event/participant/' + eventParticipant[0]._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        accepted: true
                    });
                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('accepted').equal(true);
                assert(!response['__v'], '__v version should not be included in response');
            });
        });


        describe('DEL /api/event/participant', () => {
            it('Should delete an event participant by event id and participant id', async () => {
                const response = await request(app).del('/api/event/participant?event=' + testingCampaign._id + '&participant=' + participant._id)
                    .set('Authorization', 'Bearer ' + token)
                //.send();

                expect(response).to.have.property('status').equal(200);
            });
        });

    });
};