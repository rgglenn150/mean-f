var express = require('express');
var config = require('../config/environment');

var router = express.Router();

router.get('/', (req, res)=>{
	res.send( '<html><body>Hello!</body></html>' );
});

module.exports = router;