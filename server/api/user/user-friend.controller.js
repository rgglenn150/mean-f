let EmailCtrl = require("../email/email.controller");
let utils = require('../utils/utils');
let UserFriend = require('./user-friend.model');
let config = require('../../config/environment');
let User = require('./user.model');

exports.index = function (req, res) {
    let populate = {
        path: 'friend',
        select: 'firstName lastName email profilePhoto'
    };
    if (req.params.id) {
        return UserFriend.find({
            user: req.user._id,
            friend: req.params.id,
            status: 'active'
        }).populate(populate).then((data) => {
            return res.json(data);
        }).catch((e) => {
            console.error(e);
            return res.sendStatus(500);
        });
    } else {
        return UserFriend.find({
            user: req.user._id,
            status: 'active'
        }).populate(populate).then((data) => {
            return res.json(data);
        }).catch((e) => {
            console.error(e);
            return res.sendStatus(500);
        });
    }
};

exports.getPending = function (req, res) {
    let populate = {
        path: 'user',
        select: 'firstName lastName email profilePhoto _id'
    };
    return UserFriend.find({
        friend: req.user._id,
        status: 'pending'
    }).populate(populate).then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.error(e);
        return res.sendStatus(500);
    });
};

exports.removeFriend = function (req, res) {
    return UserFriend.findOne({
        _id: req.params.id
    }).then((userFriend) => {
        return UserFriend.deleteMany({
            token: userFriend.token
        });
    }).then(() => {
        return res.sendStatus(200);
    }).catch(() => {
        return res.sendStatus(500);
    });
};

/*

    -> User Invites friend
    -> model is stored with a token in the user-friend model
    -> friend recieves an email with a link to a registration page with a token in the url parameters
    -> friend registers with that token and gets associated automatically with the requestor.

 */

exports.inviteUser = function (req, res) {
    if (!req.body.email) {
        return res.status(422).json({
            message: 'email required'
        });
    } else if (req.body.email === req.user.email) {
        return res.status(422).json({
            message: 'Cannot invite yourself'
        });
    } else {
        let token = utils.createUuid();
        let relationship = req.body.relationship ? req.body.relationship : 'friend';

        if (relationship === 'employee') {
            relationship = 'supervisor';
        } else if (relationship === 'supervisor') {
            relationship = 'employee';
        }
        let user;
        return User.findOne({
            email: req.body.email
        }).then((data) => {
            user = data;
            let userFriend = {
                user: req.user._id,
                relationship: relationship,
                token: token,
                friendEmail: req.body.email
            };
            if (!utils.isEmpty(user)) {
                userFriend = {
                    user: req.user._id,
                    relationship: relationship,
                    token: token,
                    friend: user._id
                };
            }
            return UserFriend.create(userFriend);
        }).then(() => {
            if (!user && config.env !== 'test') {
                EmailCtrl.inviteUser(req.user, req.body.email, token);
            } else if (config.env !== 'test') {
                EmailCtrl.friendRequestUser(req.user, req.body.email, req.body.relationship);
            }
            if (user) {
                return res.status(200).json({
                    token: token,
                    userId: user._id
                });
            } else {
                return res.status(200).json({
                    token: token
                });
            }

        }).catch((e) => {
            console.error(e);
            return res.sendStatus(500);
        });
    }
};

function completeInvite(friend, token) {
    if (!token || !friend) {
        return Promise.resolve({});
    } else {
        return UserFriend.updateOne({
            token: token
        }, {
            $set: {
                friend: friend._id,
                status: 'active'
            }
        }).then((result) => {
            return UserFriend.findOne({
                token: token
            }).then((requestedUser) => { // find the user that originally requested called requestedUser
                let relationship = requestedUser.relationship;
                if (relationship === 'employee') {
                    relationship = 'supervisor';
                } else if (relationship === 'supervisor') {
                    relationship = 'employee';
                }
                return UserFriend.create({
                    token: token,
                    user: friend._id,
                    friend: requestedUser.user,
                    relationship: relationship,
                    status: 'active'
                });
            });
        }).then((newFriend) => {
            return newFriend ? newFriend : friend;
        });
    }
}

exports.completeInvite = completeInvite;

exports.completeInviteRoute = function (req, res) {
    if (!req.params.id) {
        return res.status(422).json({
            message: 'id required'
        });
    } else {
        return UserFriend.findOne({
            _id: req.params.id
        }).then((userFriend) => {
            return completeInvite(userFriend.friend, userFriend.token);
        }).then((newFriend) => {
            let populate = {
                path: 'friend',
                select: 'firstName lastName email profilePhoto _id'
            };
            return UserFriend.findOne({
                _id: newFriend._id
            }).populate(populate);
        }).then((newFriend) => {
            return res.json(newFriend);
        }).catch((e) => {
            console.log(e);
            return res.sendStatus(500);
        });
    }
}