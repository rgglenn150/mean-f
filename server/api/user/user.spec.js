var config = require('../../config/environment');
var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();
var UserFriendCtrl = require('./user-friend.controller');
let UserFriend  = require('./user-friend.model');
let User = require('./user.model');
let TestingService = require('../../test/test.service');
let request = require('supertest');

module.exports = function(app) {

    describe('User Tests', () => {
        let user, token, testingUserData;

        before(async () => {
            user = await TestingService.getUser();
            expect(user).to.not.be.null;
            token = TestingService.getToken(user);
            expect(token).to.not.be.null;
            testingUserData = TestingService.getTestingUserData();
            expect(testingUserData).to.not.be.null;
            expect(testingUserData).to.have.property('email');
        });

        after(async () => {

        });

        describe('GET /api/user/me', () => {
            it('Should get the authenticated user with response 200 and have a specific email', async () => {
                const response = await request(app).get('/api/user/me')
                    .set('Authorization', 'Bearer ' + token);
                //.send();

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('email').equal(testingUserData.email)
            });
        });
    });
}
