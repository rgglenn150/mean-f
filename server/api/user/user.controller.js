'use strict';

let User = require('./user.model');
let config = require('../../config/environment');
let jwt = require('jsonwebtoken');
let EmailCtrl = require('../email/email.controller');

var validationError = function (res, err) {
    if (err && err.code === 11000) {
        return res.setStatus(422).json({
            message: "user exists"
        });
    } else {
        return res.setStatus(422).json(err);
    }
};

exports.revokeApiKey = function (req, res) {
    User.Update(req.user._id, {
            apiKey: undefined
        })
        .then(function (data) {
            return res.status(200).json(data.apiKey);
        }, function (err) {
            return res.send(500, err);
        });
};

function generateApiKey(user) {
    var token = jwt.sign({
        _id: user._id
    }, config.sessionSecret, {
        expiresIn: Number.MAX_VALUE
    });
    return User.updateOne({
        _id: user._id
    }, {
        $set: {
            apiKey: token
        }
    }).then(() => {
        return token;
    });
}

exports.generateApiKey = function (req, res) {
    generateApiKey(req.user).then((data) => {
        return res.status(200).json(data.apiKey);
    }, function (err) {
        return res.send(500, err);
    });
};

/*
Super admin to show user
 */

exports.showBySuperAdmin = function (req, res) {
    User.findOne({
        _id: req.params.id
    }).select('-salt -hashedPassword').exec().then(function (user) {
        return res.json(user);
    }, function (err) {
        return handleError(err, res);
    });
};


/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function (req, res) {


    User.find({}, '-salt -hashedPassword', function (err, users) {
        if (err) return res.send(500, err);
        res.json(200, users);
    });
};

exports.getUserByUsername = function (req, res) {

    User.findOne({
        username: req.params.username
    }, '-salt -hashedPassword', function (err, user) {

      
        if (err) return res.send(500, err);
        if (!user) {
            res.status(200).json({
                status: 'failed',
                message: 'User not found!'
            });
        } else {
            if (user.isUrlPrivate && JSON.stringify(user._id) !== JSON.stringify(req.query.userId)) {
               
                res.status(200).json({
                    status: 'private',
                    message: 'Profile is private!'
                });
            } else {
                res.status(200).json(user);
            }
        }

    });
};

exports.update = function (req, res) {
    User.Update(req.params.id, req.body).then((user) => {
        return res.json(200, user);
    }, function (err) {
        return res.send(500, err);
    });
};

exports.updateMe = function (req, res) {
    User.Update(req.user._id, req.body).then((user) => {
        return res.json(200, user);
    }, function (err) {
        return res.send(500, err);
    });
};

exports.updateFCMToken = function (req, res) {
    User.Update(req.user._id, req.body).then((user) => {
        return res.status(200).json(user.fcmToken)
  
    }, function (err) {
        
        return res.status(500).json(err)
    });
};


exports.getFCMToken = function (req, res, next) {
    
    let userId = req.query._id;
    User.findOne({
        _id: userId
    }, '-salt -hashedPassword', function (err, user) { // don't ever give out the password or salt
        if (err) return next(err);
        if (!user) return res.json(401);
       
        res.json(user.fcmToken);
    });
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.findByAccount = function (req, res) {
    User.find({
            account: req.user.account
        })
        .select('-salt -hashedPassword')
        .exec()
        .then(function (users) {
            res.json(200, users);
        }, function (err) {
            return res.send(500, err);
        });
};


function sendToken(user, res) {
    var token = jwt.sign({
        _id: user._id
    }, config.sessionSecret, {
        expiresIn: config.expiresInMinutes
    });
    return res.json({
        firstName: user.firstName,
        lastName: user.lastName,
        token: token
    });
}

function sendAuthError(res) {
    return res.json({
        success: false,
        message: 'email or password incorrect'
    });
}

exports.registerUser = function (user) {
    let newUser = {};
    return User.create(user).then((data) => {
        newUser = data;
        return generateApiKey(newUser);
    }).then(() => {
        let token = jwt.sign({
            _id: user._id
        }, config.sessionSecret, {
            expiresIn: config.expiresInMinutes
        });
        return {
            firstName: user.firstName,
            lastName: user.lastName,
            token: token
        };
    });
}

exports.register = function (req, res) {
    let user = req.body;
    user.created = new Date();
    user.email = user.email.trim();
    user.firstName = user.firstName.trim();
    user.lastName = user.lastName.trim();
    return registerUser(user).then((newResponse) => {
        return res.json(newResponse);
    }).catch((err) => {
        console.log(err);
        return validationError(res, err);
    });
};


/**
 * Get a single user
 */
exports.show = function (req, res, next) {
    delete req.user.__v;
    delete req.user.salt;
    delete req.user.hashedPassword;
    return res.json(req.user);
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function (req, res) {
    User.findByIdAndRemove(req.params.id, function (err, user) {
        if (err) return res.send(500, err);
        return res.send(204);
    });
};

/**
 * Change a users password
 */
exports.changePassword = function (req, res, next) {
    var userId = req.user._id;
    var oldPass = String(req.body.oldPassword);
    var newPass = String(req.body.newPassword);

    User.findById(userId, function (err, user) {
        if (user.authenticate(oldPass)) {
            user.password = newPass;
            user.save(function (err) {
                if (err) return validationError(res, err);
                res.send(200);
            });
        } else {
            res.send(403);
        }
    });
};

/**
 * Get my info
 */
exports.me = function (req, res, next) {
    let userId = req.user._id;
    User.findOne({
        _id: userId
    }, '-salt -hashedPassword', function (err, user) { // don't ever give out the password or salt
        if (err) return next(err);
        if (!user) return res.json(401);
        res.json(user);
    });
};



/**
 * Authentication callback
 */
exports.authCallback = function (req, res, next) {
    res.redirect('/');
};

exports.resetpassword = function (req, res) {
    if (req.body.email) {
        User.findOne({
                email: req.body.email.toLowerCase()
            }).exec()
            .then((user) => {
                if (!user) {
                    return res.sendStatus(400);
                } else {
                    user.resetToken = User.getResetToken();
                    user.resetTokenExpire = User.getResetTokenExpire();
                    user.save((err) => {
                        if (err) {
                            console.log(err);
                            return res.sendStatus(400);
                        }
                        res.json({
                            success: true
                        });
                        return EmailCtrl.resetPassword(user.firstName, user.email, user.resetToken);
                    });
                }
            }, (err) => {
                return res.sendStatus(400);
            });
    } else {
        res.sendStatus(400);
    }
};

// add username or route for vanity url
exports.setUsername = async (req, res) => {

    const _id = req.user._id;
    const username = req.body.username;
    const isUrlPrivate = req.body.isUrlPrivate;
    const isNamePrivate = req.body.isNamePrivate;
    let usernameExists = await User.findOne({
        'username': username
    });
    if (_id && username && !usernameExists) {
        try {

            let user = await User.updateOne({
                "_id": _id
            }, {
                $set: {
                    "username": username,
                    "isUrlPrivate": isUrlPrivate,
                    "isNamePrivate": isNamePrivate
                }
            }, {
                new: true
            });

            res.status(200).json({
                user: user
            });
        } catch (error) {
            console.log(error);
            res.status(400).json({
                message: 'Add username failed!'
            });
        }
    } else if (_id) {
        try {

            let user = await User.updateOne({
                "_id": _id
            }, {
                $set: {
                    "isUrlPrivate": isUrlPrivate,
                    "isNamePrivate": isNamePrivate
                }
            }, {
                new: true
            });

            res.status(200).json({
                user: user
            });
        } catch (error) {
            console.log(error);
            res.status(400).json({
                message: 'Update Privacy failed!'
            });
        }
    } else {
        res.status(400).json({
            message: 'Username Exists!'
        });
    }
}

exports.getUsername = async (req, res) => {
    const _id = req.query.userId;
    let user = await User.findOne({
        '_id': _id
    });
    if (user) {
        try {
            res.status(200).json({
                value: user.username,
                isNamePrivate: user.isNamePrivate,
                isUrlPrivate: user.isUrlPrivate
            });
        } catch (error) {
            console.log(error);
            res.status(400).json({
                message: 'Fetch username failed!'
            });
        }
    } else {
        res.status(200).json({
            message: 'Username does not exists!'
        });
    }
}




/**
 * Change password for password recovery
 */
exports.recoverChangePassword = function (req, res, next) {
    var newPass = String(req.body.newPassword);
    var retypePass = String(req.body.retypePassword);
    var resetToken = String(req.body.resetToken);
    if (retypePass !== newPass || resetToken === "") {
        res.sendStatus(400);
    } else {
        User.findOne({
            resetToken: resetToken
        }, (err, user) => {
            var now = new Date();
            if (err) {
                console.log(err);
                return next(err);
            } else if (!user || now.getTime() > user.resetTokenExpire) {
                res.sendStatus(403);
            } else {
                user.password = newPass;
                user.resetToken = "";
                user.save((err) => {
                    if (err) return res.sendStatus(400);
                    res.sendStatus(200);
                });
            }
        });
    }
};



function handleError(res, err) {
    console.log(err);
    return res.send(500, err);
}