'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var config = require('../../config/environment');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var Bluebird = require('bluebird');
var crypto = require('crypto');

var UserSchema = new Schema({
    firstName: String,
    lastName: String,
    email: {
        type: String,
        lowercase: true,
        required: true
    },
    username: String,
    linkedEmail: { //the ability to link other email accounts to your main account
        type: Schema.ObjectId,
        ref: 'User'
    },
    role: {
        type: String,
        enum: ['guest', 'user', 'admin', 'superadmin'],
        default: 'admin'
    },
    status: {
        type: String,
        enum: ['active', 'inactive', 'pending'],
        default: 'active'
    },
    hashedPassword: String,
    salt: String,
    created: {
        type: Date,
        default: Date.now
    },
    apiKey: String,
    reactivateToken: String,
    resetToken: String,
    profilePhoto: String,
    facebookId: String,
    linkedinId: String,
    isUrlPrivate: Boolean,
    isNamePrivate: Boolean,
    orgId: {
        type: Schema.ObjectId,
        ref: 'Organization',
        default: null
    },
    confirmationToken: String,
    fcmToken: String
});

/**
 * Virtuals
 */
UserSchema
    .virtual('password')
    .set(function (password) {
        this._password = password;
        this.salt = this.makeSalt();
        this.hashedPassword = this.encryptPassword(password);
    })
    .get(function () {
        return this._password;
    });

// Public profile information
UserSchema
    .virtual('profile')
    .get(function () {
        return {
            'name': this.name,
            'role': this.role,
            '_id': this._id,
            'email': this.email,
            'created': this.created
        };
    });

// Non-sensitive info we'll be putting in the token
UserSchema
    .virtual('token')
    .get(function () {
        return {
            '_id': this._id,
            'role': this.role
        };
    });

/**
 * Validations
 */

// Validate empty email
UserSchema
    .path('email')
    .validate(function (email) {
        return email.length;
    }, 'Email cannot be blank');

// Validate empty password
UserSchema
    .path('hashedPassword')
    .validate(function (hashedPassword) {
        return hashedPassword.length;
    }, 'Password cannot be blank');

UserSchema
    .path('email')
    .validate(function (value) {
        var self = this;
        this.constructor.findOne({
            email: value
        }, function (err, user) {
            if (err) throw err;
            if (user) {
                if (self.id === user.id) return true;
                return false;
            }
            return true;
        });
    }, 'The specified email address is already in use.');

var validatePresenceOf = function (value) {
    return value && value.length;
};

/**
 * Pre-save hook
 */
UserSchema
    .pre('save', function (next) {
        if (!this.isNew) return next();

        if (!validatePresenceOf(this.hashedPassword))
            next(new Error('Invalid password'));
        else
            next();
    });

UserSchema
    .virtual('profile')
    .get(function () {
        return {
            'firstName': this.firstName,
            'lastName': this.lastName,
            'email': this.email,
            '_id': this._id,
            'apiKey': this.apiKey
        };
    });

/**
 * Methods
 */
UserSchema.methods = {
    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} plainText
     * @return {Boolean}
     * @api public
     */
    authenticate: function (plainText) {
        return (this.encryptPassword(plainText) === this.hashedPassword)
    },

    /**
     * Make salt
     *
     * @return {String}
     * @api public
     */
    makeSalt: function () {
        return crypto.randomBytes(16).toString('base64');
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @return {String}
     * @api public
     */
    encryptPassword: function (password) {
        if (!password || !this.salt) return '';
        var salt = new Buffer.from(this.salt, 'base64');
        return crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('base64');
    }
};

UserSchema.statics.Update = function (objectId, object) {
    return new Bluebird((resolve, reject) => {
        this.findById(objectId, function (err, myObject) {
            if (err) return reject(err);
            if (!myObject) return reject("Object not found.");
            var updated = _.extend(myObject, object);
            updated.save(function (err, updatedObject) {
                if (err) return reject(err);
                return resolve(updatedObject);
            });
        });
    });
};

UserSchema.statics.getResetToken = function () {
    return crypto.randomBytes(32).toString('hex');
};
UserSchema.statics.getResetTokenExpire = function () {
    var now = new Date();
    return new Date(now.getTime() + (config.resetTokenExpiresMinutes * 60 * 1000)).getTime();
};

UserSchema.statics.getApiToken = function (userId) {
    var _this = this;
    return _this.findOne({
        _id: userId
    }).exec().then(function (data) {
        if (typeof data != 'undefined' && data != null && typeof data.apiKey != 'undefined' && data.apiKey != null) {
            return data.apiKey;
        } else {
            var token = jwt.sign({
                _id: userId
            }, config.secrets.session);
            return _this.update({
                _id: userId
            }, {
                $set: {
                    apiKey: token
                }
            }).exec().then(function () {
                return token;
            });
        }
    });
};

UserSchema.index({
    email: 1
}, {
    unique: true
});
UserSchema.index({
    apiKey: 1
});

module.exports = mongoose.model('User', UserSchema);