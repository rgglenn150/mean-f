var config = require('../../config/environment');
var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();
var UserFriendCtrl = require('./user-friend.controller');
let UserFriend  = require('./user-friend.model');
let User = require('./user.model');
let TestingService = require('../../test/test.service');
let request = require('supertest');

module.exports = function(app) {

    describe('User Friend Tests', () => {
        let user, token, testingUserData, inviteToken, friend2, userFriend2;

        before(async () => {
            await UserFriend.deleteMany({});
            user = await TestingService.getUser();
            expect(user).to.not.be.null;
            token = TestingService.getToken(user);
            expect(token).to.not.be.null;
            testingUserData = TestingService.getTestingUserData();
            expect(testingUserData).to.not.be.null;
            expect(testingUserData).to.have.property('email');

            let friend = await User.create({ firstName: 'friend', lastName:'foo', email:'friend3@bringonthegood.com', password:'dumb' });
            expect(friend).to.not.be.null;
            let userFriend = await UserFriend.create({ user: friend._id, friend:user._id, token: 'tokenizer', relationship:'supervisor' });
            expect(userFriend).to.not.be.null;

            friend2 = await User.create({ firstName: 'friend', lastName:'foo', email:'friend4@bringonthegood.com', password:'dumb' });
            expect(friend2).to.not.be.null;
            let myInviteToken = 'dumbtoken6';
            userFriend2 = await UserFriend.create({ user:user._id, token: myInviteToken, relationship:'supervisor' });
            expect(userFriend2).to.not.be.null;

        });

        after(async () => {
            return UserFriend.deleteMany({});
        });

        describe('POST /api/user/friend/invite', async () => {
            it('Should fail when trying to invite yourself with response 422', async () => {
                const response = await request(app).post('/api/user/friend/invite')
                    .set('Authorization', 'Bearer ' + token)
                    .send({email: user.email});

                expect(response).to.have.property('status').equal(422);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('message').equal('Cannot invite yourself');
                inviteToken = response.body.token;
            });

            it('Should invite a new user with response 200 and return a token', async () => {
                const response = await request(app).post('/api/user/friend/invite')
                    .set('Authorization', 'Bearer ' + token)
                    .send({email: 'friends100@bringonthegood.com', relationship: 'friend'});

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('token');
                inviteToken = response.body.token;
            });
        });
        describe('Complete Invite', () =>{
            it('Should complete an invite after a friend registers by creating a new UserFriendRecord for the friend and updating the original must return friend', async () => {
                let friend = await User.create({ firstName: 'friend', lastName:'foo', email:'friends100@bringonthegood.com', password:'dumb' });
                expect(friend).to.not.be.null;
                return UserFriendCtrl.completeInvite(friend, inviteToken).then((response) => {
                    //expect(response).to.have.property('email');
                    return Promise.all([
                        UserFriend.findOne({ user: user._id, token: inviteToken }),
                        UserFriend.findOne({ user: friend._id, token: inviteToken })
                    ]);
                }).then((friends)=>{
                    expect(friends[0]).to.have.property('relationship').equal('friend');
                    expect(friends[1]).to.have.property('relationship').equal('friend');
                });
            });

            it('Should complete an invite after a employee registers by creating a new UserFriendRecord for the employee and updating the original must return supervisor', async () => {
                let friend = await User.create({ firstName: 'friend', lastName:'foo', email:'friend200@bringonthegood.com', password:'dumb' });
                expect(friend).to.not.be.null;
                let myInviteToken = 'dumbtoken';
                let userFriend = await UserFriend.create({ user:user._id, token: myInviteToken, relationship:'supervisor' });
                return UserFriendCtrl.completeInvite(friend, myInviteToken).then((response) => {
                    
                    return Promise.all([
                        UserFriend.findOne({ user: user._id, token: myInviteToken }),
                        UserFriend.findOne({ user: friend._id, token: myInviteToken })
                    ]);
                }).then((friends)=>{
                    expect(friends[0]).to.have.property('relationship').equal('supervisor');
                    expect(friends[1]).to.have.property('relationship').equal('employee');
                });
            });
        });
        describe('GET /api/user/friend', async () => {

            it('Should get all friends with response 200 and return an array', async () => {
                const response = await request(app).get('/api/user/friend')
                    .set('Authorization', 'Bearer ' + token);

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('array');
                response.body.forEach((friend)=>{
                    expect(friend).to.have.property('status').equal('active');
                });
            });
        });
        describe('GET /api/user/friend/pending', async () => {
            it('Should get all pending friend requests with response 200 and return an array', async () => {
                const response = await request(app).get('/api/user/friend/pending')
                    .set('Authorization', 'Bearer ' + token);

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('array');
                expect(response.body[0]).to.have.property('status').equal('pending');
            });
        });

        describe('POST /api/user/friend/complete/:id', async () => {

            it('Should complete an invite and return a new user-friend with a response 200', async () => {
                const response = await request(app).post('/api/user/friend/complete/' + userFriend2._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({});

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('status').equal('active');
                // inviteToken = response.body.token;
            });
        });
        describe('DEL /api/user/friend/:id', async () => {
            it('Should get badge by id', async () => {
                const response = await request(app).del('/api/user/friend/' + userFriend2._id)
                    .set('Authorization', 'Bearer ' + token);

                expect(response).to.have.property('status').equal(200);
            });
        });
    });
}
