'use strict';

var express = require('express');
var controller = require('./user-subaccount.controller');
var auth = require('../../../auth/auth.middleware');

var router = express.Router();

router.get('/', auth.checkAuthenticated, controller.index);
//router.get('/:id', auth.checkAuthenticated, controller.show);
router.post('/', auth.checkAuthenticated, controller.create);
router.get('/confirm/:confirmationToken', controller.confirm);
router.delete('/:id', auth.checkAuthenticated, controller.destroy);
/*
router.put('/:id', auth.checkAuthenticated, controller.update);
router.delete('/:id', auth.checkAuthenticated, controller.destroy);*/

module.exports = router;
