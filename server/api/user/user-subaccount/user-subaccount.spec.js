var config = require('../../../config/environment');
var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();

let User = require('../user.model');
let TestingService = require('../../../test/test.service');
let request = require('supertest');

module.exports = function(app) {

    describe('User Sub-Accounts Tests', () => {
        let user, token, testingUserData;

        before(async () => {
            user = await TestingService.getUser();
            expect(user).to.not.be.null;
            token = TestingService.getToken(user);
            expect(token).to.not.be.null;
            testingUserData = TestingService.getTestingUserData();
            expect(testingUserData).to.not.be.null;
            expect(testingUserData).to.have.property('email');
        });

        after(async () => {

        });

        describe('POST /api/user/subaccount', () => {
            it('Should create user if email does not exists and create a user subaccount with response 200 and sends and email with confirmation token', async () => {
                const response = await request(app).post('/api/user/subaccount')
                    .set('Authorization', 'Bearer ' + token)
                    .send({email: 'tubiglang@gmail.com'});
                //.send();

                expect(response).to.have.property('status').equal(200);
               
            });
        });
    });
}
