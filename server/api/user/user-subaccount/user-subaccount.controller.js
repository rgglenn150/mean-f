let UserSubaccount = require('./user-subaccount.model');
let User = require('../user.model');
let utils = require('../../utils/utils');
let config = require('../../../config/environment');
let EmailCtrl = require("../../email/email.controller");
let handleError = (res, err) => {
    //console.error(err);
    return res.status(500).json({
        err: err.message,
        message: 'failed'
    });
}

exports.getUserSubaccounts = async (userId) => {
    if (userId) {
        return UserSubaccount.find({
            user: userId
        });
    }
}

exports.index = async (req, res) => {
    const userId = req.query.userId;
    if (userId) {
        try {
            const results = await this.getUserSubaccounts(userId);

            const subAccounts = (results) => {
                const promises = results.map(async (result) => {
                    var subAccount = result.toObject();

                    await User.findOne({
                        _id: result.subaccount
                    }).then(res => {
                        subAccount.data = res;
                    });
                    subAccount.status = result.status;

                    return subAccount;
                });
                return Promise.all(promises);
            }
            subAccounts(results).then(result => {

                return res.status(200).json(result);
            });


        } catch (e) {
            return res.status(400).json({
                message: 'Error occured on user-subaccount!'
            });
            handleError(res, e);
        }
    } else {
        return res.status(400).json({
            message: 'Error occured on user-subaccount!'
        });
    }
}

exports.createUserSubaccount = async (email, currentUser, orgId = null) => {
    let userExists = await User.findOne({
        email: email
    });
    let confirmationToken = utils.createUuid();
    if (!userExists) {
        userExists = await User.create({
            firstName: currentUser.firstName,
            lastName: currentUser.lastName,
            email: email,
            orgId: orgId,
            password: utils.createUuid()
        });
    }
    let subaccountId = userExists._id;
    //upsert equivalent in mongoose

    //TODO send email to invite the user
    if (config.env !== 'test') {
        EmailCtrl.confirmEmailSubaccount(userExists, email, confirmationToken);
    }

    return UserSubaccount.findOneAndUpdate({
        user: currentUser._id,
        subaccount: subaccountId
    }, {
        user: currentUser._id,
        subaccount: subaccountId,
        status: 'pending',
        confirmationToken: confirmationToken
    }, {
        new: true,
        upsert: true
    });

}


exports.confirm = async (req, res) => {
    const confirmationToken = req.params.confirmationToken;
    let subAccount = await UserSubaccount.findOne({
        confirmationToken: confirmationToken
    });
    if (!confirmationToken || !subAccount) {
        return res.status(400).json({
            message: 'Invalid confirmation token!'
        });
    }
    try {
        let updatedSubAccount = await UserSubaccount.findOneAndUpdate({
            _id: subAccount._id
        }, {
            status: 'active'
        });
        updatedSubAccount.status = 'active';
        res.status(200).json({
            subAccount: updatedSubAccount
        });
    } catch (e) {
        handleError(res, e);
    }
}

exports.create = async (req, res) => {

    let userExists = await User.findOne({
        email: req.body.email
    });
    let subAccountExists;
    if (userExists) {
        subAccountExists = await UserSubaccount.findOne({
            subaccount: userExists._id
        });
    }



    if (!req.body.email) {
        return res.status(400).json({
            message: 'Email field is required'
        });
    }

    if (subAccountExists) {
        return res.status(200).json({
            status: 'failed',
            message: 'Subaccount already exists.'
        });
    } else {
        try {
            let results;
            if (req.body.orgId) {
       
                results = await this.createUserSubaccount(req.body.email, req.user, req.body.orgId);
            } else {
         
                results = await this.createUserSubaccount(req.body.email, req.user);
            }
            res.status(200).json(results);
        } catch (e) {
            handleError(res, e);
        }
    }


}

exports.destroyUserSubaccount = async (id) => {
    return UserSubaccount.findOneAndDelete({
        _id: id
    });
}

exports.destroy = async (req, res) => {
    if (!req.params.id) {
        return res.status(400).json({
            message: 'id parameter is required'
        });
    }
    try {
        await this.destroyUserSubaccount(req.params.id);
        res.status(200).json({
            message: 'success'
        });
    } catch (e) {
        handleError(res, e);
    }
}