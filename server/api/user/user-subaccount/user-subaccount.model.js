'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSubaccountSchema = new Schema({
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    subaccount: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    confirmationToken: { type: String },
    created: { type: Date, default: Date.now },
    status: { type: String, enum: ['active', 'pending', 'inactive', 'block'], default: 'pending' }
});

UserSubaccountSchema.index({ user: 1, subaccount: 1 }, { unique: true });

module.exports = mongoose.model('UserSubaccount', UserSubaccountSchema);
