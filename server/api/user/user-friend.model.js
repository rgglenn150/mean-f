'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
let utils = require('../utils/utils');

var UserFriendSchema = new Schema({
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    friend: {
        type: Schema.ObjectId,
        ref: 'User',
        default: utils.createUuidObjectId
    },
    friendEmail: String,
    token: String,
    relationship: { type: String, enum: ['friend', 'coworker', 'employee', 'supervisor'], default:'friend'},
    created: { type: Date, default: Date.now },
    status: { type: String, enum: ['active', 'pending', 'block'], default:'pending'}
});

UserFriendSchema.index({user: 1, friend: 1}, { unique: true });

module.exports = mongoose.model('UserFriend', UserFriendSchema);
