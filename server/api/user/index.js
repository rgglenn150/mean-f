'use strict';

var express = require('express');
var controller = require('./user.controller');
var UserFriendCtrl = require('./user-friend.controller');
var auth = require('../../auth/auth.middleware');

var router = express.Router();

router.use('/subaccount', require('./user-subaccount'));



router.get('/friend', auth.checkAuthenticated, UserFriendCtrl.index);
router.get('/friend/pending', auth.checkAuthenticated, UserFriendCtrl.getPending);
router.post('/friend/invite', auth.checkAuthenticated, UserFriendCtrl.inviteUser);
router.post('/friend/complete/:id', auth.checkAuthenticated, UserFriendCtrl.completeInviteRoute);
router.delete('/friend/:id', auth.checkAuthenticated, UserFriendCtrl.removeFriend);
router.get('/friend/:id', auth.checkAuthenticated, UserFriendCtrl.index);


router.get('/revokeApiKey', auth.checkAuthenticated, controller.revokeApiKey);
router.get('/generateApiKey', auth.checkAuthenticated, controller.generateApiKey);
router.get('/showBySuperAdmin/:id', auth.hasRole('superadmin'), controller.showBySuperAdmin);

//Recover forgot password
router.post('/resetPassword', controller.resetpassword);
router.post('/recoverPassword', controller.recoverChangePassword);


//Username
router.post('/username',auth.checkAuthenticated, controller.setUsername);
router.get('/username', controller.getUsername);
router.get('/profile/:username', controller.getUserByUsername);

router.get('/', auth.hasRole('superadmin'), controller.index); 
router.put('/fcm', auth.checkAuthenticated, controller.updateFCMToken);
router.get('/fcm', auth.checkAuthenticated, controller.getFCMToken);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/me', auth.checkAuthenticated, controller.me);
router.put('/me', auth.checkAuthenticated, controller.updateMe);
router.put('/:id/password', auth.checkAuthenticated, controller.changePassword);
router.get('/:id', auth.checkAuthenticated, controller.show);
router.post('/', controller.register);

router.put('/:id', auth.hasRole('superadmin'), controller.update);

module.exports = router;
