let fs = require('fs');
let config = require('../../config/environment/index');
let utils = require('../utils/utils');
let EmailCtrl = require('./email.controller')

/*

Lets document this a bit

getEmailTemplate
parameters - options (object) -

 */

let readFilePromise = function(file) {
    return new Promise(function(ok, notOk) {
        fs.readFile(file, function(err, data) {
            if (err) {
                notOk(err)
            } else {
                ok(data)
            }
        })
    })
}

let mixInParamters = function(options = {}) {
    return function(html){
        return new Promise((resolve, reject)=> {
            try {
                let htmlString = html.toString();
                for (let property in options) {
                    if (options.hasOwnProperty(property)) {
                        htmlString = utils.replaceAll(htmlString, '{{' + property + '}}', options[property]);
                    }
                }
                //let buf = Buffer.from(htmlString, 'utf8');
                return resolve(htmlString);
            } catch(e) {
                return reject(e);
            }
        });
    }

};

let _getOptionsForTemplate = function(templateName){
    let templateOptions = require('./email-template.options');
    return !templateOptions.hasOwnProperty(templateName) ? templateOptions['generic'] : templateOptions[templateName];
};

let getEmailTemplate = function(templateName, options = {}) {
    options = !options ? _getOptionsForTemplate(templateName) : Object.assign({}, _getOptionsForTemplate(templateName), options);
    return readFilePromise(__dirname + '/templates/' + options['templateName'])
        .then(mixInParamters(options));
};

/*
    Test example: http://localhost:6969/api/email/youarein
 */
let testHtml = function(req, res) {
    return getEmailTemplate(req.params.templateName).then((html)=>{
        res.set('Content-Type', 'text/html');
        //EmailCtrl.comejoin('john', 'sean.alan.thomas@gmail.com');
        let buf = Buffer.from(html, 'utf8');
        return res.send(buf);
    }).catch((e)=>{
        console.error(e);
    });
};

module.exports = {
    readFilePromise: readFilePromise,
    mixInParamters: mixInParamters,
    getEmailTemplate: getEmailTemplate,
    testHtml: testHtml
}