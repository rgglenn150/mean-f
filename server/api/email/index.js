'use strict';

var express = require('express');
var controller = require('./email-template.controller');

var router = express.Router();

/*
    http://localhost:6969/api/email/generic
    http://localhost:6969/api/email/resetpassword
    http://localhost:6969/api/email/youarein
    http://localhost:6969/api/email/comejoin
*/

router.get('/:templateName', controller.testHtml);

module.exports = router;
