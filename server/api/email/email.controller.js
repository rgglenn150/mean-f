var config = require('../../config/environment/index'),
    Bluebird = require('bluebird'),
    helper = require('sendgrid').mail,
    EmailTemplateCtrl = require('./email-template.controller');

exports.resetPassword = function(name, email, token) {
    let subject = "Reset password Request";
    let link = config.frontEndApp + "/recoverpassword/" + token;
    return EmailTemplateCtrl.getEmailTemplate('resetpassword',{ link: link }).then((html)=>{
        return emailHelper(config.systemEmail, config.systemName, email, subject, html, 'html');
    });
};

exports.alertCancel = function(user, product, newPlan, oldPlan, reason, comments) {
    var subject = (user.name || 'Sales Bath User') +" just did: " + newPlan + " on " + config.companyName + "!";
    var text = "<p>Hi,</p><p>" + (user.name || 'Sales Bath User') + ", with email: " + user.email + " with " + product + " just got put on " + newPlan + " from " + oldPlan + "</p><p>" + reason + "</p><p>" + comments + "</p><p>Regards,</p><p>~" + config.companyName + " team</p>";
    return emailHelper(config.systemEmail, config.systemName, config.systemEmail, subject, text, 'html');
};

exports.alertAdminUpgrade = function(user, newPlan, oldPlan) {
    var text = "<p>Hi,</p><p>" + user.name + ", with email: " + user.email + " just upgraded from " + oldPlan + " to " + newPlan + "</p><p></p><p>Regards,</p><p>~" + config.companyName + " team</p>";
    var subject = user.name+" upgraded on " + config.companyName + "!";
    return emailHelper(config.systemEmail, config.systemName, config.systemEmail, subject, text, 'html');
};
//exports.alertAdminUpgrade


exports.send = function(fromName, fromEmail, list, subject, text) {
    return emailHelper(fromEmail, fromName, config.systemEmail, subject, text);
};

//Just for sending to ourselves
exports.sendMessage = function(subject, text) {
    return emailHelper(config.systemEmail, config.systemName, config.systemEmail, subject, text);
};

exports.sendMessageSupport = function(subject, text) {
    //TODO set reply headers
    return emailHelper(config.systemEmail, config.systemName, config.supportEmail, subject, text);
};

exports.sendMessageDevelopment = function(subject, text) {
    return emailHelper(config.systemEmail, config.systemName, config.developerEmail, subject, text);
};

exports.confirmEmailSubaccount = function(user, email, token){
    let subject = "Confirm Email Subaccount!";
    return EmailTemplateCtrl.getEmailTemplate('addsubaccount',{
        link: config.frontEndApp + '/sub-account/confirm/' + token,
        p: "Looks like " + user.firstName + " " + user.lastName + " wants you to come share the experience. This is the pace where your accomplishments get rewarded."
    }).then((html)=> {
        return emailHelper(config.systemEmail, config.systemName, email, subject, html, 'html');
    });
};

exports.confirmEmailRegistration = function( email, token){
    let subject = "Confirm Email Registration!";
    return EmailTemplateCtrl.getEmailTemplate('registration',{
        link: config.frontEndApp + '/account/confirm/' + token,
        p: "Click the link below now that we know you are you. The world of Whoa awaits. Make, send and earn on."
    }).then((html)=> {
        return emailHelper(config.systemEmail, config.systemName, email, subject, html, 'html');
    });
};

exports.newOrg = function( email, details){
    let subject = "New Org Registration!";
    return EmailTemplateCtrl.getEmailTemplate('newOrg',{
        p: details.submittedBy+" added the organization \""+details.orgName+"\". Activate the organization on the superadmin dashboard."
    }).then((html)=> {
        return emailHelper(config.systemEmail, config.systemName, email, subject, html, 'html');
    });
};

exports.inviteUser = function(user, email, token){
    let subject = "You've been tapped!";
    return EmailTemplateCtrl.getEmailTemplate('comejoin',{
        link: config.frontEndApp + '/register/' + token,
        p: "Looks like " + user.firstName + " " + user.lastName + " wants you to come share the experience. This is the pace where your accomplishments get rewarded."
    }).then((html)=> {
        return emailHelper(config.systemEmail, config.systemName, email, subject, html, 'html');
    });
};

exports.inviteParticipant = function(email, eventId) {
    let subject = "You're Invited! A special event awaits";
    return EmailTemplateCtrl.getEmailTemplate('inviteparticipant', {
        link: config.frontEndApp + '/events/' + eventId
    }).then((html) => {
        return emailHelper(config.systemEmail, config.systemName, email, subject, html, 'html');
    });
};

exports.invitePromoter = function(email, eventId) {
    let subject = "You're invited to be a Promoter! A special event awaits";
    return EmailTemplateCtrl.getEmailTemplate('inviteparticipant', {
        link: config.frontEndApp + '/events/' + eventId,
        h1: 'You\'re invited to be a Promoter! A special event awaits.'
    }).then((html) => {
        return emailHelper(config.systemEmail, config.systemName, email, subject, html, 'html');
    });
};

exports.friendRequestUser = function(user, email, relationship){
    if(relationship === 'supervisor'){
        relationship = 'employee';
    }else if(relationship === 'employee'){
        relationship = 'supervisor';
    }
    var subject = "Friend Request from " + user.firstName + " " + user.lastName;
    return EmailTemplateCtrl.getEmailTemplate('comejoin',{
        h1: 'Friend Request',
        p: "Looks like " + user.firstName + " " + user.lastName + " is requesting to be your " + relationship + " and wants you to come share the experience. This is the pace where your accomplishments get rewarded.",
        link: config.frontEndApp + '/friends',
        buttonCallToAction: 'View Request'
    }).then((html)=> {
        return emailHelper(config.systemEmail, config.systemName, email, subject, html, 'html');
    });
}

exports.errorMessage = function(err) {
    return emailHelper(config.systemEmail, config.systemName, config.developerEmail, config.companyName + " crash", config.companyName + " crash: " + err + ' '+ err.stack);
};

exports.sendSmtpMessage = function(subject, body, emailAccount) {
    var deferred = Q.defer();
    var smtp;
    if(typeof emailAccount.service != 'undefined' && emailAccount.service != 'Smtp'){
        smtp = {
            /*host: email.smtpServer,
             port: email.smtpPort,*/
            service: 'gmail',
            auth: {
                user: emailAccount.username,
                pass: decrypt(emailAccount.hashedPassword)
            }
        }
    }else{
        smtp = {
            host: emailAccount.smtpServer,
            port: emailAccount.smtpPort,
            auth: {
                user: emailAccount.username,
                pass: decrypt(emailAccount.hashedPassword)
            }
        }
    }
    var transporter = nodemailer.createTransport(smtpTransport(smtp));
    var emailTo = req.user.email;
    if(process.env.NODE_ENV == 'development') {
        emailTo = "sean.alan.thomas@gmail.com";
    }
    transporter.sendMail({
        to: emailTo,
        from: emailAccount.from,
        subject: subject,
        text: message
    }, function(error){
        if(error){
            console.log(error);
            deferred.reject(error);
        } else {
            deferred.resolve('success');
      
        }
    });

    return deferred.promise;
};

exports.welcomeEmail = function(toEmail){
    var text = "Hey there!\n\nWelcome to " + config.companyName + ". This is a confirmation that you have signed up for " +config.companyName + ".\n\nThx ahead,\n\nLexi";
    var subject = "Welcome to " + config.companyName;
    return emailHelper(config.systemEmail, config.systemName, toEmail, subject, text);
};

exports.dunning = function(toEmail){
    var text = "Hey there!\n\nAs a heads up, the payment information you have on file for your " + config.companyName + " subscription is no longer going through.\n\nIf you got 30 seconds, would you mind updating your payment information?\n\nUpdate here: https://salesbath.com/start\n\nLet us know if you have any questions.\n\nThx ahead,\n\nLexi";
    var subject = "Your " + config.companyName + " payment info is expired";
    return emailHelper(config.systemEmail, config.systemName, toEmail, subject, text);
};

exports.email = function(fromEmail, fromName, toEmail, subject, text){
    return emailHelper(fromEmail, fromName, toEmail, subject, text);
}

function emailHelper(fromEmail, fromName, toEmail, subject, text, textType){
    textType = typeof textType == 'undefined' ? "text/plain" : "text/html";
    return (new Bluebird(function(resolve, reject) {
        var from_email = new helper.Email(fromEmail, fromName);
        var to_email = new helper.Email(toEmail);
        var content = new helper.Content(textType, text);
        var mail = new helper.Mail(from_email, subject, to_email, content);

        var sg = require('sendgrid').SendGrid(config.sendgridApi)
        var requestBody = mail.toJSON()
        var request = sg.emptyRequest()
        request.method = 'POST'
        request.path = '/v3/mail/send'
        request.body = requestBody
        sg.API(request, function (response) {
            if(response && response.statusCode !== 202){
                console.error(response)
                return reject(response);
            }
         
            return resolve(response.body)
        })
    }));
}
