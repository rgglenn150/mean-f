'use strict';

var express = require('express');
var controller = require('./badge.controller');
var BadgeDefaultCtrl = require('./badge-default.controller');
var BadgeUploadCtrl = require('./badge-upload.controller');
var auth = require('../../auth/auth.middleware');

var router = express.Router();

router.get('/default', auth.checkAuthenticated, BadgeDefaultCtrl.index);
router.get('/default/:id', auth.checkAuthenticated, BadgeDefaultCtrl.index);

router.post('/upload', auth.checkAuthenticated, BadgeUploadCtrl.upload);

router.get('/me', auth.checkAuthenticated, controller.getMyBadges);
router.get('/me/pending', auth.checkAuthenticated, controller.getMyPendingBadges);
router.get('/me/accept/:id', auth.checkAuthenticated, controller.acceptBadge);
router.post('/me/reject/:id', auth.checkAuthenticated, controller.rejectBadge);
router.post('/award', auth.checkAuthenticated, controller.awardBadge);
router.get('/user',controller.getUserBadges);
router.get('/user-created',controller.getUserCreatedBadges);

// not used for now. 
router.get('/search/:type',auth.checkAuthenticated,controller.search);

router.get('/', auth.checkAuthenticated, controller.index);
router.get('/:id', auth.checkAuthenticated, controller.show);
router.post('/', auth.checkAuthenticated, controller.create);
router.put('/:id', auth.checkAuthenticated, controller.update);
router.delete('/:id', auth.checkAuthenticated, controller.destroy);

module.exports = router;
