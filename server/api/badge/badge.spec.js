var config = require('../../config/environment');
var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();
var BadgeController = require('./badge.controller');
let TestingService = require('../../test/test.service')
let request = require('supertest');
let Company = require('../company/company.model');
//let app = require('../lib/server')(config);
let Badge = require('./badge.model');
let BadgeUser = require('./badge-user.model');

module.exports = function(app) {

    describe('Badge Tests', () => {
        let user;
        let token;
        let testingUserData;
        let testingBadge;
        let badge = {
            name: "The Greatest Showman",
            image: "image.com",
            description: "A testing badge"
        };
        let company = {
            name: "The Greatest Showman",
            image: "image.com",
            description: "A testing badge"
        };

        async function cleanup(){
            return Promise.all([
                Company.deleteMany({}),
                Badge.deleteMany({}),
                BadgeUser.deleteMany({})
            ]);
        }

        before(async () => {
            await cleanup();
            /*await loadFixture('user');
            user = await User.findOne({email: authUser.email});
            expect(user).to.not.be.null;*/
            user = await TestingService.getUser();
            company.user = user._id;
            expect(user).to.not.be.null;
            token = TestingService.getToken(user);
            expect(token).to.not.be.null;
            testingUserData = TestingService.getTestingUserData();
            company = await Company.create(company);
            testingBadge = await Badge.create(badge);
            badge.company = company._id;
            await Badge.create(badge);

        });

        after(async () => {
            await cleanup();
        });

        describe('POST /api/badge', () => {
            it('Should create a badge with a 200 response', async () => {
                const response = await request(app).post('/api/badge')
                    .set('Authorization', 'Bearer ' + token)
                    .send(badge);
                expect(response).to.have.property('status').equal(200);
            });
        });

        describe('GET /api/badge', () => {
            it('Should get badges', async () => {
                const response = await request(app).get('/api/badge')
                    .set('Authorization', 'Bearer ' + token)
                    //.send();

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('array');
            });

            it('Should get badges by company', async () => {
                const response = await request(app).get('/api/badge?company=' + company._id)
                    .set('Authorization', 'Bearer ' + token)
                //.send();
                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('array');
                expect(response.body).to.have.lengthOf(1);
            });
        });

        describe('GET /api/badge/:id', () => {
            it('Should get badge by id', async () => {
                const response = await request(app).get('/api/badge/' + testingBadge._id)
                    .set('Authorization', 'Bearer ' + token)
                //.send();

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
            });
        });

        describe('PUT /api/badge/:id', () => {
            it('Should update a badge by id', async () => {
                const response = await request(app).put('/api/badge/' + testingBadge._id)
                    .set('Authorization', 'Bearer ' + token)
                .send({description: 'worked'});

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('description');
                expect(response.body.description).to.equal('worked');

            });
        });

        describe('DEL /api/badge/:id', () => {
            it('Should get badge by id', async () => {
                const response = await request(app).del('/api/badge/' + testingBadge._id)
                    .set('Authorization', 'Bearer ' + token)

                expect(response).to.have.property('status').equal(200);
            });
        });

        describe('POST /api/badge/award', () => {
            it('Should fail when trying to award a badge without a badge _id, expect 422 response with badge required as the message ', async () => {
                const response = await request(app).post('/api/badge/award')
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        company: company._id,
                        user: user._id
                    });
                expect(response).to.have.property('status').equal(422);
                expect(response).to.have.property('body').to.not.be.null;
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('message').equal('badge required');
            });
            it('Should fail when trying to award a badge without a user _id, expect 422 response with user required as the message ', async () => {
                const response = await request(app).post('/api/badge/award')
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        company: company._id
                    });
                expect(response).to.have.property('status').equal(422);
                expect(response).to.have.property('body').to.not.be.null;
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('message').equal('user required');
            });

            it('Should award a badge with a 200 response to a user', async () => {

                const response = await request(app).post('/api/badge/award')
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        badge: testingBadge._id,
                        company: company._id,
                        user: user._id
                    });

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body').to.not.be.null;
                expect(response.body).to.be.an('object');
                expect(response.body.user).to.not.be.null
            });

        });

        describe('GET /api/badge/me', () => {
            beforeEach(async () => {
                await BadgeUser.deleteMany({});
            });


            it('Should get all the badges of the authenticated user with a 200 response', async () => {
                let newBadge = await Badge.create({ name: 'foo', description: 'foo description'});
                await BadgeUser.create({ user: user._id, badge: newBadge._id, createdByUser: user._id });
                await BadgeUser.create({ user: user._id, badge: newBadge._id, createdByUser: user._id });
                const response = await request(app).get('/api/badge/me')
                    .set('Authorization', 'Bearer ' + token);

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('array');
                expect(response.body).to.have.lengthOf(2);
            });

            it('Should test the populate method of badges of the authenticated user with a 200 response', async () => {
                let newBadge = await Badge.create({ name: 'foo', description: 'foo description'});
                await BadgeUser.create({ user: user._id, badge: newBadge._id, createdByUser: user._id });
                await BadgeUser.create({ user: user._id, badge: newBadge._id, createdByUser: user._id });
                const response = await request(app).get('/api/badge/me?populate=badge')
                    .set('Authorization', 'Bearer ' + token);

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('array');
                expect(response.body).to.have.lengthOf(2);
                expect(response.body[0]).to.have.property('badge').to.be.an('object');
                expect(response.body[0].badge).to.have.property('_id').equal(newBadge._id.toString());
            });
        });

        describe('POST /api/badge/default', () => {
            it('Should create a badge with a 200 response and should be greater 2', async () => {
                const response = await request(app).get('/api/badge/default')
                    .set('Authorization', 'Bearer ' + token)
                    .send(badge);
                expect(response).to.have.property('status').equal(200);
                expect(response.body).to.be.an('array');
                //TODO ask if it seeded correctly and if its greater than 2, I have no internet on this plane
            });
        });

    });
};