let multiparty = require('multiparty');
let AWS = require('aws-sdk');
let config = require('../../config/environment');
let utils = require('../utils/utils');

function emptyDirectory(folder, callback){
    var params = {
        Bucket: config.bucket,
        Prefix: folder // 'folder/'
    };
    let s3Client = new AWS.S3({
        accessKeyId: config.accessKeyId,
        secretAccessKey: config.secretAccessKey
    });

    return new Promise((resolve, reject) => {
        s3Client.listObjects(params, function (err, data) {
            if (err) return reject(err);

            if (data.Contents.length == 0) resolve({});

            params = {Bucket: config.bucket};
            params.Delete = {Objects: []};

            data.Contents.forEach(function (content) {
                params.Delete.Objects.push({Key: content.Key});
            });

            s3Client.deleteObjects(params, function (err, data) {
                if (err) return reject(err);
                if (data.Deleted.length == 1000) emptyBucket(callback);
                else resolve({});
            });
        });
    });
}


function removeImage(data){
    return new Promise((resolve, reject) => {
        let s3Client = new AWS.S3({
            accessKeyId: config.accessKeyId,
            secretAccessKey: config.secretAccessKey
        });
        s3Client.deleteObject(data, function (err, objectData) {
            if (err) {
                console.error(err);
                console.log('Error uploading data: ', objectData);
                return reject(err);
            } else {
                return resolve('');
            }
        });
    });
}

function uploadImage(data){
    return new Promise((resolve, reject) => {
        let s3Client = new AWS.S3({
            accessKeyId: config.accessKeyId,
            secretAccessKey: config.secretAccessKey
            // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Config.html#constructor-property
        });
        s3Client.putObject(data, function (err, objectData) {
            if (err) {
                console.error(err);
                console.log('Error uploading data: ', objectData);
                return reject(err);
            } else {
                return resolve("https://s3.amazonaws.com/" + config.bucket + '/' + data.Key);
            }
        });
    });
}

exports.uploadEventBadge = function(base64String, eventId){
    let buf = new Buffer.from(base64String.replace(/^data:image\/\w+;base64,/, ""),'base64');
    let destPath = 'event/' + eventId + '/social/' + utils.createUuid() + '.png';
    let data = {
        Bucket: config.bucket,
        Key: destPath,
        Body: buf,
        ACL: 'public-read',
        ContentEncoding: 'base64',
        ContentType: 'image/png'
    };
    let emptyDir = 'event/' + eventId + '/social/';
    return emptyDirectory(emptyDir).then(()=> {
        return uploadImage(data);
    });
}

exports.uploadSocialBadge = function(base64String, user) {
    let buf = new Buffer.from(base64String.replace(/^data:image\/\w+;base64,/, ""),'base64');
    let destPath = 'badges/' + user._id + '/social_' + utils.createUuid() + '.png';
    let data = {
        Bucket: config.bucket,
        Key: destPath,
        Body: buf,
        ACL: 'public-read',
        ContentEncoding: 'base64',
        ContentType: 'image/png'
    };
    return uploadImage(data);
};

exports.upload = function (req, res) {
    var form = new multiparty.Form();

    form.on('error', function(err) {
        //console.log('have ');
        console.log(err);
        //return res.sendStatus(500).json({message: 'Ops.. Something happened.'})
    });

    form.on('part', function(part) {
        try {
            part.on('error', function(e){
                return res.sendStatus(500).json({message: 'Ops.. Something happened.'})
            });
            let fileType = part.headers['content-type'] === 'image/jpeg' ? 'jpg' : 'png';
            if (part.headers['content-type'] !== 'image/jpeg' && part.headers['content-type'] !== 'image/png') {
                return res.sendStatus(500); //'Must be jpeg or png
            } else {
                //let destPath = 'badges/' + req.user._id + '/custom_' + utils.createUuid() + '.' + fileType;
                let data = {
                    Bucket: config.bucket,
                    Key: 'badges/' + req.user._id + '/custom_' + utils.createUuid() + '.' + fileType,
                    ACL: 'public-read',
                    Body: part,
                    ContentLength: part.byteCount
                };
                return uploadImage(data).then((finalDestination)=>{
                    return res.json({image: finalDestination});
                }).catch((e)=>{
                    console.error(e);
                    return res.sendStatus(500);
                })
            }
        } catch(e) {
            console.log(e);
        }
    });
    form.parse(req);

    /*var form = new IncomingForm();

    console.log(req)

    form.on('file', (field, file) => {
        console.log(field);
        // Do something with the file
        // e.g. save it to the database
        // you can access it using file.path
        console.log(file);
    });
    form.on('end', () => {
        res.json();
    });
    form.on('err', (e)=>{
        console.error(e)
    })
    form.parse(req);*/

    /*var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        console.log(fields)
        console.log(files)
        console.log(err);
        res.write('File uploaded');
        res.end();
    });*/

};