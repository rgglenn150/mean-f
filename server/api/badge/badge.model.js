'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var BadgeSchema = new Schema({
    name: String,
    image: String, // amazon s3 reference
    company: {
        type: Schema.ObjectId,
        ref: 'Company'
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    description: String,
    awarded_date: { type: Date, default: Date.now },
    campaign: {
        type: Schema.ObjectId,
        ref: 'Campaign'
    },
    prize: String,
    prizeDescription: String,
    makePrivate: { type: Boolean, default: false },
    turnEmailNotificationsOff: { type: Boolean, default: false },
    turnCommentsOff: { type: Boolean, default: false },
    path: String,
    settings: {}
});

module.exports = mongoose.model('Badge', BadgeSchema);