'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var BadgeUserSchema = new Schema({
    createdByUser: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    badge: {
        type: Schema.ObjectId,
        ref: 'Badge',
        required: true
    },
    company: {
        type: Schema.ObjectId,
        ref: 'Company'
    },
    campaign: {
        type: Schema.ObjectId,
        ref: 'Campaign'
    },
    status: {
        type: String,
        default: 'pending'
    },
    awarded_date: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('BadgeUser', BadgeUserSchema);