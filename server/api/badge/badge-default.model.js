'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var BadgeDefaultSchema = new Schema({
    name: String,
    bgColor: String,
    text: {},
    image: {},
    svg: String
});

module.exports = mongoose.model('BadgeDefault', BadgeDefaultSchema);
