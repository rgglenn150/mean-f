let config = require('../../config/environment');
let Badge = require('./badge.model');
let BadgeUser = require('./badge-user.model');
let utils = require('../utils/utils');
let ObjectID = require('mongodb').ObjectID;
let BadgeUploadCtrl = require('./badge-upload.controller');
let User = require('../user/user.model');


var validationError = function (res, err) {
    console.error(err);
    return res.status(422).json(err);
};

exports.index = async (req, res) => {
    let filter = req.query.company ? {
        company: ObjectID(req.query.company)
    } : {};
    if (req.query.campaign) {
        filter.campaign = req.query.campaign;
    } else {
        filter.user = req.user._id;
    }

    let promise = Badge.find(filter);
    let populateValues = req.query.populate ? req.query.populate.split(",") : [];

    if (populateValues.length > 0) {
        populateValues = checkPopulate(populateValues); //Security check
        promise = promise.populate(populateValues);
    }

    return promise.then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};

exports.getMyBadges = function (req, res) {
    let filter = {
        user: req.user._id,
        status: {
            $nin: ['pending']
        }

    };
    let promise = BadgeUser.find(filter);
    let populateValues = req.query.populate ? req.query.populate.split(",") : [];

    if (populateValues.length > 0) {
        populateValues = checkPopulate(populateValues); //Security check
        promise = promise.populate(populateValues);
    }

    return promise.then((data) => {
        res.set(200).json(data);
    });
};
exports.getMyPendingBadges = function (req, res) {
    let filter = {
        user: req.user._id,
        status: 'pending'
    };
    let promise = BadgeUser.find(filter);
    let populateValues = req.query.populate ? req.query.populate.split(",") : [];

    if (populateValues.length > 0) {
        populateValues = checkPopulate(populateValues); //Security check
        promise = promise.populate(populateValues);
    }

    return promise.then((data) => {
       
        res.set(200).json(data);
    });
};

exports.acceptBadge = function (req, res) {
   

    BadgeUser.updateOne({
        user: req.user._id,
        _id: req.params.id
    }, {
        status: 'active'
    }).then((result) => {
   
        res.json(result)
    }).catch(err => {
        res.status(500).json(err)

    })




};

exports.rejectBadge = function (req, res) {

    BadgeUser.Update(req.body._id, req.body).then((res) => {


        return res.status(200).json(res)

    }, function (err) {

        return res.status(500).json(err)
    });
};



exports.search = async (req, res) => {

    const searchQuery = req.query.search ? formatSearch(req.query.search, req.query.field) : null;
    searchQuery.user = req.user._id;



    let promise;
    if (req.params.type === 'createdBadges') {
        promise = Badge.find(
            searchQuery
        );
    } else if (req.params.type === 'myBadges') {

        promise = BadgeUser.find();

    }
    let populateValues = req.query.populate ? req.query.populate.split(",") : [];
    if (populateValues.length > 0) {
        populateValues = checkPopulate(populateValues); //Security check
        promise = promise.populate(populateValues);
    }


    return promise.then((data) => {
        res.set(200).json(data);
    });
};


function checkPopulate(results) {
    let allowedFields = ['company', 'user', 'badge'];
    return results
        .filter(e => allowedFields.includes(e))
        .map(e =>
            e === 'participants' ||
            e === 'user' ||
            e === 'promoters' ? {
                path: e,
                select: '-__v -salt -hashedPassword'
            } : {
                path: e
            }
        )
        .map(e => {
            return e.path === 'badge' ? {
                path: e.path,
                populate: {
                    path: 'user'
                }
            } : e
        })

}

exports.show = function (req, res) {
    let promise = Badge.findOne({
        _id: req.params.id
    });
    let populateValues = req.query.populate ? req.query.populate.split(",") : [];

    if (populateValues.length > 0) {
        populateValues = checkPopulate(populateValues); //Security check
        promise = promise.populate(populateValues);
    }
    return promise.then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};

function _getImage(req) {
    if (req.body.image && req.body.image.indexOf('data:image/png;base64') !== -1) {
        return BadgeUploadCtrl.uploadSocialBadge(req.body.image, req.user);
    } else {
        return Promise.resolve(req.body.image ? req.body.image : '');
    }
}

exports.create = function (req, res) {
    req.body.user = req.user._id;
    req.body.makePrivate = !!req.body.makePrivate; //if a string gets passed in or null
    req.body.turnCommentsOff = !!req.body.turnCommentsOff; //if a string gets passed in or null
    req.body.turnEmailNotificationsOff = !!req.body.turnEmailNotificationsOff; //if a string gets passed in or null
    return _getImage(req).then((destPath) => {
        req.body.image = destPath;
        return Badge.create(req.body);
    }).then((data) => {
        return res.json(data);
    }, (err) => {
        return validationError(res, err);
    }).catch((e) => {
        console.error(e);
        return res.sendStatus(500);
    });
};

exports.update = function (req, res) {
    return utils.updateObject(Badge, req.params.id, req.body).then(() => {
        return Badge.findOne({
            _id: req.params.id
        })
    }).then((data) => {
        return res.json(data)
    }).catch((e) => {
        console.error(e);
        return res.sendStatus(500);
    });
};

exports.destroy = function (req, res) {
    return Badge.deleteOne({
        _id: req.params.id
    }).then(() => {
        return res.sendStatus(200);
    }).catch((e) => {
        console.error(e);
        return res.sendStatus(500);
    });
}

exports.awardBadge = function (req, res) {
    req.body.createdByUser = req.user._id;
    if (!req.body.user) {
        return res.status(422).send({
            message: 'user required'
        });
    } else if (!req.body.badge) {
        return res.status(422).send({
            message: 'badge required'
        });
    } else {
        return BadgeUser.create(req.body).then((data) => {
            return res.set(200).json(data);
        }, (e) => {
            return validationError(res, e);
        }).catch((e) => {
            console.log(e);
            return res.sendStatus(500);
        });
    }
};




exports.getUserBadges = async (req, res) => {
    let filter;
    await User.findOne({
        username: req.query.username
    }).then(res => {
        filter = {
            user: res._id
        };
    });

    let promise = BadgeUser.find(filter);
    let populateValues = req.query.populate ? req.query.populate.split(",") : [];
    if (populateValues.length > 0) {
        populateValues = checkPopulate(populateValues); //Security check
        promise = promise.populate(populateValues);
    }

    return promise.then((data) => {
        res.set(200).json(data);
    });
};

exports.getUserCreatedBadges = async (req, res) => {
    let filter = req.query.company ? {
        company: ObjectID(req.query.company)
    } : {};
    if (req.query.campaign) {
        filter.campaign = req.query.campaign;
    } else {
        filter.username = req.query.username;
    }

    let promise;

    await User.findOne({
        username: filter.username
    }).then(res => {
        promise = Badge.find({
            user: res._id
        });
    });

    let populateValues = req.query.populate ? req.query.populate.split(",") : [];

    if (populateValues.length > 0) {
        populateValues = checkPopulate(populateValues); //Security check
        promise = promise.populate(populateValues);
    }

    return promise.then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};

let formatSearch = (search, field = null) => {
    let query = {};
    search = search.replace('\t', '');
    if (field === 'name') {
        query.name = {
            $regex: search,
            $options: 'i'
        }
    }
    return query;
}