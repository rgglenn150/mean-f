let BadgeDefault = require('./badge-default.model');

var validationError = function(res, err) {
    return res.status(422).json(err);
};

exports.index = function(req,res){
    return BadgeDefault.find({}).then((data)=>{
        return res.json(data);
    }, (err) => {
        validationError(res, err);
    }).catch((e)=>{
        console.log(e);
        return res.sendStatus(500);
    });
};

exports.show = function(req, res){
    return BadgeDefault.findOne({ _id: req.params.id }).then((data)=>{
        return res.json(data);
    }).catch((e)=>{
        console.log(e);
        return res.sendStatus(500);
    });
};