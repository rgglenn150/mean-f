let config = require('../../config/environment');
let Notification = require('./notification.model');
let utils = require('../utils/utils');
let ObjectID = require('mongodb').ObjectID;
var AuthMiddleware = require('../../auth/auth.middleware');


var validationError = function (res, err) {
    return res.status(422).json(err);
};

exports.index = function (req, res) {
    let filter = {
        user: req.user._id
    };
    //TODO if the functionality of a super user is involved, then we can unit test this. Leaving it out for now so we can focus on delivery of routes.
    /*if(AuthMiddleware.hasRoleSerial('superadmin', req.user) && req.query.user){
        filter = req.query.user ? { user: ObjectID(req.query.user) } : {};
    }*/
    return Notification.find(filter).then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};

exports.show = function (req, res) {
    return Notification.findOne({
        _id: req.params.id
    }).then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};

exports.view  = function( req,res){
    Notification.findOneAndUpdate({_id:req.body._id, user:req.user._id}, {read:true}).then((user) => {
        return res.status(200).json(user);
    }, function (err) {
        return res.status(500).json(err);
    });
}

exports.create = function (req, res) {

    return Notification.create(req.body).then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};

exports.update = function (req, res) {
    return utils.updateObject(Notification, req.params.id, req.body).then(() => {
        return Notification.findOne({
            _id: req.params.id
        })
    }).then((data) => {
        return res.json(data)
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};



exports.destroy = function (req, res) {
    return Notification.deleteOne({
        _id: req.params.id
    }).then(() => {
        return res.sendStatus(200);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
}