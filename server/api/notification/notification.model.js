'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var NotificationSchema = new Schema({
    message: String,
    read: {type: Boolean, default: false },
    path: String,
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    thumbnail: String,
    created: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Notification', NotificationSchema);