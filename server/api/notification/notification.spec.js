var config = require('../../config/environment');
var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();
var NotificationController = require('./notification.controller');
let TestingService = require('../../test/test.service')
let request = require('supertest');
let Company = require('../company/company.model');
//let app = require('../lib/server')(config);
let Notification = require('./notification.model');

module.exports = function(app) {

    describe('Notification Tests', () => {
        let user, token, testingUserData, testingNotification, testingNotification2;
        let notification = {
            message: "You have a new friend request",
            read: false,
            path: "link_to_route_goes_here"
        };

        before(async () => {
            user = await TestingService.getUser();
            notification.user = user._id;
            expect(user).to.not.be.null;
            token = TestingService.getToken(user);
            expect(token).to.not.be.null;
            testingUserData = TestingService.getTestingUserData();
            testingNotification = await Notification.create(notification);
        });

        after(async () => {
            await Notification.deleteMany({});
        });

        describe('POST /api/notification', () => {
            it('Should create a notification with a 200 response', async () => {
                const response = await request(app).post('/api/notification')
                    .set('Authorization', 'Bearer ' + token)
                    .send(notification);
                expect(response).to.have.property('status').equal(200);
            });
        });

        describe('GET /api/notification', () => {
            it('It Should get all notifications by authenticated user', async () => {
                const response = await request(app).get('/api/notification')
                    .set('Authorization', 'Bearer ' + token);

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('array');
                expect(response.body).to.have.lengthOf(2);
            });
        });

        describe('GET /api/notification/:id', () => {
            it('Should get notification by id', async () => {
                const response = await request(app).get('/api/notification/' + testingNotification._id)
                    .set('Authorization', 'Bearer ' + token);

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('message').equal(notification.message);
                expect(response.body).to.have.property('path').equal(notification.path);
                expect(response.body).to.have.property('read').equal(notification.read);
                expect(response.body).to.have.property('user').equal(user._id.toString());
            });
        });

        describe('PUT /api/notification/:id', () => {
            it('Should update a notification by id', async () => {
                const response = await request(app).put('/api/notification/' + testingNotification._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({message: 'worked'});

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('message').equal('worked');
                expect(response.body).to.have.property('path').equal(notification.path);
                expect(response.body).to.have.property('read').equal(notification.read);
                expect(response.body.message).to.equal('worked');

            });
        });

        describe('DEL /api/notification/:id', () => {
            it('Should get notification by id', async () => {
                const response = await request(app).del('/api/notification/' + testingNotification._id)
                    .set('Authorization', 'Bearer ' + token);

                expect(response).to.have.property('status').equal(200);
            });
        });

    });
};