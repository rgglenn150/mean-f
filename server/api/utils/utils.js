var Bluebird = require('bluebird');
var _ = require('lodash');
var ObjectID = require('mongodb').ObjectID;
let fs = require('fs');

function updateObject(model, objectId, object) {
    return new Bluebird((resolve, reject) => {
        model.findById(ObjectID(objectId), function (err, myObject) {
            if (err) {
                return reject(err)
            }
            if (!myObject || typeof myObject == "undefined" || myObject == null) {
                return reject("Object not found.")
            } else {
                var updated = _.extend(myObject, object);
                updated.save(function (err, updatedObject) {
                    if (err) {
                        reject(err)
                    }
                    return resolve(updatedObject);
                });
            }
        });
    });
};

function roundToFith(num) {
    return Math.round(num * (100000)) / (100000);
}

function roundToTenth(num) {
    return Math.round(num * 10) / 10;
}

function round5Ceil(x) {
    return Math.ceil(x / 5) * 5;
}
//Rounds to 2 decimals with a multiple of 5
function round5(num) {
    return Math.round((round5Ceil(num * 100) / 100) * 100) / 100;
}

function createUuid() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });
    return uuid;
}

function createUuidObjectId() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxxxxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });
    return ObjectID(uuid);
}

function _round2Decimal(num) {
    return Math.round(num * 100) / 100;

}

function writeOutputFile(logs, name) {
    //let outMACD = hist.outMACD.map((elem)=> { return { date:null, data:elem, ema:null }; });

    //var function_desc = talib.explain("EMA");
    //console.dir(function_desc);

    //console.log(getEMA(outMACD, outMACD.length - 1, 9));

    var writeStream = fs.createWriteStream(__dirname + '/../../output/' + name + ".txt");

    for (var i in logs) {
        let log = typeof logs[i] === 'object' ? JSON.stringify(logs[i]) : logs[i];
        writeStream.write(log + '\r\n'); //get the first line and write it
    }

}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

let replaceAll = function (target, search, replacement) {
    return target.replace(new RegExp(search, 'g'), replacement);
};

module.exports = {
    updateObject: updateObject,
    roundToTenth: roundToTenth,
    round5: round5,
    createUuid: createUuid,
    createUuidObjectId: createUuidObjectId,
    roundToFith: roundToFith,
    writeOutputFile: writeOutputFile,
    isEmpty: isEmpty,
    replaceAll: replaceAll
}