let Organization = require('./organization.model')
let EmailCtrl = require("../email/email.controller");
let utils = require('../utils/utils');
let config = require('../../config/environment');

exports.index = function (req, res) {
    return Organization.find({status:{$nin:["inactive"]}}).then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};

exports.getOrg =function(req,res){
    return Organization.find({_id: req.params.id}).then((data) => {
        return res.json(data[0]);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
}


exports.create = function (req, res) {
    req.body.status = "pending";
    return Organization.create(req.body).then((data) => {
        if (config.env !== 'test') {
            //EmailCtrl.newOrg( "vetting@badgehero.com", data);
           // EmailCtrl.newOrg( "bill@bringonthegood.com", data);
        }
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};


exports.search = async (req, res) => {
    const searchQuery = req.query.search ? formatSearch(req.query.search, req.query.field) : null;

    filter = {
        status:{$nin:['inactive']},
        ...searchQuery
    };
    return Organization.find(filter).then((data) => {
        return res.json(data); 
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};

let formatSearch = (search, field = null) => {
    let query = {};
    search = search.replace('\t', '');
    if (field === 'orgName') {
        query.orgName = {
            $regex: search,
            $options: 'i'
        }
    }
    return query;
}