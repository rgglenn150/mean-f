'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var OrganizationSchema = new Schema({
    orgName: String,
    officeAddress: String,
    dateEstablished: Date,
    description: String,
    website: String,
    submittedBy: String,
    position: String,
    email: String,
    status: String,
    companyLogo: {
        type: String,
        default: null
    }, 
});


module.exports = mongoose.model('Organization', OrganizationSchema);