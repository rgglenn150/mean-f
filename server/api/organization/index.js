'use strict';

var express = require('express');
var controller = require('./organization.controller');
var auth = require('../../auth/auth.middleware');

var router = express.Router();

router.get('/', auth.checkAuthenticated, controller.index);
router.post('/', controller.create);
router.get('/search', controller.search);
router.get('/:id', auth.checkAuthenticated, controller.getOrg);




module.exports = router;