let Company = require('./company.model')

exports.index = function (req, res) {


};


exports.create = function (req, res) {
    if (!req.body.user) {
        req.body.user = req.user._id;
    }
    return Company.create(req.body).then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};