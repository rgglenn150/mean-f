'use strict';

var express = require('express');
var controller = require('./company.controller');
var auth = require('../../auth/auth.middleware');

var router = express.Router();

router.get('/', auth.checkAuthenticated, controller.index);
router.post('/', auth.checkAuthenticated, controller.create);

module.exports = router;
