'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var CompanySchema = new Schema({
    name: String,
    image: String,
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    description: String,
    awarded_date: Date
});


module.exports = mongoose.model('Company', CompanySchema);
