var config = require('../../config/environment');
var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();
var CampaignController = require('./campaign.controller');
let TestingService = require('../../test/test.service')
let request = require('supertest');
let Company = require('../company/company.model');
//let app = require('../lib/server')(config);
let Campaign = require('./campaign.model');

module.exports = function(app) {

    describe('Campaign Tests', () => {
        let user, token, testingUserData, testingCampaign, testingCampaign2;
        let campaign = {
            title: "The Greatest Showman Campaign",
            description: "A testing campaign"
        };
        let company = {
            title: "The Greatest Showman",
            image: "image.com",
            description: "A testing company"
        };

        before(async () => {
            /*await loadFixture('user');
            user = await User.findOne({email: authUser.email});
            expect(user).to.not.be.null;*/
            user = await TestingService.getUser();
            company.user = user._id;
            expect(user).to.not.be.null;
            token = TestingService.getToken(user);
            expect(token).to.not.be.null;
            testingUserData = TestingService.getTestingUserData();
            company = await Company.create(company);
            testingCampaign = await Campaign.create(campaign);
            campaign.user = user._id;
            await Campaign.create(campaign);
            campaign.company = company._id;
            testingCampaign2 = await Campaign.create(campaign);

        });

        after(async () => {
            await Company.deleteMany({});
            await Campaign.deleteMany({});
        });

        describe('POST /api/campaign', () => {
            it('Should create a campaign with a 200 response', async () => {
                const response = await request(app).post('/api/campaign')
                    .set('Authorization', 'Bearer ' + token)
                    .send(campaign);
                expect(response).to.have.property('status').equal(200);
            });
        });

        describe('GET /api/campaign', () => {
            it('It Should get all campaigns by authenticated user', async () => {
                const response = await request(app).get('/api/campaign')
                    .set('Authorization', 'Bearer ' + token)
                //.send();

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('array');
                expect(response.body).to.have.lengthOf(3);
            });

            it('Should get all campaigns by company', async () => {
                const response = await request(app).get('/api/campaign?company=' + company._id)
                    .set('Authorization', 'Bearer ' + token)
                //.send();
                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('array');
                expect(response.body).to.have.lengthOf(2);
            });

        });

        describe('GET /api/campaign/:id', () => {
            it('Should get campaign by id', async () => {
                const response = await request(app).get('/api/campaign/' + testingCampaign2._id)
                    .set('Authorization', 'Bearer ' + token)
                //.send();

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('title').equal(campaign.title);
                expect(response.body).to.have.property('description').equal(campaign.description);
                expect(response.body).to.have.property('company').equal(company._id.toString());
                expect(response.body).to.have.property('user').equal(user._id.toString());
            });
        });

        describe('PUT /api/campaign/:id', () => {
            it('Should update a campaign by id', async () => {
                const response = await request(app).put('/api/campaign/' + testingCampaign._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({description: 'worked'});

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('description');
                expect(response.body.description).to.equal('worked');

            });
        });

        describe('DEL /api/campaign/:id', () => {
            it('Should get campaign by id', async () => {
                const response = await request(app).del('/api/campaign/' + testingCampaign._id)
                    .set('Authorization', 'Bearer ' + token)
                //.send();

                expect(response).to.have.property('status').equal(200);
            });
        });

    });
};