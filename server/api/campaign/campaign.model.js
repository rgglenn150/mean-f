'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var CampaignSchema = new Schema({
    title: String,
    description: String,
    company: {
        type: Schema.ObjectId,
        ref: 'Company'
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    frequency: { type: String, enum: ['daily', 'weekly', 'monthly', 'yearly'], default:'daily'},
    rulesets: String, //TODO will be programmed in detail later
    created: { type: Date, default: Date.now },
    expire: { type: Date },
});

module.exports = mongoose.model('Campaign', CampaignSchema);
