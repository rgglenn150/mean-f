
let config = require('../../config/environment');
let Campaign = require('./campaign.model');
let utils = require('../utils/utils');
let ObjectID = require('mongodb').ObjectID;
var AuthMiddleware = require('../../auth/auth.middleware');


var validationError = function(res, err) {
    return res.status(422).json(err);
};

exports.index = function(req, res){
    let filter = { user: req.user._id };
    //TODO if the functionality of a super user is involved, then we can unit test this. Leaving it out for now so we can focus on delivery of routes.
    /*if(AuthMiddleware.hasRoleSerial('superadmin', req.user) && req.query.user){
        filter = req.query.user ? { user: ObjectID(req.query.user) } : {};
    }*/

    if(req.query.company){
        filter.company = req.query.company;
    }
    return Campaign.find(filter).then((data) => {
        return res.json(data);
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(500);
    });
};

exports.show = function(req, res){
    return Campaign.findOne({ _id: req.params.id }).then((data)=>{
        return res.json(data);
    }).catch((e)=>{
        console.log(e);
        return res.sendStatus(500);
    });
};

exports.create = function(req, res) {
    if(!req.body.user) {
        req.body.user = req.user._id;
    }
    return Campaign.create( req.body ).then((data)=>{
        return res.json(data);
    }).catch((e)=>{
        console.log(e);
        return res.sendStatus(500);
    });
};

exports.update = function(req, res){
    return utils.updateObject(Campaign, req.params.id, req.body).then(()=>{
        return Campaign.findOne({ _id: req.params.id })
    }).then((data)=>{
        return res.json(data)
    }).catch((e)=>{
        console.log(e);
        return res.sendStatus(500);
    });
};

exports.destroy = function(req, res){
    return Campaign.deleteOne({ _id: req.params.id }).then(()=>{
        return res.sendStatus(200);
    }).catch((e)=>{
        console.log(e);
        return res.sendStatus(500);
    });
}