'use strict';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const _ = require('lodash');
const baseConfig = require('./config/environment');
let config;
try {
  config = _.extend({}, baseConfig, require('./config/environment'));
} catch(e) {
  if (!/cannot find module/i.test(e.message)) {
    console.error('Unable to load ./config/environment');
    throw e;
  } else {
    console.warn('No ./config/environment found. Using base settings.');
  }
  config = baseConfig;
}
config.port = Number(process.argv[2]) || process.env.PORT || config.port;

if (!config.port) {
  console.error("Usage: node index.js <PORT>");
  process.exit(1);
}

require('./lib/server')(config);
