'use strict';

const express = require('express');
var ConnectController = require('../utils/connect');
let seed = require('../config/seed/seed');
let debug = require("debug");

function connectToDatabase(config){
    return ConnectController.connect().then(() => {
        return seed(config);
    });
}

module.exports = function createServer(config) {
    if(config.env !== "test"){
        return connectToDatabase(config).then(() => {
            return initServer(config); //returns app
        }).catch((e) => {
            console.error(e);
        });
    }else{
        return initServer(config);
    }
}

/*module.exports = function createServer(config) {

};*/

function initServer(config) {

    if(config.seedDB) { require('../config/seed/seed'); }

    const app = express();

    app.set('view engine', 'ejs');

    if (process.env.NODE_ENV === 'development') {
        app.use(function(req, res, next) {
          debug('Got request at %s', req.url);
          next();
        });
    }

    // Readability
    if (process.env.NODE_ENV !== 'production') {
        app.set('json spaces', 2);
    }

    require('../config/express')(app);
    require('../config/routes')(app);

    if(config.env !== "test") {
        app.listen(config.port, function () {
            console.log('Server listening on ' + config.protocal + config.host + ':' + config.port);
        });

        return app;
    }else{
        return app.listen(config.port, function () {
            console.log('Server listening on ' + config.protocal + config.host + ':' + config.port);
        });
    }

}