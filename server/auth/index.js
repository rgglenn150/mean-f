var express = require('express');
var AuthController = require('./auth.controller');
var AuthMiddleware = require('./auth.middleware');

var router = express.Router();


router.post('/facebook', AuthController.loginFacebook);

router.get('/linkedin/generateConsentUrl', AuthController.generateConsentUrl);

router.post('/linkedin', AuthController.loginLinkedIn);

router.get('/users/me', AuthMiddleware.checkAuthenticated, AuthController.getMyself)

router.post('/users/me', AuthMiddleware.checkAuthenticated, AuthController.updateMyself)

router.post('/login', AuthController.login)

router.post('/register', AuthController.register)

router.post('/orgUserRegister', AuthController.orgUserRegister)

router.get('/confirm/:confirmationToken', AuthController.confirmAccount);

module.exports = router;