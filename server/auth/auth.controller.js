var config = require('../config/environment');
var jwt = require('jsonwebtoken');
let User = require('../api/user/user.model');
const request = require('request-promise');
let utils = require('../api/utils/utils');
let Linkedin = require('node-linkedin')(config.linkedinClientId, config.linkedinSecret);
let Bluebird = require('bluebird');
let UserFriend = require('../api/user/user-friend.model');
let UserFriendCtrl = require('../api/user/user-friend.controller');
let EmailCtrl = require("../api/email/email.controller");

var validationError = function (res, err) {
    if (err && err.code === 11000) {
        return res.status(422).json({
            message: "user exists"
        });
    } else {
        return res.status(422).json(err);
    }
};

function sendAuthError(res) {
    return res.sendStatus(401);
}

function getMyself(req, res) {
    return res.json(config.users[req.user - 1]);
};

function updateMyself(req, res) {
    var user = config.users[req.user - 1];

    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    res.json(user);
}

let authenticateUser = function (email, password) {
    return User.findOne({
        email: email
    }).then((user) => {
        return user.authenticate(password);
    });
};

let getToken = function (user) {
    return jwt.sign({
        _id: user._id
    }, config.sessionSecret, {
        expiresIn: config.expiresInMinutes
    });
};

function login(req, res) {

   
    let user;
    return User.findOne({
        email: req.body.email
    }).then((data) => {
        user = data;
       
        return user.authenticate(req.body.password);


    }).then((isUserAuthenticated) => {

        if (isUserAuthenticated && user.status !== 'inactive') {
            return sendToken(user, res);
        } else {
            return sendAuthError(res);
        }
    }).catch((e) => {
        console.error(e);
        return sendAuthError(res);
    });
}

function generateRandomString(size) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < size; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

function registerHandler(firstName, lastName, email, password, profilePhoto, facebookId = null, linkedinId = null, token = null, status = null, confirmationToken = null,
    orgId = null, orgName = null) {
    let user;
    let name = (firstName + lastName).replace(/\s+/g, '');

    const defaultUsername = name + generateRandomString(20 - name.length);

    return User.create({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password,
        profilePhoto: profilePhoto,
        facebookId: facebookId,
        linkedinId: linkedinId,
        username: defaultUsername,
        status: status,
        isUrlPrivate: false,
        isNamePrivate: false,
        orgName: orgName,
        orgId: orgId,
        confirmationToken: confirmationToken
    }).then((data) => {
        user = data;
        if (token) {
            return UserFriendCtrl.completeInvite(user, token);
        } else {
            return UserFriend.updateOne({
                friendEmail: user.email
            }, {
                $set: {
                    friend: user._id
                }
            });
        }
    }).then(() => {
        return user;
    });
}

function register(req, res) {
    const confirmationToken = utils.createUuid();

    return registerHandler(req.body.firstName, req.body.lastName, req.body.email, req.body.password, req.body.profilePhoto, req.body.facebookId,
        req.body.linkedinId, req.body.token, 'pending', confirmationToken).then((user) => {
        sendToken(user, res);

        if (config.env !== 'test') {
            EmailCtrl.confirmEmailRegistration(req.body.email, confirmationToken);
        }

        return res.json({
            _id: user._id,
            firstName: user.firstName,
            lastName: user.lastName,
            status: user.status
        });
    }, (err) => {
        return validationError(res, err);
    }).catch((e) => {
        console.error(e);
        res.sendStatus(500);
    })

}

function orgUserRegister(req, res) {
    const confirmationToken = utils.createUuid();

    return registerHandler(req.body.firstName, req.body.lastName, req.body.email, req.body.password, req.body.profilePhoto, req.body.facebookId,
        req.body.linkedinId, req.body.token, 'pending', confirmationToken, req.body.orgId, req.body.orgName).then((user) => {
        sendToken(user, res);

        if (config.env !== 'test') {
            EmailCtrl.confirmEmailRegistration(req.body.email, confirmationToken);
        }

        return res.json({
            _id: user._id,
            firstName: user.firstName,
            lastName: user.lastName,
            status: user.status
        });
    }, (err) => {
        return validationError(res, err);
    }).catch((e) => {
        console.error(e);
        res.sendStatus(500);
    })

}



function sendToken(user, res) {
    var token = jwt.sign({
        _id: user._id
    }, config.sessionSecret, {
        expiresIn: config.expiresInMinutes
    });
    return res.json({
        _id: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        profilePhoto: user.profilePhoto,
        token: token,
        status: user.status
    });
}

function upsertRegister(firstName, lastName, email, profilePhoto = null, facebookId = null, linkedinId = null, token = null) {

    return User.findOne({
        email: email
    }).then((user) => {
        if (!user) {
            return registerHandler(firstName, lastName, email, utils.createUuid(), profilePhoto, facebookId, linkedinId, token);
        } else if (user && profilePhoto) {
            return User.updateOne({
                _id: user._id
            }, {
                $set: {
                    profilePhoto: profilePhoto
                }
            }).then(() => {
                return user;
            })
        } else {
            return user;
        }
    });
}

function loginFacebook(req, res) {

    var options = {
        method: 'GET',
        url: 'https://graph.facebook.com/' + req.body.id,
        qs: {
            access_token: req.body.authToken
        },
        headers: {
            'cache-control': 'no-cache'
        }
    };

    return request(options).then((response) => {
        try {
            response = JSON.parse(response);
        } catch (e) {
            return res.sendStatus(500);
        }

        if (response && response.id === req.body.id) {
            return upsertRegister(req.body.firstName, req.body.lastName, req.body.email, req.body.profilePhoto, req.body.id, null, req.body.token).then((user) => {
                return sendToken(user, res);
            });
        } else {
            return res.sendStatus(401);
        }
    }).catch((e) => {
        console.error(e);
        return res.sendStatus(500);
    });

}

function getAccessToken(res, code, state) {
    return new Bluebird((resolve, reject) => {
        Linkedin.auth.getAccessToken(res, code, state, function (err, results) {
            if (err || !results.access_token) {
                console.error(err);
                return reject(err);
                //return res.sendStatus(401);
            } else {
                return resolve(results.access_token);
            }
        });
    })
}

function getProfile(linkedin) {
    return new Bluebird((resolve, reject) => {
        linkedin.people.me(function (err, profile) {
            if (err) {
                console.error(err)
                return reject(err);
            } else {
                return resolve(profile);
            }
            // Loads the profile of access token owner.
        });
    });
}

function loginLinkedIn(req, res) {
    return getAccessToken(res, req.body.code, req.body.state).then((accessToken) => {
        var linkedin = Linkedin.init(accessToken, {
            timeout: 10000 /* 10 seconds */
        });
        return getProfile(linkedin);
    }).then((profile) => {
        return upsertRegister(profile.firstName, profile.lastName, profile.emailAddress, profile.pictureUrl, null, profile.id, req.body.token).then((user) => {
            return sendToken(user, res);
        });
    }).catch((e) => {
        console.log(e);
        return res.sendStatus(401);
    });
}

function generateConsentUrl(req, res) {
    let scope = ['r_basicprofile', 'r_emailaddress'];

    Linkedin.setCallback(config.linkedinRedirectUrl);
    let auth_url = Linkedin.auth.authorize(scope);
    res.json({
        url: auth_url
    });
}

 confirmAccount  = async (req, res) => {
    const confirmationToken = req.params.confirmationToken;

    let account = await User.findOne({
        confirmationToken: confirmationToken
    });
    if (!confirmationToken || !account) {
        return res.status(400).json({
            message: 'Invalid confirmation token!'
        });
    }
    try {
        let updatedAccount = await User.findOneAndUpdate({
            _id: account._id
        }, {
            status: 'active'
        });
        updatedAccount.status = 'active';
        return res.json(
            updatedAccount
        );
    } catch (e) {
        handleError(res, e);
    }
}

module.exports = {
    getMyself,
    updateMyself,
    login,
    register,
    orgUserRegister,
    confirmAccount,
    authenticateUser: authenticateUser,
    getToken: getToken,
    loginFacebook: loginFacebook,
    loginLinkedIn: loginLinkedIn,
    generateConsentUrl: generateConsentUrl
}