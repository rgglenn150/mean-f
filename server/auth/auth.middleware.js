var config = require('../config/environment');
var jwt = require('jsonwebtoken');
let User = require('../api/user/user.model');

function sendUnathorized(res, message){
    return res.status(401).send({message: message});
}

function checkAuthenticated(req, res, next){
    if(!req.header('authorization'))
        return sendUnathorized(res, 'Unathorized request. Missing authentication header');

    let token = req.header('authorization').split(' ')[1];
    var payload = jwt.decode(token, config.sessionSecret);

    if(!payload)
        return sendUnathorized(res, 'Unathorized request. Authentication header invalid');

    if(payload){
        return User.findOne({ _id: payload._id }).then((user)=> {
            if (!user) {
                return res.sendStatus(401);
            } else if(user.linkedEmail) {
                User.findOne({ _id: user.linkedEmail }).then(()=>{
                    req.user = user;
                    next();
                }).catch(()=>{
                    return res.sendStatus(500);
                });
            } else {
                req.user = user;
                next();
            }
        }).catch(()=>{
            return res.sendStatus(401);
        })
    }else{
        return res.sendStatus(401);
    }

}

/**
 * Checks if the user role meets the minimum requirements of the route
 */
function hasRole(roleRequired) {
    if (!roleRequired) throw new Error('Required role needs to be set');
    return function(req, res, next) {
        if (config.userRoles.indexOf(req.user.role) >= config.userRoles.indexOf(roleRequired)) {
            next();
        } else {
            return res.send(403);
        }
    }
}

/**
 * Checks if the user role meets the minimum requirements of the route, this function can be used anywhere
 */
function hasRoleSerial(roleRequired, user) {
    if (!roleRequired || !user) return false;
    return config.userRoles.indexOf(user.role) >= config.userRoles.indexOf(roleRequired);
}


module.exports = {
    checkAuthenticated,
    hasRole,
    hasRoleSerial
};
