var config = require('../config/environment');
var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();
var AuthController = require('./auth.controller');
let TestingService = require('../test/test.service')
let request = require('supertest');
let User = require('../api/user/user.model');
//let app = require('../lib/server')(config);

module.exports = function(app) {

    describe('Auth Tests', () => {
        let user, token, testingUserData;

        before(async () => {
            user = await TestingService.getUser();
            expect(user).to.not.be.null;
            token = TestingService.getToken(user);
            expect(token).to.not.be.null;
            testingUserData = TestingService.getTestingUserData();
        });

        after(async () => {

        });

        describe('Auth User Login ', () => {
            it('should return 200 and the full name when logging in ', async () => {
                const response = await request(app).post('/auth/login')
                //.set('Authorization', 'Bearer ' + token)
                .send({
                    email: testingUserData.email,
                    password: testingUserData.password
                });
                expect(response).to.have.property('status').equal(200);
            });
        });

        describe('invalid requests', () => {

            it('should return 401 if username and password dont match', async () => {
                //const nonExistingId = '5b10e1c601e9b8702ccfb974';
                //expect(await User.findOne({_id: nonExistingId})).to.be.null;

                const response = await request(app).post('/auth/login')
                //.set('Authorization', 'Bearer ' + token)
                    .send({
                        email: testingUserData.email,
                        password: testingUserData.password + 'da da da da'
                    });

                expect(response).to.have.property('status').equal(401);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
            });
        });

        describe('POST /auth/register', () => {

            it('should create a user and return 200', async () => {
                let user = {
                    email: "testinguser123@bringonthegood.com",
                    password: "password123",
                    firstName: "Bobby",
                    lastName: "Foo"
                };
                expect(await User.findOne({email: user.email})).to.be.null;

                const response = await request(app).post('/auth/register')
                //.set('Authorization', 'Bearer ' + token)
                    .send(user);

                expect(response).to.have.property('status').equal(200);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
              //  expect(response.body).to.have.property('token');
            });

            it('creating a duplicate user should return 422 with error message', async () => {
                expect(user).to.not.be.null;
                const response = await request(app).post('/auth/register')
                //.set('Authorization', 'Bearer ' + token)
                    .send(testingUserData);

                expect(response).to.have.property('status').equal(422);
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('message');
                expect(response.body.message).to.equal('user exists');
            });
        });



        /*

        describe('valid request', () => {
            it('should return 200 and the user resource, including the email field, when retrieving the authenticated user', async () => {
                const response = await withLogin(request(api).get(`/users/${user._id}`), authUser);

                expect(response).to.have.status(200);
                expect(response.body._id).to.equal(user._id.toString());
            });

            it('should return 200 and the user resource, excluding the email field, when retrieving another user', async () => {
                const anotherUser = await User.findOne({email: 'another_user@email.com'});

                const response = await withLogin(request(api).get(`/users/${anotherUser.id}`), authUser);

                expect(response).to.have.status(200);
                expect(response.body._id).to.equal(anotherUser._id.toString());
                expect(response.body).to.not.have.an('email');
            });

        });

        describe('invalid requests', () => {

            it('should return 404 if requested user does not exist', async () => {
                const nonExistingId = '5b10e1c601e9b8702ccfb974';
                expect(await User.findOne({_id: nonExistingId})).to.be.null;

                const response = await withLogin(request(api).get(`/users/${nonExistingId}`), authUser);
                expect(response).to.have.status(404);
            });
        });*/

    });
};