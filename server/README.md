# Badges Back-End API
# Requirements
* Node.jS
    * Download and install Node.JS (homebrew is preferred) https://nodejs.org/en/download/
* Yarn
    * Install Yarn from https://yarnpkg.com/en/docs/install or
    ```npm install -g yarn```

# Setup
* Checkout the project
```
git clone git@bitbucket.org:bringonthegood/badges-back-end.git
```
* Install Dependencies
```
yarn
```

# Use
* To run in development
```
    nodemon server
```

### Database
I run a local mongodb database because its way easier to manage. However there
is a development database, its just commented out right now. Just go to config/development.js
to change it. Running npm test will also run on local, I was doing this for
debugging purposes as well. Development and Test are using different local
databases. They won't conflict with each other.

### To run server in development:
```
yarn
```

(if you haven't installed nodemon globally: npm install nodemon -g)
```
nodemon server
```

### Deploying to production

I just deploy to heroku using 
```
git push heroku master
```

### To run tests:
```
yarn test
```