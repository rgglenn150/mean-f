// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  facebookAppId: '914525758657957',
  linkedinClientId: '78hopnrllco3at',
  host: 'localhost:6969',
  protocal: 'http://',
  stripePublishable: 'pk_test_88sH1c3OsW9rXZjkEbtp3ZcZ',
  firebase: {
    apiKey: "AIzaSyDXx1oQBkyOUFMmbYxlgTJlLTRAob0Qp6Q",
    authDomain: "badges-91374.firebaseapp.com",
    databaseURL: "https://badges-91374.firebaseio.com",
    projectId: "badges-91374",
    storageBucket: "badges-91374.appspot.com",
    messagingSenderId: "474701600571"
  },
  fcmServerKey:"AAAAboZrlzs:APA91bGWfe4-3wKOvQHIXFtVATlmUu0EkkIf8c7sjCyPC7UV0B7E-a1JadRySdfxe7kBZMG5OekNfAd-e2RPMeUbLj9F7IiXmoTRF_QSWI7a7jeb4ysgVw7glysotYDd6Uf_XElfSDH_"
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
