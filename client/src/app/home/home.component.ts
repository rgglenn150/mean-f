import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { BadgeService } from '../badge/badge.service';
import { Badge } from '../badge/badge';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { BadgeUser } from '../badge/badge-user';

@Component({
  selector: 'home',
  templateUrl: './home.component.html'
})
export class HomeComponent {

  private user: object;
  badges$: Observable<Badge[]>;
  myBadges$: Observable<BadgeUser[]>
  myPendingBadges$: Observable<BadgeUser[]>
  destroySubject$: Subject<void> = new Subject();
  public searchText;

  constructor(public auth: AuthService, private badgeService: BadgeService) {
    this.user = auth.authenticatedUser;
    this.checkLoggedIn();
  }

  ngOnInit() {
    this.auth.user.pipe(takeUntil(this.destroySubject$)).subscribe(data => {
      this.user = data;
      this.checkLoggedIn();
    });



    this.myPendingBadges$ = this.badgeService.getMyPendingBadges(['badge']).pipe(
      map(c => { // Badges components needs badge-user -> badge in the top level directory, so we map
        console.log("RGDB E",c)
        return c.map(f => f);
      })
    );


    this.myBadges$ = this.badgeService.getMyBadges(['badge']).pipe(
      map(b => { // Badges components needs badge-user -> badge in the top level directory, so we map
        return b.map(e => e.badge);
      })
    );

    this.badges$ = this.badgeService.getBadges(['user']);
  }




  

  /* searchHandler(value) {
    if (value) {

      this.badges$ = this.badgeService.searchCreatedBadges('name', value, ['badge']);
      this.myBadges$ = this.badgeService.searchMyBadges('name', value, ['badge']).pipe(
        map(b => { // Badges components needs badge-user -> badge in the top level directory, so we map
          return b.map(e => e.badge);
        })
      );
    } else {
      this.myBadges$ = this.badgeService.getMyBadges(['badge']).pipe(
        map(b => { // Badges components needs badge-user -> badge in the top level directory, so we map
          return b.map(e => e.badge);
        })
      );
      this.badges$ = this.badgeService.getBadges(['user']);
    }
  } */

  private checkLoggedIn() {
    if (!this.auth.isAuthenticated) {
      this.auth.logout();
    }
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }
}
