export interface BadgeDefault {
  _id: string;
  name: String;
  color: String;
  path: String;
  width: Number;
  height: Number;
  svg: String;
}


