import {Component, Inject, inject, Injectable, Input, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {MessageService} from '../../../utils/message.service';
import html2canvas from 'html2canvas';
import {Observable, from} from 'rxjs';

export interface DialogData {
  canvas;
}

@Injectable()
export class BadgePreviewService {

  form;
  options = {
    logging: false,
    useCORS: true
  };
  constructor(public dialog: MatDialog) {}


  getCanvas(containerId) {
    const _this = this;
    return html2canvas(document.getElementById(containerId), this.options).then(function(canvas) {
      return canvas;
    });
  }
  getCanvasUrl(containerId) {
    const _this = this;
    return html2canvas(document.getElementById(containerId), this.options).then(function(canvas) {
      return canvas.toDataURL();
    });
  }

  getImageUrl(containerId) {
    return this.getCanvas(containerId).then((data) => {
      return data.toDataURL();
    });
  }

  openDialog(canvas): Observable<any> {
      const dialogRef = this.dialog.open(BadgePreviewDialogComponent, {
        data: { canvas: canvas }
      });
      return dialogRef.afterClosed();
  }
}

@Component({
  selector: 'badge-preview-dialog',
  templateUrl: 'badge-preview-dialog.component.html',
})
export class BadgePreviewDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<BadgePreviewDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {

  }

  ngOnInit(){
    document.getElementById('badge-preview').appendChild(this.data.canvas);
  }
}
