import { Component, OnDestroy, OnInit } from '@angular/core';
import { BadgeService } from '../../badge.service';
import { Badge } from '../../badge';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from '../../../utils/message.service';
import { BadgePreviewService } from '../badge-preview/badge-preview.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ProfanityFilterService } from 'src/app/utils/profanity-filter.service';


@Component({
  selector: 'badge-create',
  templateUrl: './badge-create.component.html',
  styleUrls: ['./badge-create.component.css']
})
export class BadgeCreateComponent implements OnInit, OnDestroy {

  editBadge: Badge;
  form;
  badgeDefault;
  destroySubject$: Subject<void> = new Subject();
  settings;
  createDisabled = false;
  constructor(
    private messageService: MessageService,
    private badgePreviewService: BadgePreviewService,
    private badgeService: BadgeService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private profanityFilter: ProfanityFilterService) {
    this.form = this.badgeService.getFormGroup();
  }

  ngOnInit() {
    this.route.url.pipe(takeUntil(this.destroySubject$)).subscribe(url => {
      if (url && url[1] && url[1].path) {
        const badgesDefaultId = url[1].path;
        this.badgeService.getBadgeDefault(badgesDefaultId).pipe(takeUntil(this.destroySubject$)).subscribe(badges => {
          this.badgeDefault = badges.filter(badge => badge._id === badgesDefaultId);
          this.badgeDefault = this.badgeDefault.length > 0 ? this.badgeDefault[0] : this.badgeDefault;
          this.settings = {
            name: this.badgeDefault.name,
            svg: this.badgeDefault.svg,
            text: this.badgeDefault.text,
            image: this.badgeDefault.image,
            bgColor: this.badgeDefault.bgColor
          };
        });
      }
    });
  }


  isClean() {
    return control => {
      return !this.profanityFilter.check(control.value) ? null : { hasProfanity: true }
    }

  }

  onSubmit() {
    this.editBadge = undefined;
    if (!this.createDisabled) {
      if (this.form.valid) {
        this.createDisabled = true;
        this.badgePreviewService.getCanvas('badge-canvas').then((canvas) => {
          this.badgePreviewService.openDialog(canvas).pipe(takeUntil(this.destroySubject$)).subscribe((result) => {
            if (result && result.canvas) {
              const newBadge: Badge = this.form.value as Badge;
              newBadge.image = result.canvas.toDataURL();
              newBadge.settings = this.settings;
              this.badgeService.create(newBadge).pipe(takeUntil(this.destroySubject$)).subscribe(data => {
                this.form.reset();
                this.messageService.add('Badge Created Successfully');
                this.createDisabled = false;
              });
            } else {
              this.createDisabled = false;
            }
          });
        });
      } else {
        this.messageService.add('Please fill out the required fields in the form correctly');
      }
    }

  }
  isValid(control) {
    return this.form.controls[control].invalid && this.form.controls[control].touched;
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }
}
