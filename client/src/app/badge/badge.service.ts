import { Injectable } from '@angular/core';
import { HandleError, HttpErrorHandler } from '../http-error-handler.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Badge } from './badge';
import { catchError, filter, map, takeUntil } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { BadgeUser } from './badge-user';
import { BadgeDefault } from './badge-default';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProfanityFilterService } from '../utils/profanity-filter.service';

@Injectable()
export class BadgeService {
  BASE_URL = environment.protocal + environment.host + '/api/badge';
  private handleError: HandleError;

  constructor(
    private http: HttpClient, private httpErrorHandler: HttpErrorHandler, private formBuilder: FormBuilder, private profanityFilter: ProfanityFilterService) {
    this.handleError = httpErrorHandler.createHandleError('BadgeService');
  }

  getFormGroup(): FormGroup {
    return this.formBuilder.group({
      name: ['', [Validators.required, this.isClean()]],
      description: ['', [Validators.required, this.isClean()]],
      image: [''],
      prize: [''],
      prizeDescription: ['', [this.isClean()]],
      makePrivate: [''],
      turnCommentsOff: [''],
      turnEmailNotificationsOff: [''],
      settings: this.formBuilder.group({
        name: ['Default Badge'],
        bgColor: ['#A0C2BB'],
        text: this.formBuilder.group({
          name: ['Badge Name'],
          color: ['#FFF'],
          font: ['Oswald'],
          width: ['150'],
          height: ['100'],
          fontSize: ['22'],
          position: this.formBuilder.group({
            x: ['125'],
            y: ['100']
          })
        }),
        image: this.formBuilder.group({
          path: ['/assets/img/badges/Badge1.png'],
          width: ['150'],
          height: ['150'],
          position: this.formBuilder.group({
            x: ['100'],
            y: ['50']
          })
        })
      })
    });
  }

  isClean() {
    return control => {
      return !this.profanityFilter.check(control.value) ? null : { hasProfanity: true }
    }

  }

  getBadgesDefault(): Observable<any> {
    return this.http.get<BadgeDefault[]>(this.BASE_URL + '/default')
      .pipe(
        catchError(this.handleError('getBadgesDefault', []))
      );
  }

  getBadgeDefault(id): Observable<any> {
    return this.http.get<BadgeDefault[]>(this.BASE_URL + '/default/' + id)
      .pipe(
        catchError(this.handleError('getBadgeDefault', []))
      );
  }

  getMyBadges(populate = []): Observable<any> {
    const queryParams = populate.length > 0 ? '?populate=' + populate : '';
    const url = this.BASE_URL + '/me' + queryParams;
    return this.http.get<any[]>(url)
      .pipe(
        catchError(this.handleError('getMyBadges', []))
      );
    // Add safe, URL encoded search parameter if there is a search term
    // const options = params: new HttpParams().set('symbol', symbol)} : {};
  }

  getMyPendingBadges(populate = []): Observable<any> {
    const queryParams = populate.length > 0 ? '?populate=' + populate : '';
    const url = this.BASE_URL + '/me/pending' + queryParams;
    return this.http.get<any[]>(url)
      .pipe(
        catchError(this.handleError('getMyPendingBadges', []))
      );

  }

  acceptPendingBadge(_id, isPrivate) {

    console.log('accept RGDB ', _id);
    return this.http.get(this.BASE_URL + '/me/accept/'+_id).pipe(catchError(this.handleError('acceptPendingBadge', []))).subscribe(data=>{
      console.log('accept RGDB ', data);
      window.location.reload();
    });
  }

  rejectPendingBadge(_id){

  }

  getBadges(populate = []): Observable<Badge[]> {
    const queryParams = populate.length > 0 ? '?populate=' + populate : '';
    const url = this.BASE_URL + '/' + queryParams;
    return this.http.get<Badge[]>(url)
      .pipe(
        catchError(this.handleError('getBadges', []))
      );
    // Add safe, URL encoded search parameter if there is a search term
    // const options = params: new HttpParams().set('symbol', symbol)} : {};
  }

  getReceivedBadgesByUsername(username: string, populate = []): Observable<any> {
    const queryParams = populate.length > 0 && username ? '?populate=' + populate + '&username=' + username : '';
    const url = this.BASE_URL + '/user' + queryParams;

    return this.http.get<any[]>(url)
      .pipe(
        catchError(this.handleError('getMyBadges', []))
      );
    // Add safe, URL encoded search parameter if there is a search term
    // const options = params: new HttpParams().set('symbol', symbol)} : {};
  }

  getCreatedBadgesByUsername(username: string, populate = []): Observable<Badge[]> {
    const queryParams = populate.length > 0 && username ? '?populate=' + populate + '&username=' + username : '';
    const url = this.BASE_URL + '/user-created' + queryParams;
    return this.http.get<Badge[]>(url)
      .pipe(
        catchError(this.handleError('getBadges', []))
      );

  }


  searchCreatedBadges(field: string, search: string, populate = []) {
    const queryParams = populate.length > 0 && field && search ? '?populate=' + populate + '&field=' + field + '&search=' + search : '';
    const url = this.BASE_URL + '/search/createdBadges' + queryParams;
    return this.http.get<Badge[]>(url)
      .pipe(
        catchError(this.handleError('getBadges', []))
      );
  }
  searchMyBadges(field: string, search: string, populate = []) {
    const queryParams = populate.length > 0 && field && search ? '?populate=' + populate + '&field=' + field + '&search=' + search : '';
    const url = this.BASE_URL + '/search/myBadges' + queryParams;
    return this.http.get<any[]>(url)
      .pipe(
        catchError(this.handleError('getMyBadges', []))
      );
  }



  create(badge: Badge): Observable<Badge> {
    return this.http.post<Badge>(this.BASE_URL + '/', badge)
      .pipe(
        catchError(this.handleError('CreateBadge', badge))
      );
  }
  createNoError(badge: Badge): Observable<Badge> {
    return this.http.post<Badge>(this.BASE_URL + '/', badge);
  }
  awardBadge(badge: BadgeUser): Observable<any> {
    return this.http.post<Badge>(this.BASE_URL + '/award', badge)
      .pipe(
        catchError(this.handleError('CreateBadge', badge))
      );
  }
  getFirst() {
    return {
      name: '',
      description: '',
      prize: '',
      prizeDescription: '',
      makePrivate: false,
      turnEmailNotificationsOff: false,
      turnCommentsOff: false,
      settings: {
        name: 'Default Badge',
        bgColor: '#f3c766',
        text: {
          color: '#485868',
          font: 'Kobar',
          width: 338,
          height: 63,
          fontSize: 60,
          position: { x: 0, y: 156 },
          name: 'BADGE NAME'
        },
        image: {
          path: '/assets/img/badges/1-st-a.png',
          width: 219,
          height: 239,
          position: { x: 65, y: 7 }
        }
      }
    };
  }
  getSecond() {
    return {
      name: '',
      description: '',
      prize: '',
      prizeDescription: '',
      makePrivate: false,
      turnEmailNotificationsOff: false,
      turnCommentsOff: false,
      settings: {
        name: 'Default Badge',
        bgColor: '#9aaaba',
        text: {
          color: '#485868',
          font: 'Troche',
          width: 332,
          height: 73,
          fontSize: 50,
          position: { x: 3, y: 151 },
          name: 'Badge Name'
        },
        image: {
          path: '/assets/img/badges/2-nd-a.png',
          width: 230,
          height: 239,
          position: { x: 61, y: 9 }
        }
      }
    };
  }
  getThird() {
    return {
      name: '',
      description: '',
      prize: '',
      prizeDescription: '',
      makePrivate: false,
      turnEmailNotificationsOff: false,
      turnCommentsOff: false,
      settings: {
        name: 'Default Badge',
        bgColor: '#6e6f71',
        text: {
          color: '#9fc2bb',
          font: 'Westmeath',
          width: 332,
          height: 73,
          fontSize: 50,
          position: { x: 5, y: 180 },
          name: 'Badge Name'
        },
        image: {
          path: '/assets/img/badges/3-rd-a.png',
          width: 215,
          height: 216,
          position: { x: 68, y: 6 }
        }
      }
    };
  }
  getParticipant() {
    return {
      name: '',
      description: '',
      prize: '',
      prizeDescription: '',
      makePrivate: false,
      turnEmailNotificationsOff: false,
      turnCommentsOff: false,
      settings: {
        name: 'Default Badge',
        bgColor: '#9fc2bb',
        text: {
          color: '#485868',
          font: 'Troche',
          width: 174,
          height: 151,
          fontSize: 45,
          position: { x: 80, y: 58 },
          name: 'Badge Name'
        },
        image: {
          path: '/assets/img/badges/badge-14.png',
          width: 257,
          height: 232,
          position: { x: 38, y: 10 }
        }
      }
    };
  }
}
