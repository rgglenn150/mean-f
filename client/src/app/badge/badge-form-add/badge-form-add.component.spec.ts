import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BadgeFormAddComponent } from './badge-form-add.component';

describe('BadgeFormAddComponent', () => {
  let component: BadgeFormAddComponent;
  let fixture: ComponentFixture<BadgeFormAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BadgeFormAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BadgeFormAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
