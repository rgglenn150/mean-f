import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BadgeService} from '../badge.service';
import {MessageService} from '../../utils/message.service';
import {takeUntil} from 'rxjs/operators';
import {DialogService} from '../../utils/dialog-service/dialog.service';
import {Subject} from 'rxjs';

@Component({
  selector: 'badge-form-add',
  templateUrl: './badge-form-add.component.html',
  styleUrls: ['./badge-form-add.component.css']
})
export class BadgeFormAddComponent implements OnInit, OnDestroy {

  @Input()
  public parentForm: FormGroup;

  destroySubject$: Subject<void> = new Subject();

  constructor(
    private _fb: FormBuilder,
    private badgeService: BadgeService,
    private messageService: MessageService,
    private dialogService: DialogService) { }

  ngOnInit() {}

  duplicateBadge(badge) {
    const control = <FormArray>this.parentForm.controls['badges'];
    control.push(this.badgeService.getFormGroup());
    this.parentForm.controls['badges']['controls'][control.length - 1].patchValue(badge.value);
    this.messageService.add('Successfully duplicated badge');
  }

  removeBadge(i: number) {
    this.dialogService.openDialog(
      'Are you sure you want to remove this badge? This action cannot be undone.',
      'Confirm Remove'
    )
    .pipe(takeUntil(this.destroySubject$))
    .subscribe((result) => {
      if (result) {
        const control = <FormArray>this.parentForm.controls['badges'];
        control.removeAt(i);
        this.messageService.add('Successfully removed badge');
      }
    });
  }

  get formData() { return <FormArray>this.parentForm.get('badges'); }

  ngOnDestroy() {
    this.destroySubject$.next();
  }

}
