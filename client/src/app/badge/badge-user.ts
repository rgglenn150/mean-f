export interface BadgeUser {
  _id: string;
  user: string;
  createdByUser: string;
  badge: string;
  company: string;
  compaign: string;
  awarded_date: Date;
}
