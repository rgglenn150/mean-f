import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Badge} from '../badge';
import { BadgeUploadService } from './badge-upload.service';
import {FormBuilder, Validators} from '@angular/forms';
import {BadgeService} from '../badge.service';
import {MessageService} from '../../utils/message.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

export interface DialogData {
  animal: string;
  name: string;
}

/* <badge-upload (valueChange)='handleBadgeUpload($event)'></badge-choose> */

/**
 * @title Dialog Overview
 */
@Component({
  selector: 'badge-upload',
  templateUrl: 'badge-upload.component.html',
  styleUrls: ['badge-upload.component.css'],
})
export class BadgeUploadComponent {

  badge: Badge;
  name: string;
  destroySubject$: Subject<void> = new Subject();

  @Output() valueChange = new EventEmitter();

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(BadgeUploadDialogComponent, {
      width: 'auto',
      minHeight: 'auto',
      disableClose: true,
      data: {name: this.name, badge: this.badge}
    });

    dialogRef.afterClosed().pipe(takeUntil(this.destroySubject$)).subscribe(result => {
      this.badge = result;
      this.valueChange.emit(result);
    });
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }

}

@Component({
  selector: 'badge-upload-dialog',
  templateUrl: 'badge-upload-dialog.html',
  styleUrls: ['badge-upload.component.css']
})
export class BadgeUploadDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<BadgeUploadDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Badge,
    private uploaderService: BadgeUploadService,
    private formBuilder: FormBuilder,
    private badgeService: BadgeService,
    private messageService: MessageService) {

    this.form = badgeService.getFormGroup();
  }

  badge: Badge;
  message: string;
  image: string = null;
  form;
  loading: Boolean = false;

  destroySubject$: Subject<void> = new Subject();

  onPicked(input: HTMLInputElement) {
    const file = input.files[0];
    if (file) {
      this.loading = true;
      this.uploaderService.upload(file).pipe(takeUntil(this.destroySubject$)).subscribe(
        msg => {
          input.value = null;
          // @ts-ignore
          this.image = msg.image ? msg.image : '';
          // @ts-ignore
          this.message = msg.message;
          this.messageService.add(this.message);
          this.loading = false;
        },
        e => {
          this.loading = false;
        }

      );
    }
  }

  onSubmit() {
    if (this.form.valid) {
      const newBadge: Badge = this.form.value as Badge;
      newBadge.image = this.image;
      this.badgeService.create(newBadge).pipe(takeUntil(this.destroySubject$)).subscribe(data => {
        this.form.reset();
        this.messageService.add('Badge Created Successfully');
        this.dialogRef.close();
      });
    } else {
      this.messageService.add('Please fill out the required fields in the form correctly');
    }
  }
  isValid(control){
    return this.form.controls[control].invalid && this.form.controls[control].touched;
  }


  handleBadgeSelection(badge) {
    this.badge = badge;
    this.dialogRef.close(badge);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }

}
