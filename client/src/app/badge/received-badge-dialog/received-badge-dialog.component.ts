import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BadgeService } from '../badge.service';

@Component({
  selector: 'app-received-badge-dialog',
  templateUrl: './received-badge-dialog.component.html',
  styleUrls: ['./received-badge-dialog.component.css']
})
export class ReceivedBadgeDialogComponent implements OnInit {
  makePrivate: boolean = false;
  badge;
  badgeUser;
  badgeUserId;
  constructor(public dialogRef: MatDialogRef<ReceivedBadgeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private badgeService: BadgeService) {
    this.badge = data.badge;
    console.log('RGDB DATA',data);
    this.badgeUserId= data.id;
    this.badgeUser = data;
  }

  ngOnInit() {
  }

  toggleCheckbox(event) {
    console.log('RGDB EVENT CHECKBOX', event)
    this.makePrivate = event.checked;
  }

  accept(_id: string) {

    this.badgeService.acceptPendingBadge(_id, this.makePrivate);
    this.dialogRef.close();
  }

  reject(_id: string) {
    console.log('RGDB reject ', _id);
    this.dialogRef.close();
  }


}
