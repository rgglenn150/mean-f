import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceivedBadgeDialogComponent } from './received-badge-dialog.component';

describe('ReceivedBadgeDialogComponent', () => {
  let component: ReceivedBadgeDialogComponent;
  let fixture: ComponentFixture<ReceivedBadgeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceivedBadgeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceivedBadgeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
