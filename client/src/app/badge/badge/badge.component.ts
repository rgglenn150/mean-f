import {Component, EventEmitter, Input, Output} from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import {BadgeService} from '../badge.service';
import { Badge } from '../badge';
import {Observable} from 'rxjs';

/*

In order to use this component you must catch and handle the event by the following

<badge [badgeType]="'user'" (valueChange)='handleThisNewFunction($event)'></badge>

 */

@Component({
  selector: 'badge',
  templateUrl: 'badge.component.html',
  styleUrls: ['./badge.component.css']
})
export class BadgeComponent {
  constructor(private auth: AuthService, private badgeService: BadgeService) {}

  public badges$: Observable<Badge[]>;
  editBadge: Badge;

  @Output() valueChange = new EventEmitter();

  @Input()
  badgeType: String = 'me';

  ngOnInit() {
    if (this.badgeType === 'user') {
      this.badges$ = this.badgeService.getBadges();
    } else {
      this.badges$ = this.badgeService.getMyBadges(); // .subscribe( badges => this.badges = badges);
    }
  }

  useBadge(badge) {
    this.valueChange.emit(badge);
  }
}
