import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Badge} from '../badge';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

export interface DialogData {
  animal: string;
  name: string;
}

/* <badge-choose (valueChange)='handleBadgeSelection($event)'></badge-choose> */

/**
 * @title Dialog Overview
 */
@Component({
  selector: 'badge-choose',
  templateUrl: 'badge-choose.component.html',
  styleUrls: ['badge-choose.component.css'],
})
export class BadgeChooseComponent {

  badge: Badge;
  name: string;

  destroySubject$: Subject<void> = new Subject();

  @Output() valueChange = new EventEmitter();

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(BadgeChooseDialogComponent, {
      width: 'auto',
      data: {name: this.name, badge: this.badge}
    });

    dialogRef.afterClosed().pipe(takeUntil(this.destroySubject$)).subscribe(result => {
      this.badge = result;
      this.valueChange.emit(result);
    });
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }

}

@Component({
  selector: 'badge-choose-dialog',
  templateUrl: 'badge-choose-dialog.html',
})
export class BadgeChooseDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<BadgeChooseDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Badge) {}

  badge: Badge;

  handleBadgeSelection(badge) {
    this.badge = badge;
    this.dialogRef.close(badge);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
