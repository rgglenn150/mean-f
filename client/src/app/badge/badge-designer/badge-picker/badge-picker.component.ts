import {Component, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'badge-picker',
  templateUrl: './badge-picker.component.html',
  styleUrls: ['./badge-picker.component.css']
})
export class BadgePickerComponent implements OnInit, OnChanges {

  @Input()
  settings;

  sizes = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 50, 55, 60, 65, 70, 75, 80];

  fonts = ['Oswald', 'Raleway', 'Montserrat', 'Lato', 'Open Sans', 'Roboto', 'Troche', 'Kobar', 'Westmeath'];

  constructor() { }

  ngOnInit() { }

  changeBackgroundColor (color) {
    this.settings.bgColor = color;
  }
/*
  changeBadgeColor (color) {
    this.settings.badgeColor = color;
  }
*/
  changeTextColor (color) {
    this.settings.text.color = color;
  }

  ngOnChanges (changes: SimpleChanges) {
    if (changes.hasOwnProperty('settings') && changes['settings'].hasOwnProperty('currentValue')) {
      this.settings = changes['settings'].currentValue;
    }
  }

}
