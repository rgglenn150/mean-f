import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BadgePickerComponent } from './badge-picker.component';

describe('BadgePickerComponent', () => {
  let component: BadgePickerComponent;
  let fixture: ComponentFixture<BadgePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BadgePickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BadgePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
