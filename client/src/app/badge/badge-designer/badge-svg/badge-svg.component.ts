import {Component, HostListener, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material';

@Component({
  selector: 'badge-svg',
  templateUrl: './badge-svg.component.html',
  styleUrls: ['./badge-svg.component.css']
})
export class BadgeSvgComponent implements OnInit, OnChanges {

  aspectRatio = false;
  movingOffset = { x: 0, y: 0 };
  showBorder = false;
  position: any = {x: 100, y: 50};

  @Input()
  dragBounds;

  @Input()
  rzContainment: String = 'container';

  @Input()
  settings;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {

    iconRegistry.addSvgIcon(
      'thumbs-up',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/svg/Badge1.svg'));
  }

  ngOnInit() {

  }

  /*@HostListener('window:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent): void {
    if (event.getModifierState &&
      (event.getModifierState('Meta') ||
        event.getModifierState('Alt') ||
        event.getModifierState('Control') ||
        event.getModifierState('Shift'))) {
      this.aspectRatio = true;
    }
  }

  @HostListener('window:keyup', ['$event'])
  onKeyUp(event: KeyboardEvent): void {
    this.aspectRatio = false;
  }*/

  onResizeStart(event) {
    this.showBorder = true;
  }

  onResizeStop(event) {
    // this.state = 'Resize Stopped';
    // this.size = event.size;
    this.showBorder = false;
  }

  onMoving(event) {
    this.movingOffset.x = event.x;
    this.movingOffset.y = event.y;
  }

  onDragBegin(event) {
    this.showBorder = true;
  }
  onDragEnd(event) {
    this.showBorder = false;
  }

  ngOnChanges (changes: SimpleChanges) {
    if (changes.hasOwnProperty('settings') && changes['settings'].hasOwnProperty('currentValue')) {
      this.settings = changes['settings'].currentValue;
    }
  }


}
