import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BadgeSvgComponent } from './badge-svg.component';

describe('BadgeSvgComponent', () => {
  let component: BadgeSvgComponent;
  let fixture: ComponentFixture<BadgeSvgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BadgeSvgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BadgeSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
