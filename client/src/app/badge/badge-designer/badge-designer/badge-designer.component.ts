import {Component, Input, OnDestroy, OnInit} from '@angular/core';

/*

Example: <badge-designer [settings]="settings"></badge-designer>

 */

@Component({
  selector: 'badge-designer',
  templateUrl: './badge-designer.component.html',
  styleUrls: ['./badge-designer.component.css']
})
export class BadgeDesignerComponent implements OnInit {

  constructor() {}

  @Input() settings;
  @Input() index?: string;
  // destroySubject$: Subject<void> = new Subject();

  ngOnInit() {
    this.index = this.index ? this.index : '';
    this.settings = this.settings ? this.settings :
      {
        name: 'Default Badge',
        bgColor : '#A0C2BB',
        text: {
          color: '#FFF',
          font: 'Oswald',
          width: 150,
          height: 150,
          fontSize: 22,
          position: {x: 125, y: 100},
          name: 'Badge Name'
        },
        image: {
          path: '/assets/img/badges/Badge1.png',
          width: 150,
          height: 150,
          position: {x: 100, y: 50}
        }
      };
  }

  /*ngOnDestroy() {
    this.destroySubject$.next();
  }*/

}
