import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BadgeTextareaComponent } from './badge-textarea.component';

describe('BadgeTextareaComponent', () => {
  let component: BadgeTextareaComponent;
  let fixture: ComponentFixture<BadgeTextareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BadgeTextareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BadgeTextareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
