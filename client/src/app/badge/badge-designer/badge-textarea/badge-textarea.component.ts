import {Component, ElementRef, HostListener, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';

@Component({
  selector: 'badge-textarea',
  templateUrl: './badge-textarea.component.html',
  styleUrls: [
    './badge-textarea.component.css']
})
export class BadgeTextareaComponent implements OnInit, OnChanges {

  aspectRatio = false;
  movingOffset = { x: 0, y: 0 };
  showBorder = false;

  @Input()
  dragBounds;

  @Input()
  rzContainment: String = 'container';

  @Input()
  settings;

  constructor() { }

  ngOnInit() {
    this.settings.text.name = this.settings.text.name ? this.settings.text.name : 'Badge Title';
  }

  @HostListener('window:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent): void {
    if (event.getModifierState &&
      (event.getModifierState('Meta') ||
        event.getModifierState('Alt') ||
        event.getModifierState('Control') ||
        event.getModifierState('Shift'))) {
      this.aspectRatio = true;
    }
  }

  @HostListener('window:keyup', ['$event'])
  onKeyUp(event: KeyboardEvent): void {
    this.aspectRatio = false;
  }

  onResizeStart(event) {
    this.showBorder = true;
  }

  onResizeStop(event) {
    // this.state = 'Resize Stopped';
    // this.size = event.size;
    this.settings.text.width = event.size.width;
    this.settings.text.height = event.size.height;
    this.showBorder = false;
  }

  onMoving(event) {
    this.movingOffset.x = event.x;
    this.movingOffset.y = event.y;
    this.settings.text.position.x = event.x;
    this.settings.text.position.y = event.y;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('settings') && changes['settings'].hasOwnProperty('currentValue')) {
      this.settings = changes['settings'].currentValue;
    }
  }

}
