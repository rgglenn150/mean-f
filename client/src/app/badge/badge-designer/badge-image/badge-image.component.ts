import {Component, HostListener, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'badge-image',
  templateUrl: './badge-image.component.html',
  styleUrls: ['./badge-image.component.css']
})
export class BadgeImageComponent implements OnInit, OnChanges {

  aspectRatio = false;
  movingOffset = { x: 0, y: 0 };
  showBorder = false;

  @Input()
  dragBounds;

  @Input()
  rzContainment: String = 'container';

  @Input()
  settings;

  constructor() {
  }

  ngOnInit() {
  }

  @HostListener('window:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent): void {
    if (event.getModifierState &&
      (event.getModifierState('Meta') ||
        event.getModifierState('Alt') ||
        event.getModifierState('Control') ||
        event.getModifierState('Shift'))) {
      this.aspectRatio = true;
    }
  }

  @HostListener('window:keyup', ['$event'])
  onKeyUp(event: KeyboardEvent): void {
    this.aspectRatio = false;
  }

  onResizeStart(event) {
    this.showBorder = true;
  }

  onResizeStop(event) {
    // this.state = 'Resize Stopped';
    // this.size = event.size;
    this.showBorder = false;
    this.settings.image.width = event.size.width;
    this.settings.image.height = event.size.height;
  }

  onMoving(event) {
    this.movingOffset.x = event.x;
    this.movingOffset.y = event.y;
    this.settings.image.position.x = event.x;
    this.settings.image.position.y = event.y;
  }

  onDragBegin(event) {
    this.showBorder = true;
  }
  onDragEnd(event) {
    this.showBorder = false;
  }

  ngOnChanges (changes: SimpleChanges) {
    if (changes.hasOwnProperty('settings') && changes['settings'].hasOwnProperty('currentValue')) {
      this.settings = changes['settings'].currentValue;
    }
  }


}
