export interface Badge {
  _id: string;
  image: string;
  name: string;
  company: string;
  description: string;
  awarded_date: Date;
  campaign: string;
  prize: string;
  prizeDescription: string;
  makePrivate: Boolean;
  turnEmailNotificationsOff: Boolean;
  turnCommentsOff: Boolean;
  settings: object;
}
