import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Badge } from '../badge';
import { takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { ReceivedBadgeDialogComponent } from '../received-badge-dialog/received-badge-dialog.component';
import { isPending } from 'q';

@Component({
  selector: 'badges',
  templateUrl: './badges.component.html',
  styleUrls: ['./badges.component.css']
})
export class BadgesComponent implements OnInit, OnDestroy {

  //@Input() badges$: Observable<Badge[]>;
  //destroySubject$: Subject<void> = new Subject();

  @Input() badges: Badge[] = [];
  @Input() searchText: string;
  @Input() isPending: boolean;

  myBadges: Badge[] = [];
  constructor(private dialog: MatDialog) { }

  ngOnInit() {
    /*if (this.badges.length < 1 && this.badges$) {
      this.myBadges = this.badges$.pipe(takeUntil(this.destroySubject$).subscribe(b => this.badges = b);
    };*/
    //
    //console.log(typeof this.badges$);
  }

  showBadges() {
    //console.log(typeof this.badges$);
  }

  ngOnDestroy() {
    //this.destroySubject$.next();
  }



  viewPendingBadge(data): void {
    console.log('RGDB test', data,this.isPending);
    if (this.isPending) {
      const dialogRef = this.dialog.open(ReceivedBadgeDialogComponent, {
        width: '25%',
        data: { id:data._id,
          badge: data.badge }
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');

      });
    }

  }

}
