export interface BadgeDefault {
  _id: string;
  name: string;
  color: string;
  path: string;
}
