import { Component } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import {BadgeService} from '../badge.service';
import { BadgeDefault } from './BadgeDefault';
import {Badge} from '../badge';
import {Observable} from 'rxjs';

@Component({
  selector: 'badge-select',
  templateUrl: 'badge-select.component.html',
  styleUrls: ['./badge-select.component.css']
})
export class BadgeSelectComponent {
  constructor(private auth: AuthService, private badgeService: BadgeService) {}

  badgesDefault$: Observable<BadgeDefault[]>;
  badge: Badge;
  searchText;

  ngOnInit() {
    this.badgesDefault$ = this.badgeService.getBadgesDefault();
  }
  handleBadgeUpload(badge) {
    this.badge = badge;
  }
}
