import { Component, OnInit } from '@angular/core';
import { OrganizationService } from './organization.service';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.css']
})
export class OrganizationComponent implements OnInit {
  searchText;
  orgs;
  constructor(private orgService: OrganizationService) { }

  searchHandler(value) {

    if (value) {

      console.log(value);
      this.orgService.searchOrg("orgName",value).subscribe(res=>{
      
        this.orgs= res;

      });
      //this.orgs = this.eventService.searchEvents("name", value);
  
    } else {
      this.getAllOrgs();
    }
  }

  ngOnInit() {
    this.getAllOrgs();

  }

  getAllOrgs() {
    this.orgService.getOrganizations().subscribe(res => {
      console.log(res);
      this.orgs = res;
    }, e => {
      try {
        if (e._body) {
          const body = JSON.parse(e._body);
          if (body.message) {
            this.orgService.notif(body.message);
          }
        }
      } catch (e) { }
    });
  }

}
