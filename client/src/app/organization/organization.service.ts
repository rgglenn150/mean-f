import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { takeUntil, catchError } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { HandleError, HttpErrorHandler } from '../http-error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  BASE_URL = environment.protocal + environment.host + '/api/organization';
  destroySubject$: Subject<void> = new Subject();
  private handleError: HandleError;

  constructor(private http: HttpClient,
    private snackbar:MatSnackBar,private httpErrorHandler:HttpErrorHandler) { 

      this.handleError = httpErrorHandler.createHandleError('OrganizationService');
    }


  public notif(message) {
    this.snackbar.open(message, 'Close', { duration: 5000 });
  }

  getOrganizations(){
    return this.http.get(this.BASE_URL +'/').pipe(takeUntil(this.destroySubject$));
  }

  getOrganization(id:string){
    return this.http.get(this.BASE_URL +'/'+id).pipe(takeUntil(this.destroySubject$));
  }

  searchOrg(field: string, search: string): Observable<Event[]> {
    const queryParams =  field && search ? '?field=' + field + '&search=' + search : '';
    return this.http.get<Event[]>(this.BASE_URL + '/search'+ queryParams)
      .pipe(
        catchError(this.handleError('getOrg', []))
      );
  }

  register(org) {
    
    this.http.post(this.BASE_URL + '/', org).pipe(takeUntil(this.destroySubject$)).subscribe(res => {

      // for message display only
      this.notif('Organization Registration was sent for review.');
    
    }, e => {
     
      try {
        if (e._body) {
          const body = JSON.parse(e._body);
          if (body.message) {
            this.notif(body.message);
          }
        }
      } catch (e) { }
    }, () => { });
  }

}
