import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrganizationService } from '../organization.service';
import { Subscription, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-organization-profile',
  templateUrl: './organization-profile.component.html',
  styleUrls: ['./organization-profile.component.css']
})
export class OrganizationProfileComponent implements OnInit {

  org;
  private sub: Subscription;
  private orgId: string;
  destroySubject$: Subject<void> = new Subject();

  constructor(private route: ActivatedRoute, private orgService: OrganizationService) {

  }

  ngOnInit () {
    this.route.params.pipe(takeUntil(this.destroySubject$)).subscribe(params => {
      this.orgId = params['_id'];
    });
    
    if (this.orgId) {
      this.orgService.getOrganization(this.orgId).subscribe((res:any) => {
        this.org = res;
      }, err => {
      });
    }
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }
}
