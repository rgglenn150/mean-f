import { Component } from '@angular/core'

@Component({
    selector: 'user',
    template: `
        <mat-card class="card">
        <mat-card-content>
            <mat-form-field>
                <input [(ngModel)]="model.firstName" matInput placeholder="First Name" />
            </mat-form-field>
            <mat-form-field>
                <input [(ngModel)]="model.lastName" matInput placeholder="Last Name" />
            </mat-form-field>
            <mat-card-actions>
                <button (click)="post()" mat-raised-button color="primary">SAVE CHANGES</button>
            </mat-card-actions>
        </mat-card-content>
        </mat-card>
    `
})
export class UserComponent {

    constructor(){}

    model = {
        firstName: "",
        lastName: ""
    }

    ngOnInit(){
    }

    post(){

    }
}
