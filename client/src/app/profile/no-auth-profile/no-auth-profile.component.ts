import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { ActivatedRoute } from '@angular/router';
import { takeUntil, map } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { ProfileToNavService } from 'src/app/utils/profileToNavService';
import { Badge } from 'src/app/badge/badge';
import { BadgeUser } from 'src/app/badge/badge-user';
import { BadgeService } from 'src/app/badge/badge.service';

@Component({
  selector: 'app-no-auth-profile',
  templateUrl: './no-auth-profile.component.html',
  styleUrls: ['./no-auth-profile.component.css']
})
export class NoAuthProfileComponent implements OnInit {
  userProfile: object;
  username: string;
  constructor(private auth: AuthService, private route: ActivatedRoute, private profileToNav: ProfileToNavService, private badgeService: BadgeService) { }
  destroySubject$: Subject<void> = new Subject();
  profileStatus: string;
  badges$: Observable<Badge[]>;
  myBadges$: Observable<BadgeUser[]>;

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.username = params['username'];
    });

    this.auth.getUserByUsername(this.username).pipe(takeUntil(this.destroySubject$)).subscribe((profile: any) => {

      this.profileStatus = profile.status;
      this.userProfile = profile;
      this.profileToNav.updateData(profile);



      this.myBadges$ = this.badgeService.getReceivedBadgesByUsername(this.username, ['badge']).pipe(
        map(b => { // Badges components needs badge-user -> badge in the top level directory, so we map
          return b.map(e => e.badge);
        })
      );

      this.badges$ = this.badgeService.getCreatedBadgesByUsername(this.username, ['user']);

    });
  }

}
