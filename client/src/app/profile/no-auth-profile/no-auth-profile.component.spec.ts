import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoAuthProfileComponent } from './no-auth-profile.component';

describe('NoAuthProfileComponent', () => {
  let component: NoAuthProfileComponent;
  let fixture: ComponentFixture<NoAuthProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoAuthProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoAuthProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
