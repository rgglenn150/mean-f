import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Badge } from 'src/app/badge/badge';
import { BadgeUser } from 'src/app/badge/badge-user';
import { AuthService } from 'src/app/auth/auth.service';
import { BadgeService } from 'src/app/badge/badge.service';
import { map, takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileToNavService } from 'src/app/utils/profileToNavService';

@Component({
  selector: 'app-public-profile',
  templateUrl: './public-profile.component.html',
  styleUrls: ['./public-profile.component.css']
})
export class PublicProfileComponent implements OnInit {
  private user: any;
  badges$: Observable<Badge[]>;
  myBadges$: Observable<BadgeUser[]>
  destroySubject$: Subject<void> = new Subject();
  username: string;
  routeParams: any;
  userProfile: object;
  searchText;
  profileStatus;
  constructor(public auth: AuthService, private badgeService: BadgeService, private router: Router, private route: ActivatedRoute, private profileToNav: ProfileToNavService) {
    this.user = auth.authenticatedUser;
    this.routeParams = this.route.params.subscribe(params => {
      this.username = params['username'];
    });
    //if (!auth.authenticatedUser) this.router.navigate(['/' + this.username + '/public']);
    //this.checkLoggedIn();
  }

  ngOnInit() {
    this.auth.user.pipe(takeUntil(this.destroySubject$)).subscribe(data => {
      this.user = data;
      this.checkLoggedIn();
    });

    /* this.myBadges$ = this.badgeService.getMyBadges(['badge']).pipe(
      map(b => { // Badges components needs badge-user -> badge in the top level directory, so we map
        return b.map(e => e.badge);
      })
    ); */


    this.badges$ = this.badgeService.getCreatedBadgesByUsername(this.username, ['user']);

    this.myBadges$ = this.badgeService.getReceivedBadgesByUsername(this.username, ['badge']).pipe(
      map(b => { // Badges components needs badge-user -> badge in the top level directory, so we map
        return b.map(e => e.badge);
      })
    );

    this.auth.getUserByUsername(this.username).pipe(takeUntil(this.destroySubject$)).subscribe((profile:any) => {
      this.userProfile = profile;
      this.profileStatus = profile.status;
  
      this.profileToNav.updateData(profile);
    });

  }



  private checkLoggedIn() {
    if (!this.auth.isAuthenticated) {
      this.auth.logout();
    }
  }

  ngOnDestroy() {
    this.profileToNav.updateData(null)
    this.routeParams.unsubscribe();
    this.destroySubject$.next();
  }

}
