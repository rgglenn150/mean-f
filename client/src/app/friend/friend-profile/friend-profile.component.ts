import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { FriendService } from '../friend.service';
import { Friend } from '../friend';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ProfileToNavService } from 'src/app/utils/profileToNavService';

@Component({
  selector: 'friend',
  templateUrl: './friend-profile.component.html',
  styleUrls: ['./friend-profile.component.css']
})
export class FriendProfileComponent implements OnInit, OnDestroy {
  constructor(private auth: AuthService, private friendService: FriendService, private route: ActivatedRoute, private profileToNav: ProfileToNavService) { }

  friends: Friend[];
  public selectedFriend;
  destroySubject$: Subject<void> = new Subject();
  friendId: string;
  ngOnInit() {
    this.route.params.pipe(takeUntil(this.destroySubject$)).subscribe(params => {
      this.friendId = params['id'];

    });


    this.friendService.getFriend(this.friendId).subscribe(result => {
   
      this.profileToNav.updateData(result[0].friend);

    })

  }

  ngOnDestroy() {
    this.profileToNav.updateData(null);
    this.destroySubject$.next();
  }
}
