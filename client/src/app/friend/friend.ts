export interface Friend {
  _id: string;
  user: string;
  friend: object;
  relationship: string;
  token: string;
  created: Date;
}
