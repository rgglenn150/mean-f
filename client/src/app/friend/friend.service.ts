import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { AuthService } from '../auth/auth.service';
import { HandleError, HttpErrorHandler } from '../http-error-handler.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Friend } from './friend';
import { catchError, finalize, last } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Observable, Subject } from 'rxjs';
import { Badge } from '../badge/badge';
import { MessageService } from '../utils/message.service';

@Injectable()
export class FriendService {
  BASE_URL = environment.protocal + environment.host + '/api/user';
  private handleError: HandleError;

  constructor(
    private http: HttpClient, private snackBar: MatSnackBar, private auth: AuthService, private messageService: MessageService, private httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('BadgeService');
  }

  private friendsStore = [];
  private friendSubject = new Subject();

  getFriends(): Observable<any> {
    return this.http.get<Friend[]>(this.BASE_URL + '/friend')
      .pipe(
        catchError(this.handleError('getFriends', []))
      );
  }

  getFriend(_id: string): Observable<any> {

    return this.http.get<Friend[]>(this.BASE_URL + '/friend/' + _id)
      .pipe(
        catchError(this.handleError('getFriend', []))
      );

  }

  getPendingFriends(): Observable<Friend[]> {
    return this.http.get<Friend[]>(this.BASE_URL + '/friend/pending')
      .pipe(
        catchError(this.handleError('getPendingFriends', []))
      );
  }

  removeRequest(id): Observable<{}> {
    return this.http.delete(this.BASE_URL + '/friend/' + id)
      .pipe(
        catchError(this.handleError('removeRequest'))
      );
  }

  completeInvite(friend): Observable<Friend> {
    return this.http.post<any>(this.BASE_URL + '/friend/complete/' + friend._id, {})
      .pipe(
        catchError(this.handleError('invite', friend))
      );
  }

  getExampleFriends() {
    const friend1 = {
      _id: '010101',
      created: new Date(),
      company: 'blahcompany',
      firstName: 'Adam',
      lastName: 'Aderson',
      email: 'duh@dumb.com',
      path: '/assets/img/profile-pic-1.png'
    };
    const friend2 = {
      _id: '1111',
      created: new Date(),
      company: 'blahcompany',
      firstName: 'Ashley',
      lastName: 'Abby',
      email: 'duh@dumb.com',
      path: '/assets/img/dssdf.png'
    };
    const friend3 = {
      _id: '2222',
      created: new Date(),
      company: 'blahcompany',
      firstName: 'Ben',
      lastName: 'Caterile',
      email: 'duh@dumb.com',
      path: '/assets/img/circular-profile.png'
    };
    const friend4 = {
      _id: '3333',
      created: new Date(),
      company: 'blahcompany',
      firstName: 'Jen',
      lastName: 'Hemingway',
      email: 'duh@dumb.com',
      path: '/assets/img/indedfdsx.png'
    };

    const friendsStore = [];
    friendsStore.push(friend1);
    friendsStore.push(friend2);
    friendsStore.push(friend3);
    friendsStore.push(friend4);
    return friendsStore;
  }

  invite(friend): Observable<any> {
    return this.http.post<any>(this.BASE_URL + '/friend/invite', friend)
      .pipe(
        catchError(this.handleError('invite', friend))
      );
  }
  /*getBadgesDefault (): Observable<any> {
    return this.http.get<Badge[]>(this.BASE_URL + '/default')
      .pipe(
        catchError(this.handleError('getBadgesDefault', []))
      );
  }

  getMyBadges (): Observable<Badge[]> {
    return this.http.get<Badge[]>(this.BASE_URL + '/me')
      .pipe(
        catchError(this.handleError('getMyBadges', []))
      );
    // Add safe, URL encoded search parameter if there is a search term
    // const options = params: new HttpParams().set('symbol', symbol)} : {};
  }
  getBadges (): Observable<Badge[]> {
    return this.http.get<Badge[]>(this.BASE_URL + '/')
      .pipe(
        catchError(this.handleError('getBadges', []))
      );
    // Add safe, URL encoded search parameter if there is a search term
    // const options = params: new HttpParams().set('symbol', symbol)} : {};
  }
  create (badge: Badge): Observable<Badge>{
    return this.http.post<Badge>(this.BASE_URL + '/', badge)
      .pipe(
        catchError(this.handleError('CreateBadge', badge))
      );
  }*/
}
