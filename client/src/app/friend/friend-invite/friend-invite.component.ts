import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Friend } from '../friend';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { MessageService } from '../../utils/message.service';
import { FriendService } from '../friend.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NotificationService } from 'src/app/notification/notification.service';
import { AuthService } from 'src/app/auth/auth.service';

export interface DialogData {
  animal: string;
  name: string;
}

/* <badge-choose (valueChange)='handleBadgeSelection($event)'></badge-choose> */

/**
 * @title Dialog Overview
 */
@Component({
  selector: 'friend-invite',
  templateUrl: 'friend-invite.component.html',
  styleUrls: ['friend-invite.component.css'],
})
export class FriendInviteComponent {

  friend: Friend;
  name: string;
  destroySubject$: Subject<void> = new Subject();

  @Output() valueChange = new EventEmitter();

  constructor(public dialog: MatDialog) { }

  openDialog(): void {
    const dialogRef = this.dialog.open(FriendInviteDialogComponent, {
      width: 'auto',
      data: { name: this.name, friend: this.friend }
    });

    dialogRef.afterClosed().pipe(takeUntil(this.destroySubject$)).subscribe(result => {
      this.friend = result;
      this.valueChange.emit(result);
    });
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }

}

@Component({
  selector: 'friend-invite-dialog',
  templateUrl: 'friend-invite-dialog.html',
  styleUrls: ['friend-invite.component.css'],
})
export class FriendInviteDialogComponent {
  form;
  constructor(
    public dialogRef: MatDialogRef<FriendInviteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Friend,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private friendService: FriendService,
    private notificationService: NotificationService,
    private auth: AuthService) {

    this.form = formBuilder.group({
      email: ['', [Validators.required, emailValid()]],
      relationship: ['', Validators.required]
      /*friends: this.formBuilder.group({
        email: [''],
        relationship: ['']
      })*/
    });

  }

  friend;
  loading = false;
  relationships = ['friend', 'coworker', 'employee', 'supervisor'];
  destroySubject$: Subject<void> = new Subject();

  onSubmit() {
    if (this.form.valid) {
      this.loading = true;
      this.friendService.invite(this.form.value).pipe(takeUntil(this.destroySubject$)).subscribe((result) => {
        if (result && result.email) {
          this.messageService.add('You cannot invite yourself!');
        } else {
         
          let user: any = this.auth.authenticatedUser;
          let thumbnail = user.profilePhoto ? user.profilePhoto : "assets/img/noun_profile_888930.png";
         
          this.notificationService.sendNotif(result.userId, "You have a new friend request from " + user.firstName + ' ' + user.lastName, '/friends', thumbnail);
          this.messageService.add('Invite sent!');
        }
        this.loading = false;

        this.dialogRef.close(this.form.value);
      }, e => { console.log(e); this.loading = false; });
    } else {
      this.messageService.add('Please fill out the form correctly and completely');
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  isValid(control) {
    return this.form.controls[control].invalid && this.form.controls[control].touched;
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }

}

function emailValid() {
  return control => {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(control.value) ? null : { invalidEmail: true }
  }
}
