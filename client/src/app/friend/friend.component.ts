import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { FriendService } from './friend.service';
import { Friend } from './friend';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.css']
})
export class FriendComponent {
  constructor(private auth: AuthService, private friendService: FriendService,private router: Router) {}

  placeholderFriends = this.friendService.getExampleFriends();
  friends: Friend[];
  public selectedFriend;

  destroySubject$: Subject<void> = new Subject();

  ngOnInit() {
    this.selectedFriend = { _id: '', firstName: '', lastName: '', email: '', company: '', created: new Date(), path: ''};
    this.friendService.getExampleFriends();
  }

  selectFriend(friend) {
    this.selectedFriend = friend;
   
    this.router.navigate(['friends/'+friend]);
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }
}
