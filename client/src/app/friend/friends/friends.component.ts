import {Component, Input, OnInit} from '@angular/core';
import {FriendService} from '../friend.service';

@Component({
  selector: 'friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {

  @Input() friends;
  placeholderFriends = this.friendService.getExampleFriends();

  constructor(private friendService: FriendService) { }

  ngOnInit() {

  }

}
