import {Component, EventEmitter, Input, Output} from '@angular/core';
import { FriendService } from '../friend.service';
import { Friend } from '../friend';
import { MessageService } from '../../utils/message.service';
import { DialogService } from '../../utils/dialog-service/dialog.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

/*

<friend-select [selectedFriends]="selectedFriends" (valueChange)='handleThisNewFunction($event)'></friend-select>

 */


@Component({
  selector: 'friend-select',
  templateUrl: 'friend-select.component.html',
  styleUrls: ['./friend-select.component.css']
})
export class FriendSelectComponent {
  constructor(private friendService: FriendService, private messageService: MessageService, private dialogService: DialogService) {}

  placeholderFriends = this.friendService.getExampleFriends();
  friends: Friend[];
  pendingFriends: Friend[];
  public selectedFriend = undefined;
  destroySubject$: Subject<void> = new Subject();

  @Input()
  friendType: String = 'select';

  // An array of selected friends by default. Used to populate your friends list of who is already selected
  @Input() selectedFriends?: Array<any> = [];

  // An optional array of disabled friends. Used to disable any friend.
  // If a disableMessage is not included, then no message will be displayed
  // @Input() disabledFriends?: Array<any> = [];
  // An optional string to display a message about a disabled friend. Example: Invite sent
  // @Input() disabledMessage?: String;

  // emits the whole array of friends selected or de-selected
  @Output() valueChange = new EventEmitter();

  // emits for every time a single friend is selected or de-selected
  @Output() friendChange = new EventEmitter();

  ngOnInit() {

    this.selectedFriend = { _id: '', firstName: '', lastName: '', email: '', company: '', created: new Date(), path: ''};
    this.friendService.getPendingFriends().pipe(takeUntil(this.destroySubject$)).subscribe( friends => this.pendingFriends = friends);
    this.friendService.getFriends()
      .pipe(takeUntil(this.destroySubject$))
      .subscribe( friends => this.friends = friends);
  }

  selectFriend(friend) {
    this.selectedFriend = friend;
    if (this.selectedFriends.includes(friend._id)) {
      this.selectedFriends = this.selectedFriends.filter(id => id !== friend._id);
      this.friendChange.emit({ _id: friend._id, state: 'remove'});
    } else {
      this.selectedFriends.push(friend._id);
      this.friendChange.emit({ _id: friend._id, state: 'add'});
    }
    this.valueChange.emit(this.selectedFriends);
  }
  completeInvite(userFriend) {
    this.friendService.completeInvite(userFriend).pipe(takeUntil(this.destroySubject$)).subscribe( (result) => {
      this.messageService.add('You are now connected');
      // @ts-ignore
      this.friends.push(result);
      this.pendingFriends = this.pendingFriends.filter(p => p !== userFriend);
    });
  }
  removeRequest(userFriend) {
    this.dialogService.openDialog('Are you sure you want to remove this request?', 'Remove').pipe(takeUntil(this.destroySubject$)).subscribe((result) => {
      if (result) {
        this.friendService.removeRequest(userFriend._id).pipe(takeUntil(this.destroySubject$)).subscribe( (done) => {
          this.messageService.add('Successfully removed request');
          this.pendingFriends = this.pendingFriends.filter(p => p !== userFriend);
        });
      }
    });
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }
}
