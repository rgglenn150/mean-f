import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/auth/auth.service';
import { MessageService } from 'src/app/utils/message.service';
import { ProfanityFilterService } from 'src/app/utils/profanity-filter.service';

@Component({
  selector: 'app-update-name',
  templateUrl: './update-name.component.html',
  styleUrls: ['./update-name.component.css']
})
export class UpdateNameComponent implements OnInit {

  @Input() user;
  form: FormGroup;

  @Output() updatedName = new EventEmitter<string>();

  constructor(private formBuilder: FormBuilder, private auth: AuthService, private message: MessageService, private profanityFilter:ProfanityFilterService) {

  }

  ngOnInit() {

    this.form = this.formBuilder.group({
      firstName: ['', [Validators.required,this.isClean()]],
      lastName: ['', [Validators.required,this.isClean()]],
    });
  }

  isClean(){
    return control => {
      return !this.profanityFilter.check(control.value) ? null : { hasProfanity: true }
    }
  
  }


  saveName(form: FormGroup) {
    const formValue = form.controls;
    const firstName = formValue.firstName.value;
    const lastName = formValue.lastName.value;
    if (this.form.valid) {
      this.auth.updateName(firstName, lastName).subscribe((result: any) => {
        if (result) {
          localStorage.setItem('firstName', result.firstName);
          localStorage.setItem('lastName', result.lastName);
          this.updatedName.emit(result.firstName + ' ' + result.lastName);
          this.message.add("Display Name Changed.");

          this.form.reset();
        } else {
          this.message.add("Failed to update Name.");
        }
      });
    } else {
      this.message.add("First Name and Last Name are required");
    }
  }

}
