import { Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileToNavService } from '../utils/profileToNavService';
import { ProfileDropdownComponent } from './profile-dropdown/profile-dropdown.component';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'nav-home',
  templateUrl: 'nav-home.component.html',
  styleUrls: ['./nav-home.component.css'],
  
})
export class NavHomeComponent {
  public user;
  public profilePhoto;

  destroySubject$: Subject<void> = new Subject();
  dropdown = false;
  dropdownOpen = false;
  username: string = null;
  userProfile = null;
  profileStatus=null;

  constructor(public auth: AuthService, private router: Router, private route: ActivatedRoute, public profileToNav: ProfileToNavService) {
    this.user = auth.authenticatedUser;
   

  }

  @ViewChild(ProfileDropdownComponent ) profileDropdown: ProfileDropdownComponent ; 

  ngOnInit() {
    this.auth.user.pipe(takeUntil(this.destroySubject$)).subscribe(data => {
      this.user = data;
    });

   
   
   
    this.profileToNav.getData().subscribe(data => {
      this.userProfile = data;
    })




  }

  toggleDropdown() {

    this.profileDropdown.getSubAccounts();

    //this.dropdown = !this.dropdown;
    if (!this.userProfile) {
      this.dropdown = true;
      this.dropdownOpen = true;
    }
  }

  handleDropdown() {
    this.dropdown = false;
    this.dropdownOpen = false;
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }
}
