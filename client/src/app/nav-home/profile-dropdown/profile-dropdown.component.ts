import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl, FormGroupDirective } from '@angular/forms';
import { UserComponent } from 'src/app/user.component';
import { AuthService } from 'src/app/auth/auth.service';
import { SubAccountService } from 'src/app/sub-account/sub-account.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DialogService } from 'src/app/utils/dialog-service/dialog.service';
import { MessageService } from 'src/app/utils/message.service';
import { ProfanityFilterService } from 'src/app/utils/profanity-filter.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { EmailService } from 'src/app/utils/email.service';

@Component({
  selector: 'app-profile-dropdown',
  templateUrl: './profile-dropdown.component.html',
  styleUrls: ['./profile-dropdown.component.css'],
})


export class ProfileDropdownComponent implements OnInit {

  @Output() valueChange = new EventEmitter();



  @Input()
  dropdown = false;
  @Input()
  dropdownOpen = false;
  email = '';
  emails: string[] = [];
  subAccounts: { status: string, _id: string }[] = [];
  emailForm: FormGroup;
  destroySubject$: Subject<void> = new Subject();
  public user;
  displayName: string;
  updateNameToggle = false;
  isCompanyEmail = false;
  selectedOrgId = '';
  showCompanyField = false;
  showSubAccountForm = false;

  constructor(
    private formBuilder: FormBuilder,
    public auth: AuthService,
    private eRef: ElementRef,
    private subAccountService: SubAccountService,
    private dialogService: DialogService,
    private messageService: MessageService,
    private profanity: ProfanityFilterService,
    private emailUtils: EmailService
  ) {
    this.dropdownOpen = false;
    this.emailForm = this.getBlankForm();
    this.user = auth.authenticatedUser;
    this.displayName = this.user.firstName + ' ' + this.user.lastName;

  }

  toggleCompanyField() {
    this.dropdownOpen = true;
    this.showCompanyField = !this.showCompanyField;
  }



  isValid(control) {
    return this.emailForm.controls[control].invalid && this.emailForm.controls[control].touched;
  }

  orgExists() {
    return control => {
      return this.selectedOrgId !== undefined ? null : { doesNotExists: true }
    }
  }

  setOrgId(event) {
   
    this.selectedOrgId = event;
    this.dropdownOpen = true;
  }

  getBlankForm() {
    return this.formBuilder.group({
      email: ['', [Validators.required, this.companyEmailCheck(), this.emailValid()]],
      orgName: ['', [this.orgExists()]],
      orgId: [null],
    });
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (!this.dropdownOpen && !this.eRef.nativeElement.contains(event.target)) {
      this.valueChange.emit(true);
    } else {
      this.dropdownOpen = false;
    }
  }

  ngOnInit() {
    this.auth.user.pipe(takeUntil(this.destroySubject$)).subscribe(data => {
      this.user = data;
    });
    this.getSubAccounts();
  }

  getSubAccounts() {
    if (this.user._id) {
      this.subAccountService.getSubAccounts(this.user._id).pipe(takeUntil(this.destroySubject$)).subscribe(res => {
        if (res) {
          this.emails = [];
          this.subAccounts = [];

          res.forEach(subAccount => {
            if (subAccount.data) {
              this.emails.push(subAccount.data.email);
              this.subAccounts[subAccount.data.email] = subAccount;
            }
          });
        } else {
          console.log(' no result found');
        }
      });
    } else {
      console.log(' no user._id');
    }
  }

  companyEmailCheck() {
    return control => {
      if (this.emailUtils.isCompanyEmail(control.value) && control.value) {
      
        this.isCompanyEmail = true;
      
      } else {
        this.selectedOrgId = null;
        this.isCompanyEmail = false;
      
      }
    }
  }

  onSubmit(emailForm: FormGroup, formDirective: FormGroupDirective) {
    const formValue = emailForm.controls.email.value;

    /*  if (this.emailUtils.isCompanyEmail(formValue)) {
       this.isCompanyEmail = true;
       this.dialogService.openDialog("Looks like you're using a company email. Would you like to be connected to an org?", 'Yes');
     } else { */
    this.isCompanyEmail = false;
    this.subAccountService.addSubAccount(formValue,this.selectedOrgId)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((res: {
        status: string,
        message: string,
        _id: string
      }) => {
        if (res) {
          if (res.status !== 'failed') {
            this.dialogService.openDialog('Verification has been sent to the email!', null);
            //   this.messageService.add('Verification has been sent to the email!');
            this.emails.push(formValue);
            this.subAccounts[formValue] = { status: res.status, _id: res._id };
            this.showCompanyField = false;
            emailForm.reset();
            formDirective.resetForm();
          } else {
            alert(res.message);
          }
        }
        else {
          this.messageService.add('Something went wrong!');
        }
      });
    //   }

  }

  removeEmailHandler(email, id) {
    this.dropdownOpen = false;
    this.dialogService.openDialog("Are you sure you want to remove this email?", "Yes").pipe(takeUntil(this.destroySubject$)).subscribe((result) => {
      console.log('dialogService Data', result);
      if (result) {

        this.emails = this.emails.filter((data) => data != email);
        delete this.subAccounts[email];
        this.subAccountService.deleteSubAccount(id)
          .pipe(takeUntil(this.destroySubject$))
          .subscribe(res => {
            this.messageService.add('Delete Sub Account: ' + res.message);
          });
      }

    });

  }


  emailValid() {

    return control => {
      var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (this.profanity.check(control.value)) this.messageService.add('Profanity not allowed!');
      return regex.test(control.value) && !this.profanity.check(control.value) ? null : { invalidEmail: true }
    }
  }

  getUpdatedName(updatedName: string) {
    this.updateNameToggle = false;
    this.displayName = updatedName;
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }
}
