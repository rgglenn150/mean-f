import {Component, Inject, OnInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {Badge} from "../../badge/badge";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {BadgeUploadService} from "../../badge/badge-upload/badge-upload.service";
import {BadgeService} from "../../badge/badge.service";
import {MessageService} from "../../utils/message.service";

@Component({
  selector: 'app-profile-upload',
  templateUrl: './profile-upload.component.html',
  styleUrls: ['./profile-upload.component.css']
})
export class ProfileUploadComponent  {

/*
  badge: Badge;
  message: string;
  image: string = null;
  form: FormGroup;
  loading: Boolean = false;

  destroySubject$: Subject<void> = new Subject();

  constructor(
    private uploaderService: BadgeUploadService,
    private formBuilder: FormBuilder,
    private badgeService: BadgeService,
    private messageService: MessageService
  ){ }

  onPicked(input: HTMLInputElement) {
    const file = input.files[0];
    if (file) {
      this.loading = true;
      this.uploaderService.upload(file).pipe(takeUntil(this.destroySubject$)).subscribe(
        msg => {
          input.value = null;
          // @ts-ignore
          this.image = msg.image ? msg.image : '';
          // @ts-ignore
          this.message = msg.message;
          this.messageService.add(this.message);
          this.loading = false;
        },
        e => {
          this.loading = false;
        }

      );
    }
  }

  onSubmit() {
    if (this.form.valid) {
      const newBadge: Badge = this.form.value as Badge;
      newBadge.image = this.image;
      this.badgeService.create(newBadge).pipe(takeUntil(this.destroySubject$)).subscribe(data => {
        this.form.reset();
        this.messageService.add('Profile Updated Successfully');
      });
    } else {
      this.messageService.add('Please fill out the required fields in the form correctly');
    }
  }
  isValid(control){
    return this.form.controls[control].invalid && this.form.controls[control].touched;
  }


  handleBadgeSelection(badge) {
    this.badge = badge;

  }

  onNoClick(): void {

  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }
*/
}
