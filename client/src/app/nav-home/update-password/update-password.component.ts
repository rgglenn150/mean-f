import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.css']
})
export class UpdatePasswordComponent implements OnInit {

  form: FormGroup;
  showForm = false;

  constructor(private formBuilder: FormBuilder) { }

  getBlankForm() {
    return this.formBuilder.group({
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    }, { validator: this.matchingFields('password', 'confirmPassword')})
  }

  ngOnInit() {
    this.form = this.getBlankForm();
  }

  matchingFields(field1, field2) {
    return form => {
      if(form.controls[field1].value !== form.controls[field2].value) {
        return { mismatchedFields: true }
      }
    }
  }

  isValid(control){
    return this.form.controls[control].invalid && this.form.controls[control].touched;
  }

  onSubmit(){

  }

}
