import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VanityLinksComponent } from './vanity-links.component';

describe('VanityLinksComponent', () => {
  let component: VanityLinksComponent;
  let fixture: ComponentFixture<VanityLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VanityLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VanityLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
