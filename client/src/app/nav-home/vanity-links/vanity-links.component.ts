import { Component, OnInit } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormGroupDirective } from '@angular/forms';
import { AuthService } from 'src/app/auth/auth.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ProfanityFilterService } from 'src/app/utils/profanity-filter.service';


@Component({
  selector: 'app-vanity-links',
  templateUrl: './vanity-links.component.html',
  styleUrls: ['./vanity-links.component.css']
})




export class VanityLinksComponent implements OnInit {
  vanityLinkForm: FormGroup;
  private user;
  baseUrl: string = "test";
  vanityLink: string;
  username: string;
  isNamePrivate: boolean;
  isUrlPrivate: boolean;
  isCheckboxChanged: boolean = false;
  showForm = false;

  destroySubject$: Subject<void> = new Subject();
  constructor(private auth: AuthService, private platformLocation: PlatformLocation, private formBuilder: FormBuilder, private authService: AuthService, private profanityFilter: ProfanityFilterService) {
    this.baseUrl = (this.platformLocation as any).location.origin.toString();

    this.user = auth.authenticatedUser;
  }



  getBlankForm() {
    return this.formBuilder.group({
      username: ['', [usernameValid(),usernameMax(),this.isClean()]],
      isNamePrivate: [this.isNamePrivate],
      isUrlPrivate: [this.isNamePrivate]
    })
  }

  ngOnInit() {
    this.vanityLinkForm = this.getBlankForm();


    this.authService.getUsername(this.user._id).subscribe(res => {
      var response: any = res;
      this.username = response.value;
      this.isNamePrivate = response.isNamePrivate;
      this.isUrlPrivate = response.isUrlPrivate;
      this.vanityLink = this.username ? this.baseUrl + '/' + this.username : '';

    }, e => {
      console.log('Unable to get username. Something went wrong', e);
    });
  }

  isValid(control) {
    return this.vanityLinkForm.controls[control].invalid && this.vanityLinkForm.controls[control].touched;
  }

  isClean(){
    return control => {
      return !this.profanityFilter.check(control.value) ? null : { hasProfanity: true }
    }
  
  }


  setUsername(usernameField, event = null) {



    if ((this.vanityLinkForm.valid && usernameField.value !== '') || (this.isCheckboxChanged === true)) {
      if (event === null || (event && event.keyCode == 13)) {
        let usernameInput = this.vanityLinkForm.value.username;
        let isUrlPrivate = this.vanityLinkForm.value.isUrlPrivate !== null ? this.vanityLinkForm.value.isUrlPrivate : this.isUrlPrivate;
        let isNamePrivate = this.vanityLinkForm.value.isNamePrivate !== null ? this.vanityLinkForm.value.isNamePrivate : this.isNamePrivate;
        try {

          let a = this.authService.addUsername(this.user._id, usernameInput, isUrlPrivate, isNamePrivate).pipe(takeUntil(this.destroySubject$)).subscribe(res => {

            if (res) {

              if (usernameInput) {
                this.username = usernameInput;
                this.vanityLink = this.baseUrl + '/' + this.username;
              }

              this.isNamePrivate = isNamePrivate;
              this.isUrlPrivate = isUrlPrivate;
              this.isCheckboxChanged = false;
              this.vanityLinkForm.markAsPristine();
              this.vanityLinkForm.markAsUntouched();
              usernameField.value = '';
            }
          });
        } catch (error) {
          console.log('Setusername error', error);
        }
      }
    }
  }

  onChangeCheckbox(usernameField) {
    this.isCheckboxChanged = true;
    this.setUsername(usernameField);
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }


}

function usernameValid() {
  return control => {
    var regex = /^[A-Za-z0-9]{8,}$|^$/;
    return regex.test(control.value) ? null : { invalidUsername: true }
  }
}



function usernameMax() {
  return control => {
  
    return control.value.length <=50 ? null : { usernameAboveLimit: true }
  }
} 
