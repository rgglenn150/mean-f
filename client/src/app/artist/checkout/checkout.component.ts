import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MessageService} from '../../utils/message.service';
// https://alligator.io/angular/stripe-elements/

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  form: FormGroup;

  constructor( private formBuilder: FormBuilder,
               private messageService: MessageService) { }

  getBlankForm () {
    return this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      creditCard: ['', Validators.required],
      cvc: ['', [Validators.required]],
      address1: ['', [Validators.required]],
      address2: ['', []],
      state: ['', []],
      city: ['', []],
      zip: ['', []],
      saveCard: ['', []]
    });
  }


  // Testing credit card: 4242424242424242 cvc: 123
  buy() {
    console.log(this.form);
    if (this.form.valid) {
      const name = this.form.get('name').value;
      /*this.stripeService
        .createToken(this.card.getCard(), { name })
        .subscribe(result => {
          if (result.token) {
            // Use the token to create a charge or a customer
            // https://stripe.com/docs/charges
            console.log(result.token.id);
          } else if (result.error) {
            // Error creating the token
            console.log(result.error.message);
          }
        });*/
    } else {
      this.messageService.add('Please fill out the required fields in the form correctly');
    }
  }

  ngOnInit() {

    this.form = this.getBlankForm();
  }

}
