import {ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import { environment } from '../../../../environments/environment';

// https://alligator.io/angular/stripe-elements/

//https://stripe.com/docs/stripe-js

//Need to look how elements are created

@Component({
  selector: 'app-checkout-form',
  templateUrl: './checkout-form.component.html',
  styleUrls: ['./checkout-form.component.css']
})
export class CheckoutFormComponent implements OnInit {
  @ViewChild('cardInfo') cardInfo: ElementRef;

  constructor(private cd: ChangeDetectorRef) { }

  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;
  private stripe;
  private elements;

  @Input()
  public parentForm: FormGroup;

  ngOnInit() {
    this.stripe = (window as any).Stripe(environment.stripePublishable);
    this.elements = this.stripe.elements;
    console.log(this.stripe);
    console.log(this.elements);
  }

  isValid(control) {
    return this.parentForm.controls[control].invalid;
  }



  ngAfterViewInit() {
    console.log('after view')

    var style = {
      base: {
        color: '#32325d',
        lineHeight: '18px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    var elements = this.stripe.elements();

    this.card = elements.create('card', {style: style});
    this.card.mount(this.cardInfo.nativeElement);

    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit() {
    const { token, error } = await this.stripe.createToken(this.card);

    if (error) {
      console.log('Something is wrong:', error);
    } else {
      console.log('Success!', token);
      // ...send the token to the your backend to process the charge
    }
  }
}
