import { Injectable } from '@angular/core';
import { HandleError, HttpErrorHandler } from '../http-error-handler.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, filter, takeUntil } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Observable, Subject } from 'rxjs';
import { any } from 'codelyzer/util/function';
import { RequestOptions, ResponseContentType } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class SubAccountService {
  BASE_URL = environment.protocal + environment.host + '/api/user/subaccount';
  private handleError: HandleError;
  destroySubject$: Subject<void> = new Subject();


  constructor(
    private http: HttpClient, private httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('SubAccountService');

  }

  getSubAccounts(userId) {
    return this.http.get<any>(this.BASE_URL + '/',  { params: new HttpParams().set('userId', userId) }).pipe(
      catchError(this.handleError('getSubAccount'))
    );
  }


  addSubAccount(email: string,orgId:string=null) {
  
    return this.http.post(this.BASE_URL + '/', { email: email,orgId:orgId }).pipe(
      catchError(this.handleError('addSubAccount'))
    );
  }

  deleteSubAccount(id:string){
    return this.http.delete<any>(this.BASE_URL + '/'+id).pipe(
      catchError(this.handleError('deleteSubAccount'))
    );
  }

  confirmSubAccount(confirmationToken) {
    return this.http.get<any>(this.BASE_URL + '/confirm/' + confirmationToken).pipe(
      catchError(this.handleError('confirmSubAccount'))
    );
  }



}
