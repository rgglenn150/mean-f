import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubAccountConfirmationComponent } from './sub-account-confirmation.component';

describe('SubAccountConfirmationComponent', () => {
  let component: SubAccountConfirmationComponent;
  let fixture: ComponentFixture<SubAccountConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubAccountConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubAccountConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
