import { Component, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { SubAccountService } from '../sub-account.service';

@Component({
  selector: 'app-sub-account-confirmation',
  templateUrl: './sub-account-confirmation.component.html',
  styleUrls: ['./sub-account-confirmation.component.css']
})
export class SubAccountConfirmationComponent implements OnInit {
  confirmationToken: string = undefined;
  message: string;
  destroySubject$: Subject<void> = new Subject();
  confirmed: boolean;
  constructor(private route: ActivatedRoute, private subAccountService: SubAccountService) {

  }

  ngOnInit() {

    this.route.url.pipe(takeUntil(this.destroySubject$)).subscribe(url => {
      this.confirmationToken = url && url[2] && url[2].path ? url[2].path : '';
    });

    this.confirmSubAccount();

  }


  confirmSubAccount() {

    this.subAccountService.confirmSubAccount(this.confirmationToken)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(res => {
        if (this.confirmationToken && this.confirmationToken !== null &&
          res) {
          this.message = "Success! Sub-account Confirmed.";
          this.confirmed = true;
        } else {
          this.message = "Failed! Something went wrong."
          this.confirmed = false;
        }


      });



  }





}
