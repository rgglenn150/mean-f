import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { catchError, takeUntil } from 'rxjs/operators';
import { identifierModuleUrl } from '@angular/compiler';


@Injectable()
export class AuthService {

  BASE_URL = environment.protocal + environment.host + '/auth';
  BASE_URL_USER = environment.protocal + environment.host + '/api/user';
  FNAME_KEY = 'firstName';
  LNAME_KEY = 'lastName';
  TOKEN_KEY = 'token';
  REGISTER_TOKEN = 'registerToken';
  PROFILE_PHOTO = 'photo';
  USER_ID = 'mail_id_12AB';
  USERNAME = 'username';
  STATUS_KEY = 'status'; 

  private userStore = {};
  private userSubject = new Subject();
  user = this.userSubject.asObservable();

  destroySubject$: Subject<void> = new Subject();

  constructor(private http2: HttpClient, private http: Http, private router: Router, private snackBar: MatSnackBar) {

  }
  isEmpty(obj) {
    return (Object.getOwnPropertyNames(obj).length === 0);
  }
  set registerToken(token) {
    localStorage.setItem(this.REGISTER_TOKEN, token);
  }
  get registerToken() {
    return localStorage.getItem(this.REGISTER_TOKEN);
  }
  get userId() {
    return localStorage.getItem(this.USER_ID);
  }
  set userId(id) {
    localStorage.setItem(this.USER_ID, id);
  }
  public removeRegisterToken() {
    localStorage.removeItem(this.REGISTER_TOKEN);
  }

  get userStatus(){
    return localStorage.getItem(this.STATUS_KEY);
  }

 


  get authenticatedUser() {
    if (!this.isEmpty(this.userStore)) {
      return this.userStore;
    } else if (localStorage.getItem(this.TOKEN_KEY)) {
      this.userStore = {
        token: localStorage.getItem(this.TOKEN_KEY),
        firstName: localStorage.getItem(this.FNAME_KEY),
        lastName: localStorage.getItem(this.LNAME_KEY),
        profilePhoto: localStorage.getItem(this.PROFILE_PHOTO) !== 'undefined' ? localStorage.getItem(this.PROFILE_PHOTO) : false,
        _id: localStorage.getItem(this.USER_ID)
      };

      this.userSubject.next(this.userStore);
      return this.userStore;
    }
  }

  ngOnInit() {
    return this.authenticatedUser;
  }

  public forgotpassword(email) {
    this.http.post(this.BASE_URL_USER + '/resetPassword', { email: email }).pipe(takeUntil(this.destroySubject$)).subscribe(res => {
      this.handleError2('You have been emailed your password reset link, check your email.');
    }, e => {
      this.handleError2('Unable to request password reset. Please contact customer support.');
    });
  }

  public recoverpassword(data) {
    this.http.post(this.BASE_URL_USER + '/recoverpassword', data).pipe(takeUntil(this.destroySubject$)).subscribe(res => {
      this.handleError2('You have successfully reset your password.');
      this.router.navigateByUrl('/login');

    }, e => {
      this.handleError2('Unable to request password reset. Please contact customer support.');
    });
  }

  public handleError2(message) {
    this.snackBar.open(message, 'Close', { duration: 5000 });
  }

  get name() {
    return localStorage.getItem(this.FNAME_KEY) + ' ' + localStorage.getItem(this.LNAME_KEY);
  }

  get isAuthenticated() {
    return !!localStorage.getItem(this.TOKEN_KEY);
  }

  get token() {
    return 'Bearer ' + localStorage.getItem(this.TOKEN_KEY);
  }

  get tokenHeader() {
    const header = new Headers({ 'Authorization': 'Bearer ' + localStorage.getItem(this.TOKEN_KEY) });
    return new RequestOptions({ headers: header });
    /*return {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem(this.TOKEN_KEY)
      })
    };*/
  }

  login(loginData) {
    this.http.post(this.BASE_URL + '/login', loginData).pipe(takeUntil(this.destroySubject$)).subscribe(res => {
      this.authenticate(res);
    }, e => {

      this.handleError2('Unable to login. Wrong username and password or account not active');
    });
  }

  register(user) {
   
    delete user.confirmPassword;
    this.http.post(this.BASE_URL + '/register', user).pipe(takeUntil(this.destroySubject$)).subscribe(res => {
      this.authenticate(res);
      // for message display only
      this.handleError2('A confirmation email was sent to ' + user.email);
    }, e => {
      try {
        if (e._body) {
          const body = JSON.parse(e._body);
          if (body.message) {
            this.handleError2(body.message);
          }
        }
      } catch (e) { }
    }, () => { });
  }


  orgUserRegister(user) {
    
    delete user.confirmPassword;
    delete user.orgName;
    this.http.post(this.BASE_URL + '/orgUserRegister', user).pipe(takeUntil(this.destroySubject$)).subscribe(res => {
      this.authenticate(res);
      // for message display only
      this.handleError2('A confirmation email was sent to ' + user.email);
    }, e => {
      try {
        if (e._body) {
          const body = JSON.parse(e._body);
          if (body.message) {
            this.handleError2(body.message);
          }
        }
      } catch (e) { }
    }, () => { });
  }

  addUsername(_id, username, isUrlPrivate, isNamePrivate) {

    let res = this.http2.post(this.BASE_URL_USER + '/username', { username, isUrlPrivate, isNamePrivate }).pipe(takeUntil(this.destroySubject$));
    res.subscribe(res => {

      if (!username) {
        this.handleError2('Successfully changed privacy.');
      }
      else {
        this.handleError2('Successfully changed username.');
      }
      this.authenticate(res);
    }, e => {
      this.handleError2('Unable to add username. ' + JSON.parse(e._body).message);
    });
    return res;
  }


  getUsername(userId) {

    return this.http2.get(this.BASE_URL_USER + '/username', { params: new HttpParams().set('userId', userId) }).pipe(takeUntil(this.destroySubject$));
  }

  getFCMToken(userId){
    return this.http2.get(this.BASE_URL_USER + '/fcm', { params: new HttpParams().set('_id', userId) }).pipe(takeUntil(this.destroySubject$));
  }

  getUserByUsername(username: string) {

    return this.http2.get(this.BASE_URL_USER + '/profile/' + username, { params: new HttpParams().set('userId', this.userId) }).pipe(takeUntil(this.destroySubject$));
  }

  updateName(firstName: string, lastName: string) {
    return this.http2.put(this.BASE_URL_USER + '/me', { firstName, lastName }).pipe(takeUntil(this.destroySubject$));
  }


  updateFCMToken(token:string){
    
     this.http2.put(this.BASE_URL_USER + '/fcm', { fcmToken:token }).pipe(takeUntil(this.destroySubject$)).pipe(takeUntil(this.destroySubject$)).subscribe(data=>{
     
       
     },err=>{
       console.log('ERROR',err)
     });
  }



  goHomeIfAuthenticated() {
    if (this.isAuthenticated) {
      if(this.userStatus==="pending"){
    
        this.router.navigateByUrl('/account/pending');
      }else{
        this.router.navigateByUrl('/me');
      }
     
    }
  }

  logout() {
    localStorage.removeItem(this.FNAME_KEY);
    localStorage.removeItem(this.LNAME_KEY);
    localStorage.removeItem(this.TOKEN_KEY);
    localStorage.removeItem(this.PROFILE_PHOTO);
    localStorage.removeItem(this.REGISTER_TOKEN);
    // this.userSubject.next({});
    this.router.navigateByUrl('/login');
  }



  authenticate(res) {
    const authResponse = (typeof res.json !== 'undefined') ? res.json() : res;
   
    if (!authResponse.token) { return; }
    this.removeRegisterToken();

    localStorage.setItem(this.TOKEN_KEY, authResponse.token);
    localStorage.setItem(this.FNAME_KEY, authResponse.firstName);
    localStorage.setItem(this.LNAME_KEY, authResponse.lastName);
    localStorage.setItem(this.PROFILE_PHOTO, authResponse.profilePhoto);
    localStorage.setItem(this.USER_ID, authResponse._id);
    localStorage.setItem(this.STATUS_KEY, authResponse.status);
    this.userStore = {
      _id: authResponse._id,
      token: authResponse.token,
      firstName: authResponse.firstName,
      lastName: authResponse.lastName,
      profilePhoto: authResponse.profilePhoto,
      status: authResponse.status
    };

    this.userSubject.next(this.userStore);

    if (authResponse.status === "active") {
      this.router.navigate(['/']);
    }else if(authResponse.status==="pending"){
      this.router.navigate(['/account/pending']);
    }


  }

  loginFacebook(userData) {
    userData.profilePhoto = userData.photoUrl;
    delete userData.photoUrl;
    this.http.post(this.BASE_URL + '/facebook', userData).pipe(takeUntil(this.destroySubject$)).subscribe(res => {
      this.authenticate(res);
    }, e => {
      this.handleError2('Unable to login. Wrong username and password');
    });
  }

  /*

  getOrders(symbol: string): Observable<Order[]> {
  symbol = symbol.trim();

  // Add safe, URL encoded search parameter if there is a search term
  const options = symbol ?
    {params: new HttpParams().set('symbol', symbol).set('filter', '{"open":"true"}')} : {};

  return this.http.get<Order[]>(this.BASE_URL, options)
    .pipe(
      catchError(this.handleError<Order[]>('getOrders', []))
    );
}

   */

  /*generateConsent(): Observable<object> {
    return this.httpClient.get<object>(this.BASE_URL, {})
      .pipe(
        catchError(this.handleError<object>('generateConsent', {}))
      );
  }*/


  confirmAccount(confirmationToken) {

    let confirmation=  this.http2.get<any>(this.BASE_URL + '/confirm/' + confirmationToken).pipe(
      takeUntil(this.destroySubject$)
    );
      confirmation.subscribe(res => {
       
        localStorage.setItem(this.STATUS_KEY, res.status);
      }, e => {
        this.handleError2('Unable to login. Wrong username and password');
      });;

    return confirmation ;

  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }
}
