import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../environments/environment';
import {catchError} from 'rxjs/operators';
import {HandleError, HttpErrorHandler} from '../http-error-handler.service';
import {AuthService} from './auth.service';


@Injectable()
export class AuthSocialService {

  BASE_URL = environment.protocal + environment.host + '/auth';

  private handleError: HandleError;

  constructor(private authService: AuthService, private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('AuthSocialService');
  }

  generateConsent(): Observable<object> {
    return this.http.get<object>(this.BASE_URL + '/linkedin/generateConsentUrl', {})
      .pipe(
        catchError(this.handleError<object>('generateConsent', {}))
      );
  }

  loginLinkedin(code, state, token = null) {

    //const options = new HttpParams().set('code', code).set('state', state).set('token', token);

    return this.http.post(this.BASE_URL + '/linkedin', {code: code, state: state, token: token}).pipe(
      catchError(this.handleError<object>('Could not login with Linkedin', {}))
    );
  }

  /*

  getOrders(symbol: string): Observable<Order[]> {
  symbol = symbol.trim();

  // Add safe, URL encoded search parameter if there is a search term
  const options = symbol ?
    {params: new HttpParams().set('symbol', symbol).set('filter', '{"open":"true"}')} : {};

  return this.http.get<Order[]>(this.BASE_URL, options)
    .pipe(
      catchError(this.handleError<Order[]>('getOrders', []))
    );
}

   */

  /*generateConsent(): Observable<object> {
    return this.httpClient.get<object>(this.BASE_URL, {})
      .pipe(
        catchError(this.handleError<object>('generateConsent', {}))
      );
  }*/
}
