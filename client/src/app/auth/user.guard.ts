import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Route, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {

  constructor(private authService:AuthService,private router:Router){

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let url:string = state.url;  
    return this.checkStatus(url);
  }

 
  checkStatus(url: string){
    
    if(this.authService.userStatus==="pending"){
      this.router.navigate(['/account/pending']);
      return false;
    }
    else{
      return true;
    }
    
  }


}
