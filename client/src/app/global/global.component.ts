import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import {BadgeService} from '../badge/badge.service';
import {Badge} from '../badge/badge';

@Component({
  selector: 'global',
  templateUrl: './global.component.html',
  styleUrls: ['./global.component.css']
})
export class GlobalComponent {
  constructor(private auth: AuthService, private badgeService: BadgeService) {}

  badges: Badge[];

  ngOnInit() {
    //this.badgeService.getBadgesDefault().subscribe( badges => this.badgesDefault = badges);
  }
}
