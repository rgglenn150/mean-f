import {Component, Input, OnInit} from '@angular/core';

/* how to use:

<nav-breadcrumb link="/events">Daft Event</nav-breadcrumb>

 */


@Component({
  selector: 'nav-breadcrumb',
  templateUrl: './nav-breadcrumb.component.html',
  styleUrls: ['./nav-breadcrumb.component.css']
})
export class NavBreadcrumbComponent implements OnInit {

  @Input() link = '/';

  constructor() { }

  ngOnInit() {
  }

}
