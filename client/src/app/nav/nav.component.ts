import { Component } from '@angular/core'
import { AuthService } from '../auth/auth.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
    selector: 'nav',
    templateUrl: 'nav.component.html',
    styleUrls: ['./nav.component.css']
})
export class NavComponent {
  public user;
  public event;

  destroySubject$: Subject<void> = new Subject();

  constructor(public auth: AuthService, private router: Router) {
    this.user = auth.authenticatedUser;
  }
  ngOnInit() {

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd ) {
        this.event = event.url ? event.url.indexOf('events') !== -1 : false;
      }
    });

    this.auth.user.pipe(takeUntil(this.destroySubject$)).subscribe(data => {
      this.user = data;
    });
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }
}
