import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { FriendService } from '../friend/friend.service';
import { Friend } from '../friend/friend';
import { Badge } from '../badge/badge';
import { BadgeService } from '../badge/badge.service';
import { BadgeUser } from '../badge/badge-user';
import { MessageService } from '../utils/message.service';
import { DialogService } from '../utils/dialog-service/dialog.service';
import { takeUntil } from 'rxjs/operators';
import { forkJoin, Subject } from 'rxjs';
import { NotificationService } from '../notification/notification.service';

@Component({
  selector: 'give',
  templateUrl: 'give.component.html',
  styleUrls: ['./give.component.css']
})
export class GiveComponent {
  constructor(private auth: AuthService, private friendService: FriendService, private badgeService: BadgeService,
    private messageService: MessageService, private dialogService: DialogService,
    private notificationService: NotificationService) { }

  placeholderFriends = [];
  friends: Friend[];
  pendingFriends: Friend[];
  public selectedFriends: Array<any> = [];
  public badge: Badge = undefined;
  user;

  destroySubject$: Subject<void> = new Subject();

  ngOnInit() {
    this.user = this.auth.authenticatedUser;
  }

  selectFriend(friends) {
    this.selectedFriends = friends;
  }

  handleBadgeSelection(badge) {
    this.badge = badge;
  }
  onSubmit() {
    if (!this.badge) {
      this.messageService.add('You must first pick a badge.');
    } else if (this.selectedFriends.length < 1) {
      this.messageService.add('Select one of your friends/Co-Workers/Employees to award a badge');
    } else {

      const subscriptions = this.selectedFriends.map((friendId) => {
        // @ts-ignore
        const newBadgeUser: BadgeUser = {
          badge: this.badge._id,
          user: friendId
        };

       
        this.notificationService.sendNotif(friendId, 'You Received a new Badge from ' + this.user.firstName + ' ' + this.user.lastName, '/me', this.badge.image);

        return this.badgeService.awardBadge(newBadgeUser).pipe(takeUntil(this.destroySubject$));
      });
      forkJoin(subscriptions).subscribe((result) => {
        this.selectedFriends = [];
        this.badge = undefined;

        this.messageService.add('Successfully gave badge.');
      });

    }

  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }
}
