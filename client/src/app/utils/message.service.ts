import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material';

@Injectable()
export class MessageService {
  messages: string[] = [];

  constructor (private snackBar: MatSnackBar) { }

  add(message: string) {
    this.messages.push(message);
    this.snackBar.open(message, 'Close', { duration: 5000 });
  }

  clear() {
    this.messages = [];
  }
}
