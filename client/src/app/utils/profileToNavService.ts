import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class ProfileToNavService {
    private dataObs$ = new Subject();

    getData() {
        return this.dataObs$;
    }

    updateData(data:any) {
        this.dataObs$.next(data);
    }
}