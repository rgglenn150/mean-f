import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

/* to use:

<app-button (onClick)="addCustom()" icon="icon-CreateEventIcon">Add Custom</app-button>

 */

interface IconSize {
  width: number;
  height: number;
}

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {


  @Input() icon?: string;
  @Input() size?: string;
  @Input() selected?: Boolean;
  @Input() iconSize?: IconSize;
  @Input() type?: string;

  @Output() onClick = new EventEmitter<any>();

  constructor() { }

  onClickbutton(event) {
    this.onClick.emit(event);
  }

  ngOnInit() {
    this.size = this.size ? this.size : 'large';
    this.iconSize = this.iconSize ? this.iconSize : { width: 17, height: 17 };
    this.type = this.type ? this.type : 'submit';
  }

}
