import { TestBed } from '@angular/core/testing';

import { ProfanityFilterService } from './profanity-filter.service';

describe('ProfanityFilterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProfanityFilterService = TestBed.get(ProfanityFilterService);
    expect(service).toBeTruthy();
  });
});
