import {Component, Inject, inject, Injectable} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {Observable} from 'rxjs';

export interface DialogData {
  message: string;
  button: string;
}

@Injectable()
export class DialogService {
  constructor(public dialog: MatDialog) {}

  openDialog(message, button): Observable<any> {
    const dialogRef = this.dialog.open(DialogServiceComponent, {
      data: { message: message,  button: button }
    });

    return dialogRef.afterClosed();
    /*
    .subscribe(result => {
      console.log(`Dialog result: ${result}`);
    }); */
  }
}

@Component({
  selector: 'dialog-content-example-dialog',
  templateUrl: 'dialog-content-example-dialog.html',
})
export class DialogServiceComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogServiceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}
}

/*import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Observable} from 'rxjs';

export interface DialogData {
  message: string;
  button: string;
}

@Inject
export class DialogService {

  name: string;

  constructor(public dialog: MatDialog) {}

  confirm(message, button): Observable<any> {
    const dialogRef = this.dialog.open(DialogServiceComponent, {
      width: 'auto',
      data: {message: message, button: button}
    });

    // simply subscribe to get the result
    // .subscribe(result => {
    //  return result;
    //});
    return dialogRef.afterClosed();
  }

}

@Component({
  selector: 'dialog-service',
  template: `
    <div mat-dialog-content>
      {{data.message}}
    </div>
    <div mat-dialog-actions>
      <button mat-button (click)="onNoClick()">No Thanks</button>
      <button mat-button [mat-dialog-close]="true" cdkFocusInitial>{{data.button}}</button>
    </div>
  `
})
export class DialogServiceComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogServiceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onConfirm() {
    this.dialogRef.close(true);
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

}
*/
