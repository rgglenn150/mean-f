import { Component } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'login',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.css']
})
export class RecoverPasswordComponent {
  public form;
  destroySubject$: Subject<void> = new Subject();

  constructor(
    private auth: AuthService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder) {
    auth.goHomeIfAuthenticated();

    this.form = formBuilder.group({
      newPassword: ['', Validators.required],
      retypePassword: ['', Validators.required],
    }, { validator: matchingFields('newPassword', 'retypePassword')});

  }

  private resetToken;

  onSubmit() {
    if (this.form.valid) {
      this.form.value.resetToken = this.resetToken;
      this.auth.recoverpassword(this.form.value);
    } else {
      this.auth.handleError2('Please fill out the form correctly and completely');
    }
  }

  ngOnInit() {
    this.route.url.pipe(takeUntil(this.destroySubject$)).subscribe(url => {
      this.resetToken = url && url[1] && url[1].path ? url[1].path : '';
    });
  }

  isValid(control) {
    return this.form.controls[control].invalid && this.form.controls[control].touched;
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }

}

function matchingFields(field1, field2) {
  return form => {
    if(form.controls[field1].value !== form.controls[field2].value)
      return { mismatchedFields: true }
  }
}

function emailValid() {
  return control => {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(control.value) ? null  : { invalidEmail: true }
  };
}
