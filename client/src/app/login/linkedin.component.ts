import { Component } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthSocialService} from '../auth/auth.social.service';
import {AuthService} from '../auth/auth.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'linkedinComponent',
  template: `<mat-card>Loading ...</mat-card>`
})
export class LinkedinComponent {

  destroySubject$: Subject<void> = new Subject();

  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private authSocialService: AuthSocialService,
    private router: Router) { }


  ngOnInit() {
    this.activatedRoute.queryParams.pipe(takeUntil(this.destroySubject$)).subscribe(params => {
      if (params['code'] && params['state']) {
        this.authSocialService.loginLinkedin(params['code'], params['state'], this.authService.registerToken).subscribe(res => {
          this.authService.authenticate(res);
        });
      } else {
        this.router.navigateByUrl('/login');
      }
    });
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }
}
