import { Component } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'login',
  templateUrl: 'forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {
  constructor(private auth: AuthService, private route: ActivatedRoute, private formBuilder: FormBuilder) {
    auth.goHomeIfAuthenticated();
  }

  public email = '';

  forgotpassword() {
    this.auth.forgotpassword(this.email);
    this.email = '';
  }

}
