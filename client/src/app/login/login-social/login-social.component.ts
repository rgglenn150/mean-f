import { Component } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { AuthService as SocialAuthService } from 'angularx-social-login';
import { FacebookLoginProvider, LinkedInLoginProvider } from 'angularx-social-login';
import { environment } from '../../../environments/environment';
import {AuthSocialService} from '../../auth/auth.social.service';
import {ActivatedRoute} from '@angular/router';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'login-social',
  template: `
    <img style="width:250px; cursor:pointer;" src="assets/img/facebook.png" (click)='facebookLogin()' />
    <br />
    <br />
    <img style="width:250px; cursor:pointer;" src="assets/img/linkedin_default.png" (click)='linkedinLogin()' />
  `
})
export class LoginSocialComponent {

  BASE_URL = environment.protocal + environment.host + '/linkedin';
  client_id: String = environment.linkedinClientId;
  redirect_uri: String = encodeURI(this.BASE_URL);

  destroySubject$: Subject<void> = new Subject();


  constructor(
    private authSocialService: AuthSocialService,
    private socialAuthService: SocialAuthService,
    private authService: AuthService,
    private route: ActivatedRoute) {}

  token = undefined;

  ngOnInit(){

    (window as any).fbAsyncInit = function() {
      // @ts-ignore
      FB.init({
        appId      : environment.facebookAppId,
        cookie     : true,
        xfbml      : true,
        version    : 'v3.2'
      });
      // @ts-ignore
      FB.AppEvents.logPageView();
    };

    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    this.route.url.pipe(takeUntil(this.destroySubject$)).subscribe(url => {
      this.token = url && url[1] && url[1].path ? url[1].path : '';
    });

  }


  private sendToRestApiMethod(userData): void {
    this.authService.loginFacebook(userData);
  }

  public facebookLogin() {
    const socialPlatformProvider = environment.facebookAppId;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        if (this.token) {
          // @ts-ignore
          userData.token = this.token;
        }
        this.sendToRestApiMethod(userData);
      }
    );
  }

  public linkedinLogin() {
    this.authSocialService.generateConsent().pipe(takeUntil(this.destroySubject$)).subscribe(data => {
      // @ts-ignore
      if(data && data.url) {
        // @ts-ignore
        (window as any).location.replace(data.url);
      }
    });


    //(window as any).open(url);
    //this.socialAuthService.signIn(environment.linkedinClientId);
    /*.then(
      (userData) => {
        this.sendToRestApiMethod(userData);
      }
    );*/
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }


}
