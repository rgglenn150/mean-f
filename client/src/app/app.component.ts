import { Component } from '@angular/core';
import { MessagingService } from './shared/messaging.service';

@Component({
  selector: 'app-root',
  template: `
  <svg-map></svg-map>
  <nav></nav>
  <router-outlet></router-outlet>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  

  constructor() { }

  ngOnInit() {
    
  }
}
