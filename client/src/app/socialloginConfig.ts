import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { FacebookLoginProvider, LinkedInLoginProvider} from 'angularx-social-login';
import { environment } from '../environments/environment';

export function getAuthServiceConfigs()
{
  const config = new AuthServiceConfig([
    {
      id: environment.facebookAppId,
      provider: new FacebookLoginProvider('Facebook-App-Id')
    }
    /*, {
      id: environment.linkedinClientId,
      provider: new LinkedInLoginProvider('LinkedIn-client-Id', false, 'en_US')
    }*/
  ]);
  return config;
}
