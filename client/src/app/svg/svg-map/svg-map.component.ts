import { Component, OnInit } from '@angular/core';

/*
Maps are generated with moon icon
// https://icomoon.io/
 */

@Component({
  selector: 'svg-map',
  templateUrl: './svg-map.component.html',
  styleUrls: ['./svg-map.component.css']
})
export class SvgMapComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
