import {Component, Input, OnInit} from '@angular/core';

/* Examples:

Maps are generated with moon icon

// https://icomoon.io/

<svg-icon width="25" height="25" icon="icon-search"></svg-icon>

<svg-icon width="20" height="20" icon="icon-website"></svg-icon>

 */

/*

icons for @Input() icon

icon-dayIcon
icon-AddFriend
icon-Badge2
icon-BitCoin
icon-Comment
icon-CreateEventIcon
icon-CustomBadgeRequest
icon-DolarsSign
icon-EditIcon
icon-EmployeeIconTransparent
icon-Friends-512
icon-HeartIcon
icon-HeartIconActivated
icon-Linkedin_circle_black-512
icon-oneweekIcon
icon-PayPalIcon2
icon-PictureIcon
icon-RemoveIcon
icon-ShareIcon
icon-StarIcon
icon-starIconActivacted
icon-uploadBadge
icon-website
icon-search
icon-Recurence
icon-notifications
icon-analytics
icon-done - checkbox for done
icon-pencil - edit pencil
icon-left-arrow - left arrow
icon-duplicate - duplicate button on events for badges
icon-trophy - podium for trophy
icon-participation - participation badge icon
 */

/*

Create Event Icon: <svg-icon width="20" height="20" icon="icon-CreateEventIcon"></svg-icon>
Upload Badge Icon: <svg-icon width="20" height="20" icon="icon-uploadBadge"></svg-icon>
Search Icon <svg-icon width="20" height="20" icon="icon-search"></svg-icon>
Notification Icon <svg-icon width="20" height="20" icon="icon-search"></svg-icon>
Share Icon <svg-icon width="20" height="20" icon="icon-ShareIcon"></svg-icon>
Comment Icon <svg-icon width="20" height="20" icon="icon-Comment"></svg-icon>
Heart Icon <svg-icon width="20" height="20" icon="icon-HeartIcon"></svg-icon>
Notifications <svg-icon width="20" height="20" icon="icon-notifications"></svg-icon>
Analytics <svg-icon width="20" height="20" icon="icon-analytics"></svg-icon>
1 Day icon <svg-icon width="23" height="27" icon="icon-dayIcon"></svg-icon>
 */


@Component({
  selector: 'svg-icon',
  templateUrl: './svg-icon.component.html',
  styleUrls: ['./svg-icon.component.css']
})
export class SvgIconComponent {
  @Input() icon = 'icon-search';
  @Input() width = 25;
  @Input() height ? = 25;
  @Input() fill ? = 'black';
}
