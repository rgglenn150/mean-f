import { Component } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import {CampaignService} from '../campaign-service/campaign.service';
import {Campaign} from '../campaign';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-campaign-home',
  templateUrl: './campaign-home.component.html',
  styleUrls: ['./campaign-home.component.css']
})
export class CampaignHomeComponent {
  constructor(private auth: AuthService, private campaignService: CampaignService) {}

  campaigns: Observable<Campaign[]>;

  ngOnInit() {
    // this.badgesDefault$ = this.badgeService.getBadgesDefault();
  }
}
