export interface Campaign {
  _id?: string;
  title: string;
  description: string;
  company: string;
  user: string;
  frequency: string;
  rulesets: string;
  created: Date;
  expire: Date;
}
