import { Component } from '@angular/core';
import {AuthService} from './auth/auth.service';

@Component({
  selector: 'not-authenticated',
  template: `
    <mat-card *ngIf="!auth.isAuthenticated" class="card">
      <mat-card-content>You must login to use this</mat-card-content>
    </mat-card>
    `,
  styles: [``]
})
export class NotAuthenticatedComponent {
  constructor(public auth: AuthService) { }
}
