export interface Notification {
  _id: string;
  message: string;
  read: Boolean;
  path: string;
}
