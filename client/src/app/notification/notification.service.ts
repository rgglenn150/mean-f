import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { AuthService } from '../auth/auth.service';
import { HandleError, HttpErrorHandler } from '../http-error-handler.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { catchError, takeUntil } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Observable, Subject } from 'rxjs';
import { Notification } from './notification';
import { RequestOptions, Headers, Http } from '@angular/http';
import { MessagingService } from '../shared/messaging.service';

@Injectable()
export class NotificationService {
  BASE_URL = environment.protocal + environment.host + '/api/notification';
  private handleError: HandleError;
  userToken;

  constructor(
    private http: HttpClient, private snackBar: MatSnackBar, private auth: AuthService, private httpErrorHandler: HttpErrorHandler, private http2: Http, private messagingService: MessagingService) {
    this.handleError = httpErrorHandler.createHandleError('BadgeService');
  }

  getNotifications(): Observable<Notification[]> {
    return this.http.get<Notification[]>(this.BASE_URL + '/')
      .pipe(
        catchError(this.handleError('getNotifications', []))
      );
  }



  sendNotif(userId: string, body: string, path = "", thumbnail = "") {

    this.auth.getFCMToken(userId).subscribe(token => {
      let notif = {
        "notification": {
          "title": 'Badges Notifications',
          "body": body
        },
        "to": token
      };

      this.saveNotif(userId, body, path, thumbnail);
      let headers = new Headers({ 'Authorization': 'key=' + environment.fcmServerKey });
      let options = new RequestOptions({ headers });


      this.http2.post('https://fcm.googleapis.com/fcm/send', notif, options).pipe(
        catchError(this.handleError('sendNotif', []))).subscribe(data => {

        });


    });
  }

  private saveNotif(user, message, path, thumbnail) {
    let notif = {
      user,
      message,
      path,
      thumbnail
    };

    this.http.post(this.BASE_URL + '/', notif).pipe(
      catchError(this.handleError('saveNotif', []))).subscribe(data => {

      });
  }

  updateReadNotif(notifId) {
    let notif = {
      _id: notifId
    };
    this.http.put(this.BASE_URL + '/view', notif).pipe(
      catchError(this.handleError('saveNotif', []))).subscribe(data => {

      });
  }

  getNotificationsExample(): Observable<any> {
    const notificationStore: Notification[] = [];
    const notificationSubject = new Subject();
    const notifications = notificationSubject.asObservable();
    const notification = {
      _id: '010101',
      message: 'You have got notifications on the line!',
      path: '/me',
      read: false
    };

    notificationStore.push(notification);
    notificationSubject.next(notificationStore);
    return notifications;
  }


}
