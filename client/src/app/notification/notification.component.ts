import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { NotificationService } from './notification.service';
import { Notification } from './notification';
import { MessagingService } from '../shared/messaging.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

/*

In order to use this component you must catch and handle the event by the following

<badge [badgeType]="'user'" (valueChange)='handleThisNewFunction($event)'></badge>

 */

@Component({
  selector: 'notification',
  templateUrl: 'notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent {

  message;
  destroySubject$: Subject<void> = new Subject();
  user: any;
  constructor(private auth: AuthService, private notificationService: NotificationService, private messagingService: MessagingService, private http: Http, private router: Router) {
    this.user = this.auth.authenticatedUser;
  }

  public notifications: Notification[] = [];

  @Output() valueChange = new EventEmitter();

  ngOnInit() {

    //firebase notifs
    const userId = this.user._id;
    this.messagingService.requestPermission(userId)
    this.messagingService.receiveMessage();
    this.messagingService.currentMessage.pipe(takeUntil(this.destroySubject$)).subscribe(data => {
      this.getAllNotifications();
    });
    //this.message = this.messagingService.currentMessage
  }

  getAllNotifications() {
    this.notifications = [];
    this.notificationService.getNotifications().pipe(takeUntil(this.destroySubject$)).subscribe(data => {
      this.notifications.push(...data);
    });
  }

  useBadge(badge) {
    this.valueChange.emit(badge);
  }

  viewNotification(path, notifId) {
    this.notificationService.updateReadNotif(notifId);

    this.router.navigate([path]);
    this.getAllNotifications()

  }


  ngOnDestroy() {
    this.destroySubject$.next();
  }


}
