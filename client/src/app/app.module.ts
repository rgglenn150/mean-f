import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatTableModule,
  MatStepperModule,
  MatButtonToggleModule,
  MatButtonModule,
  MatSelectModule,
  MatCheckboxModule,
  MatCardModule,
  MatInputModule,
  MatSnackBarModule,
  MatToolbarModule,
  MatTabsModule,
  MatMenuModule,
  MatIconModule,
  MatSliderModule,
  MatDialogModule,
  MatListModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatAutocompleteModule,
  MatFormFieldModule
} from '@angular/material';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavComponent } from './nav/nav.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { AuthService } from './auth/auth.service';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user.component';
import { PageNotFoundComponent } from './page-not-found.component';
import { NotAuthenticatedComponent } from './not-authenticated.component';
import { RoundPipe } from './utils/round.pipe';
import { httpInterceptorProviders } from './http-interceptors';
import { HttpClientModule } from '@angular/common/http';
import { HttpErrorHandler } from './http-error-handler.service';
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { getAuthServiceConfigs } from './socialloginConfig';
import { LoginSocialComponent } from './login/login-social/login-social.component';
import { LinkedinComponent } from './login/linkedin.component';
import { AuthSocialService } from './auth/auth.social.service';
import { NavHomeComponent } from './nav-home/nav-home.component';
import { BadgeComponent } from './badge/badge/badge.component';
import { BadgeCreateComponent } from './badge/badge-create/badge-create/badge-create.component';
import { BadgeSelectComponent } from './badge/badge-select/badge-select.component';
import { BadgeService } from './badge/badge.service';
import { routes } from './app.routes';
import { ForgotPasswordComponent } from './login/forgot-password/forgot-password.component';
import { RecoverPasswordComponent } from './login/recover-password/recover-password.component';
import { GiveComponent } from './give/give.component';
import { FriendComponent } from './friend/friend.component';
import { RecentComponent } from './recent/recent.component';
import { GlobalComponent } from './global/global.component';
import { FriendService } from './friend/friend.service';
import { BadgeChooseComponent, BadgeChooseDialogComponent } from './badge/badge-choose/badge-choose.component';
import { FilterPipe } from './utils/filter.pipe';
import { BadgeUploadComponent, BadgeUploadDialogComponent } from './badge/badge-upload/badge-upload.component';
import { BadgeUploadService } from './badge/badge-upload/badge-upload.service';
import { MessageService } from './utils/message.service';
import { FriendInviteComponent, FriendInviteDialogComponent } from './friend/friend-invite/friend-invite.component';
import { NotificationComponent } from './notification/notification.component';
import { NotificationService } from './notification/notification.service';
import { DialogService, DialogServiceComponent } from './utils/dialog-service/dialog.service';
import { FriendSelectComponent } from './friend/friend-select/friend-select.component';
import { AngularDraggableModule } from 'angular2-draggable';
import { FriendProfileComponent } from './friend/friend-profile/friend-profile.component';
import { BadgeTextareaComponent } from './badge/badge-designer/badge-textarea/badge-textarea.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { BadgeImageComponent } from './badge/badge-designer/badge-image/badge-image.component';
import { ArtistComponent } from './artist/artist/artist.component';
import { ArtistRequestComponent } from './artist/artist-request/artist-request.component';
import { CheckoutComponent } from './artist/checkout/checkout.component';
import { BadgePickerComponent } from './badge/badge-designer/badge-picker/badge-picker.component';
import { SanitizeHtmlPipe } from './utils/sanitizeHtml';
import { BadgePreviewService, BadgePreviewDialogComponent } from './badge/badge-create/badge-preview/badge-preview.service';
import { CampaignComponent } from './campaign/campaign/campaign.component';
import { CampaignHomeComponent } from './campaign/campaign-home/campaign-home.component';
import { EventComponent } from './event/event/event.component';
import { EventHomeComponent } from './event/event-home/event-home.component';
import { EventService } from './event/event-service/event-service.service';
import { SvgIconComponent } from './svg/svg-icon/svg-icon.component';
import { SvgMapComponent } from './svg/svg-map/svg-map.component';
import { EventEditComponent } from './event/event-edit/event-edit.component';
import { BadgeDesignerComponent } from './badge/badge-designer/badge-designer/badge-designer.component';
import { BadgeSvgComponent } from './badge/badge-designer/badge-svg/badge-svg.component';
import { TimeFrameComponent } from './event/event-edit/time-frame/time-frame.component';
import { RecurrenceComponent } from './event/event-edit/recurrence/recurrence.component';
import { EventCategoryComponent } from './event/event-edit/event-category/event-category.component';
import { BadgeFormAddComponent } from './badge/badge-form-add/badge-form-add.component';
import { BadgeFormComponent } from './badge/badge-form/badge-form.component';
import { ButtonComponent } from './utils/button/button.component';
import { EventViewComponent } from './event/event-view/event-view.component';
import { MomentModule } from 'angular2-moment';
import { EventAddBadgesComponent } from './event/event-edit/event-add-badges/event-add-badges.component';
import { NavBreadcrumbComponent } from './nav/nav-breadcrumb/nav-breadcrumb.component';
import { CommentsComponent } from './comments/comments.component';
import { BadgesComponent } from './badge/badges/badges.component';
import { FriendsComponent } from './friend/friends/friends.component';
import { ArrowComponent } from './utils/arrow/arrow.component';
import { BackButtonComponent } from './utils/back-button/back-button.component';
import { EventFriendsComponent } from './event/event-friends/event-friends.component';
import { EventAcceptComponent } from './event/event-accept/event-accept.component';
import { CheckoutFormComponent } from './artist/checkout/checkout-form/checkout-form.component';
import { ProfileComponent } from './profile/profile/profile.component';
import { ProfileEditComponent } from './profile/profile-edit/profile-edit.component';
import { ProfileDropdownComponent } from './nav-home/profile-dropdown/profile-dropdown.component';
import { UpdatePasswordComponent } from './nav-home/update-password/update-password.component';
import { SubAccountComponent } from './sub-account/sub-account.component';
import { SubAccountConfirmationComponent } from './sub-account/sub-account-confirmation/sub-account-confirmation.component';
import { SubAccountService } from './sub-account/sub-account.service';
import { VanityLinksComponent } from './nav-home/vanity-links/vanity-links.component';
import { PublicProfileComponent } from './profile/public-profile/public-profile.component';
import { ProfileToNavService } from './utils/profileToNavService';
import { UpdateNameComponent } from './nav-home/update-name/update-name.component';
import { NoAuthProfileComponent } from './profile/no-auth-profile/no-auth-profile.component';
import { ConfirmRegistrationComponent } from './register/confirm-registration/confirm-registration.component';
import { CompanyFieldComponent } from './register/company-field/company-field.component';
import { CompanyRegistrationComponent } from './register/company-registration/company-registration.component';
import { AccountPendingComponent } from './profile/account-pending/account-pending.component';
import { OrganizationComponent } from './organization/organization.component';
import { CompanyUserComponent } from './register/company-user/company-user.component';
import { OrganizationProfileComponent } from './organization/organization-profile/organization-profile.component';
import { RECAPTCHA_SETTINGS, RecaptchaSettings, RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { JwSocialButtonsModule } from 'jw-angular-social-buttons';
import { PublicEventComponent } from './event/public-event/public-event.component';
import { MessagingService } from './shared/messaging.service';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AsyncPipe } from '@angular/common';
import { ProfileUploadComponent } from './nav-home/profile-upload/profile-upload.component';
import { NgxCroppieModule } from 'ngx-croppie';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ReceivedBadgeDialogComponent } from './badge/received-badge-dialog/received-badge-dialog.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    BrowserAnimationsModule,
    MatButtonModule, MatCheckboxModule, MatCardModule, MatInputModule, MatToolbarModule, MatSnackBarModule, MatTabsModule, MatMenuModule,
    MatSliderModule, MatButtonToggleModule, MatTableModule, MatDialogModule, MatSelectModule, MatStepperModule, MatListModule, MatDatepickerModule, MatNativeDateModule, MatCheckboxModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    SocialLoginModule,
    AngularDraggableModule,
    ColorPickerModule,
    MomentModule,
    RouterModule.forRoot(routes),
    RecaptchaModule,
    NgxCroppieModule,
    RecaptchaFormsModule,
    JwSocialButtonsModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
  ],
  declarations: [
    AppComponent,
    NavComponent, HomeComponent, RegisterComponent, LoginComponent, UserComponent,
    PageNotFoundComponent,
    LinkedinComponent,
    NotAuthenticatedComponent,
    NavHomeComponent,
    RoundPipe, FilterPipe, SanitizeHtmlPipe,
    LoginSocialComponent,
    BadgeComponent,
    BadgeCreateComponent,
    BadgeSelectComponent,
    ForgotPasswordComponent,
    RecoverPasswordComponent,
    GiveComponent, GlobalComponent, FriendComponent, RecentComponent, FriendInviteComponent, FriendInviteDialogComponent,
    BadgeChooseComponent, BadgeChooseDialogComponent, BadgeUploadComponent, BadgeUploadDialogComponent,
    NotificationComponent, DialogServiceComponent, FriendSelectComponent, FriendProfileComponent, BadgeTextareaComponent,
    BadgeImageComponent, ArtistComponent, ArtistRequestComponent, CheckoutComponent, BadgePickerComponent,
    BadgePreviewDialogComponent,
    CampaignComponent,
    CampaignHomeComponent,
    EventComponent,
    EventHomeComponent,
    SvgIconComponent,
    SvgMapComponent,
    EventEditComponent,
    BadgeDesignerComponent,
    BadgeSvgComponent,
    TimeFrameComponent,
    RecurrenceComponent,
    EventCategoryComponent,
    BadgeFormAddComponent,
    BadgeFormComponent,
    ButtonComponent,
    EventViewComponent,
    EventAddBadgesComponent,
    NavBreadcrumbComponent,
    CommentsComponent,
    BadgesComponent,
    FriendsComponent,
    ArrowComponent,
    BackButtonComponent,
    EventFriendsComponent,
    EventAcceptComponent,
    CheckoutFormComponent,
    ProfileComponent,
    ProfileEditComponent,
    ProfileDropdownComponent,
    UpdatePasswordComponent,
    SubAccountComponent,
    SubAccountConfirmationComponent,
    VanityLinksComponent,
    PublicProfileComponent,
    UpdateNameComponent,
    NoAuthProfileComponent,
    ConfirmRegistrationComponent,
    CompanyFieldComponent,
    CompanyRegistrationComponent,
    AccountPendingComponent,
    OrganizationComponent,
    CompanyUserComponent,
    OrganizationProfileComponent,
    PublicEventComponent,
    ProfileUploadComponent,
    PrivacyPolicyComponent,
    ReceivedBadgeDialogComponent
  ],
  entryComponents: [
    BadgeChooseDialogComponent,
    BadgeUploadDialogComponent,
    FriendInviteDialogComponent,
    DialogServiceComponent,
    BadgePreviewDialogComponent,
    CompanyRegistrationComponent,
    ReceivedBadgeDialogComponent
  ],
  providers: [
    AsyncPipe,
    AuthService,
    AuthSocialService,
    BadgeUploadService,
    BadgeService,
    MessageService,
    NotificationService,
    FriendService,
    DialogService,
    BadgePreviewService,
    EventService,
    SubAccountService,
    httpInterceptorProviders,
    ProfileToNavService,
    MessagingService,
    HttpErrorHandler,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: { siteKey: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI' } as RecaptchaSettings,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
