import { Component } from '@angular/core';

@Component({
  selector: 'page-not-found',
  template: '<mat-card class="card"><mat-card-content>Are you Lost?</mat-card-content></mat-card>',
  styles: [``]
})
export class PageNotFoundComponent {}
