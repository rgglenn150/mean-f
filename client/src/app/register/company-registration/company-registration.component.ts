import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { ProfanityFilterService } from 'src/app/utils/profanity-filter.service';
import { AuthService } from 'src/app/auth/auth.service';
import { OrganizationService } from 'src/app/organization/organization.service';

@Component({
  selector: 'app-company-registration',
  templateUrl: './company-registration.component.html',
  styleUrls: ['./company-registration.component.css']
})
export class CompanyRegistrationComponent implements OnInit {
  form;
  captcha: boolean;

  constructor(private auth: AuthService,public dialogRef: MatDialogRef<CompanyRegistrationComponent>,private formBuilder: FormBuilder ,
    private profanityFilter:ProfanityFilterService, private orgService:OrganizationService) { 
    this.form = formBuilder.group({
   
      orgName: ['', [Validators.required, this.isClean()]],
      officeAddress: ['', [Validators.required, this.isClean()]],
      dateEstablished: ['', [Validators.required, this.isClean()]],
      description: ['', [Validators.required, this.isClean()]],
      website: ['', [Validators.required, this.isClean()]],
      submittedBy: ['', [Validators.required, this.isClean()]],
      position: ['', [Validators.required, this.isClean()]],
      email: ['', [Validators.required, this.isClean()]],
      recaptchaReactive: [null, Validators.required]
    } )

  
  }

  ngOnInit() {
    
  }

  isClean() {
    return control => {
      return !this.profanityFilter.check(control.value) ? null : { hasProfanity: true }
    }

  }

  isValid(control) {
   
    return this.form.controls[control].invalid && this.form.controls[control].touched;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response ${captchaResponse}:`);
    this.captcha = captchaResponse ? true : false;

    
  }

  onSubmit() {
    if (this.form.valid) {
   
      this.orgService.register(this.form.value);
      this.dialogRef.close();
    } else {
      this.orgService.notif('Please fill out the form correctly and completely');
    }



  }

}
