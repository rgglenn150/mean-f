import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-company-user',
  templateUrl: './company-user.component.html',
  styleUrls: ['./company-user.component.css']
})
export class CompanyUserComponent implements OnInit {


  @Input() formGroup: FormGroup;
  @Output() selectedOrgId = new EventEmitter<string>();
  @Output() resolved = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }
  isValid(control) {
   
    return this.formGroup.controls[control].invalid && this.formGroup.controls[control].touched;
  }



}
