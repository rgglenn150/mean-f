import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
import { useAnimation } from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ProfanityFilterService } from '../utils/profanity-filter.service';
import { OrganizationService } from '../organization/organization.service';
import { async } from 'q';
import { EmailService } from '../utils/email.service';

@Component({
  moduleId: 'module.id',
  selector: 'register',
  templateUrl: 'register.component.html',
  styles: [`
    .error {
      background-color: #ff0f0;
    }
  `]
})
export class RegisterComponent {
  form;
  companyUserForm;
  destroySubject$: Subject<void> = new Subject();
  public formSent: boolean = false;
  captcha: boolean;

  showUserForm: boolean = true;
  selectedOrgId: string = '';
  token = undefined;


  constructor(private formBuilder: FormBuilder, private auth: AuthService, private route: ActivatedRoute, private profanityFilter: ProfanityFilterService,
    private orgService: OrganizationService, private emailUtils:EmailService) {
    this.form = formBuilder.group({
      firstName: ['', [Validators.required, this.isClean()]],
      lastName: ['', [Validators.required, this.isClean()]],
      email: ['', [Validators.required, emailValid(), this.isClean()]],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      recaptchaReactive: [null, Validators.required]

    }, { validator: matchingFields('password', 'confirmPassword') });


    this.companyUserForm = formBuilder.group({
      orgName: ['', [Validators.required, this.orgExists()]],
      orgId: [''],
      firstName: ['', [Validators.required, this.isClean()]],
      lastName: ['', [Validators.required, this.isClean()]],
      email: ['', [Validators.required, this.isNotCompanyEmail(), emailValid(), this.isClean()]],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      recaptchaReactive: [null, Validators.required]
    }, { validator: matchingFields('password', 'confirmPassword') });
    auth.goHomeIfAuthenticated();
  }

  ngOnInit() {
    this.route.url.pipe(takeUntil(this.destroySubject$)).subscribe(url => {
      this.token = url && url[1] && url[1].path ? url[1].path : '';
      this.auth.registerToken = this.token;
    });
  }

  isClean() {
    return control => {
      return !this.profanityFilter.check(control.value) ? null : { hasProfanity: true }
    }
  }

  isNotCompanyEmail(){
    return control => {
      return this.emailUtils.isCompanyEmail(control.value) ? null : { notCompanyEmail: true }
    }
  }
  

  orgExists() {
    return control => {
      return this.selectedOrgId !== undefined ? null : { doesNotExists: true }
    }
  }

  onSubmit() {
    if (this.form.valid) {
      if (this.token) {
        this.form.value.token = this.token;
      }


      this.auth.register(this.form.value);

      this.formSent = true;
    }
    else {
      this.auth.handleError2('Please fill out the form correctly and completely');
    }
  }


  onSubmitCompanyUser() {
    if (this.companyUserForm.valid) {
      if (this.token) {
        this.companyUserForm.value.token = this.token;
      }
      this.companyUserForm.value.orgId = this.selectedOrgId;


      this.auth.orgUserRegister(this.companyUserForm.value);

      this.formSent = true;
    } else {
      this.auth.handleError2('Please fill out the form correctly and completely');
    }
  }

  setOrgId(event) {
    this.selectedOrgId = event;
  }
  toggleForm() {
    this.showUserForm = !this.showUserForm;
  }

  isValid(control) {
    return this.form.controls[control].invalid && this.form.controls[control].touched;
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }

  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response ${captchaResponse}:`);
    this.captcha = captchaResponse ? true : false;

   
  }


}

function matchingFields(field1, field2) {
  return form => {
    if (form.controls[field1].value !== form.controls[field2].value)
      return { mismatchedFields: true }
  }
}


function emailValid() {
  return control => {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(control.value) ? null : { invalidEmail: true }
  }
}
