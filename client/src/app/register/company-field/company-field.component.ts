import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
import { startWith, map, takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { FormControlName, FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { CompanyRegistrationComponent } from '../company-registration/company-registration.component';
import { OrganizationService } from 'src/app/organization/organization.service';

@Component({
  selector: 'app-company-field',
  templateUrl: './company-field.component.html',
  styleUrls: ['./company-field.component.css']
})
export class CompanyFieldComponent implements OnInit {

  @Input() formGroup: FormGroup;
  //@Output() selectedOrgId:string;
  @Output() selectedOrgId = new EventEmitter<string>();
  @Output() orgs = new EventEmitter<any>();

  options: any[] = [{ orgName: 'None' }];
  filteredOptions: any = this.options;
  destroySubject$: Subject<void> = new Subject();
  displayInvite = false;

  constructor(public dialog: MatDialog, private orgService: OrganizationService) { }

  ngOnInit() {

    this.formGroup.valueChanges
      .pipe(takeUntil(this.destroySubject$)).subscribe(value => {
        startWith('')
        this.orgService.searchOrg("orgName", value.orgName).subscribe(res => {
          this.filteredOptions = res.length > 0 ? res : [{ orgName: 'None' }];
        });
        //  this.filteredOptions = this._filter(value.company)
      });
  }

  private _filter(value: any): string[] {
    const filterValue = value;
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  displayInviteForm() {

    const dialogRef = this.dialog.open(CompanyRegistrationComponent, {
      width: '385px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  setOrg(org) {
    
    this.selectedOrgId.emit(org);
  }



  isValid(control) {
    return this.formGroup.controls[control].invalid && this.formGroup.controls[control].touched;
  }


}
