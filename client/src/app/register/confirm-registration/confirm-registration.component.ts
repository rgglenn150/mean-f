import { Component, OnInit, ɵConsole } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { Subject } from 'rxjs';
import { takeUntil, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-confirm-registration',
  templateUrl: './confirm-registration.component.html',
  styleUrls: ['./confirm-registration.component.css']
})
export class ConfirmRegistrationComponent implements OnInit {
  confirmationToken: string = undefined;
  message: string;
  destroySubject$: Subject<void> = new Subject();
  confirmed:boolean;
  constructor(private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router) {

  }

  ngOnInit() {

    this.route.url.pipe(takeUntil(this.destroySubject$)).subscribe(url => {
      this.confirmationToken = url && url[2] && url[2].path ? url[2].path : '';

    });

    this.confirmAccount();
  }

  confirmAccount() {

    this.authService.confirmAccount(this.confirmationToken)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(res => {
        if (this.confirmationToken && this.confirmationToken !== null &&
          res) {
          this.message = "Success!";
          this.confirmed= true;
          setTimeout(() => {
            this.router.navigateByUrl('/');
          }, 5000);

        } else {
          this.confirmed = false;
          this.message = "Failed!";
        }




      }, err => {
        this.message = err.error.message;
      });

  }
}
