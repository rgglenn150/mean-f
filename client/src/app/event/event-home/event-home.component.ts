import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Event } from '../event';
import { EventService } from '../event-service/event-service.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { MessageService } from '../../utils/message.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { NONE_TYPE } from '@angular/compiler/src/output/output_ast';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-event-home',
  templateUrl: './event-home.component.html',
  styleUrls: ['./event-home.component.css'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        display:'block',
        opacity: 1,
        backgroundColor: 'white'
      })),
      state('closed', style({
        height: '0px',
        opacity: 0,
        backgroundColor: 'white'
      })),
      transition('open => closed', [
        animate('0.3s')
      ]),
      transition('closed => open', [
        animate('0.3s')
      ])
    ]),
  ]
})
export class EventHomeComponent implements OnInit, OnDestroy {

  public events$: Observable<Event[]>;
  destroySubject$: Subject<void> = new Subject();
  searchText;
  advancedSearchVisible = false;
  searchByDateFrom:string;
  


  constructor(
    private eventService: EventService,
    private router: Router,
    private messageService: MessageService,
    private auth: AuthService
  ) {

    if (!auth.authenticatedUser) {
      router.navigate(['/events/public']);
    }

   }

  ngOnInit() {
    this.events$ = this.eventService.getEvents();
  }

  createAndEdit() {
    const newEvent: Event = {
      _id: undefined,
      name: '',
      description: '',
      company: undefined,
      user: undefined,
      recurrence: 'just once',
      timeFrame: 1,
      rulesets: '',
      makePrivate: false,
      turnCommentsOff: false,
      turnEmailNotificationsOff: false,
      image: '',
      category: '',
      settings: {},
      status: 'draft',
      participants: 0,
      promoters: 0
    };
    this.eventService.createEvent(newEvent).pipe(takeUntil(this.destroySubject$)).subscribe((event) => {
      if (event._id) {
        this.router.navigateByUrl('/events/' + event._id);
      } else {
        this.messageService.add('Something went wrong with creating an event!');
      }

    });
  }


  searchHandler(value) {

    if (value) {
      this.events$ = this.eventService.searchEvents("name", value);
    } else {
      this.events$ = this.eventService.getEvents();
    }
  }

  searchDateFrom(value) {
    console.log(value)
    if (value) {
      this.events$ = this.eventService.searchEvents("date", value);
    } else {
      this.events$ = this.eventService.getEvents();
    }
   
  }

  /* setDateTo(value) {
    this.dateTo = value;
    this.searchByDateHandler(this.dateFrom,this.dateTo);
    
  } */



  ngOnDestroy() {
    this.destroySubject$.next();
  }

}
