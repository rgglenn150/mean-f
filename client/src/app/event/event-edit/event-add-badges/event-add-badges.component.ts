import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Event} from '../../event';
import {takeUntil} from 'rxjs/operators';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {BadgePreviewService} from '../../../badge/badge-create/badge-preview/badge-preview.service';
import {MessageService} from '../../../utils/message.service';
import {EventService} from '../../event-service/event-service.service';
import {DialogService} from '../../../utils/dialog-service/dialog.service';
import {BadgeService} from '../../../badge/badge.service';
import {Subject} from 'rxjs';

/*

<event-add-badges [parentForm]="form" (formSaved)='handleFormSaved($event)'></event-add-badges>

 */

@Component({
  selector: 'event-add-badges',
  templateUrl: './event-add-badges.component.html',
  styleUrls: ['./event-add-badges.component.css']
})
export class EventAddBadgesComponent implements OnInit {

  @Input() parentForm: FormGroup;
  @Output() formSaved = new EventEmitter();
  saving = false;
  selected = [false, false, false];
  destroySubject$: Subject<void> = new Subject();
  
  constructor(
    private  badgePreviewService: BadgePreviewService,
    private messageService: MessageService,
    private eventService: EventService,
    private dialogService: DialogService,
    private badgeService: BadgeService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.saving = false;
  }

  addBadgeImages(badges) {
    const promises: Array<any> = badges.map((badge, i) => {
      return this.badgePreviewService.getCanvasUrl('badge-canvas' + i).then((imageUrl) => {
        badge.image = imageUrl;
        return badge;
      });
    });
    return Promise.all(promises);
  }

  onSubmitSecondForm() {
    if (this.parentForm.valid && !this.saving) {
      this.saving = true;
      const editEvent: Event = this.parentForm.value as Event;
      if (editEvent.badges.length > 0) {
        return this.addBadgeImages(editEvent.badges).then((badges) => {
          this.parentForm.value.badges = badges;
          this.saving = false;
          this.formSaved.emit(badges);
        }).catch(() => {
          this.saving = false;
          this.messageService.add('Oops.. something went wrong');
        });
      } else {
        this.saving = false;
        this.formSaved.emit([]);
      }
    } else {
      this.messageService.add('Please fill out the required fields in the form correctly');
    }
  }

  markSelected(index) {
    this.selected.forEach((selected, i) => {
      this.selected[i] = false;
    });
    this.selected[index] = true;
  }

  applyOneWinner() {
    this.markSelected(0);
    this.parentForm = this.formBuilder.group({
      badges: this.formBuilder.array([
        this.badgeService.getFormGroup(),
        this.badgeService.getFormGroup()
      ])
    });
    this.parentForm.controls['badges']['controls'][0].patchValue(this.badgeService.getFirst());
    this.parentForm.controls['badges']['controls'][1].patchValue(this.badgeService.getParticipant());
  }

  oneWinner() {
    if (this.parentForm.value.badges && this.parentForm.value.badges.length > 0) {
      this.dialogService.openDialog('Are you sure you want to change the badges? Doing this action will remove all your current progress for the badges you are currently working on. This action cannot be undone.', 'Change Badge Type').pipe(takeUntil(this.destroySubject$)).subscribe((result) => {
        if (result) {
          this.applyOneWinner();
        }
      });
    } else {
      this.applyOneWinner();
    }
  }

  applyMultipleWinners() {
    this.markSelected(1);
    this.parentForm = this.formBuilder.group({
      badges: this.formBuilder.array([
        this.badgeService.getFormGroup(),
        this.badgeService.getFormGroup(),
        this.badgeService.getFormGroup(),
        this.badgeService.getFormGroup()
      ])
    });
    this.parentForm.controls['badges']['controls'][0].patchValue(this.badgeService.getFirst());
    this.parentForm.controls['badges']['controls'][1].patchValue(this.badgeService.getSecond());
    this.parentForm.controls['badges']['controls'][2].patchValue(this.badgeService.getThird());
    this.parentForm.controls['badges']['controls'][3].patchValue(this.badgeService.getParticipant());
  }

  multipleWinner() {
    if (this.parentForm.value.badges && this.parentForm.value.badges.length > 0) {
      this.dialogService.openDialog('Are you sure you want to change the badges? Doing this action will remove all your current progress for the badges you are currently working on. This action cannot be undone.', 'Change Badge Type').pipe(takeUntil(this.destroySubject$)).subscribe((result) => {
        if (result) {
          this.applyMultipleWinners();
        }
      });
    }else{
      this.applyMultipleWinners();
    }
  }

  applyParticipation() {
    this.markSelected(2);
    this.parentForm = this.formBuilder.group({
      badges: this.formBuilder.array([
        this.badgeService.getFormGroup()
      ])
    });
    this.parentForm.controls['badges']['controls'][0].patchValue(this.badgeService.getParticipant());
  }

  participation() {
    if (this.parentForm.value.badges && this.parentForm.value.badges.length > 0) {
      this.dialogService.openDialog('Are you sure you want to change the badges? Doing this action will remove all your current progress for the badges you are currently working on. This action cannot be undone.', 'Change Badge Type').pipe(takeUntil(this.destroySubject$)).subscribe((result) => {
        if (result) {
          this.applyParticipation();
        }
      });
    } else {
      this.applyParticipation();
    }
  }
  
  /*
  get badges() {
    return this.parentForm.get('badges') as FormArray;
  }
   */

  handleBadgeSelection(event) {
    if (event) {
      const control = <FormArray>this.parentForm.controls['badges'];
      control.push(this.badgeService.getFormGroup());
      this.parentForm.controls['badges']['controls'][control.length - 1].patchValue(event);
      this.messageService.add('Successfully added badge');
    }
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }

}
