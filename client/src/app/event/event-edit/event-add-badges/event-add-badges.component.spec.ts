import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventAddBadgesComponent } from './event-add-badges.component';

describe('EventAddBadgesComponent', () => {
  let component: EventAddBadgesComponent;
  let fixture: ComponentFixture<EventAddBadgesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventAddBadgesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventAddBadgesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
