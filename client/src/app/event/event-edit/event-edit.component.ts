import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {from, Observable, Subject} from 'rxjs';
import {EventService} from '../event-service/event-service.service';
import {Event} from '../event';
import {concatMap, flatMap, map, mergeMap, takeUntil, tap} from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from '../../utils/message.service';
import {BadgePreviewService} from '../../badge/badge-create/badge-preview/badge-preview.service';
import {BadgeService} from '../../badge/badge.service';
import {DialogService} from '../../utils/dialog-service/dialog.service';
import {Badge} from '../../badge/badge';
import { ProfanityFilterService } from 'src/app/utils/profanity-filter.service';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-event-edit',
  templateUrl: './event-edit.component.html',
  styleUrls: ['./event-edit.component.css']
})
export class EventEditComponent implements OnInit, OnDestroy {
  form: FormGroup;
  secondFormGroup: FormGroup;
  event$: Observable<Event>;
  destroySubject$: Subject<void> = new Subject();
  saving: boolean;
  selectedIndex = 0;
  cachedEventImage = '';
  participants: Array<any> = [];
  promoters: Array<any> = [];
  participantsDisabled: Array<any> = [];
  promotersDisabled: Array<any> = [];
  badges: Badge[];
  userLoggedIn:any;

  settings = {
      bgColor: '#f27360',
      text: {
        color: '#485868',
        font: 'Troche',
        width: 200,
        height: 140,
        fontSize: 50,
        position: { x: 70, y: 43 },
        name: 'Event Name'
      },
      image: {
        path: '/assets/img/badges/badge-23.png',
        width: 257,
        height: 230,
        position: {x: 40, y: 10}
      }
    };

  constructor(
    private formBuilder: FormBuilder,
    private eventService: EventService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private badgePreviewService: BadgePreviewService,
    private badgeService: BadgeService,
    private dialogService: DialogService,
    private router: Router,
    private profanityFilter:ProfanityFilterService,
    private auth:AuthService
  ) {

    this.userLoggedIn= auth.authenticatedUser;
  }

  getBlankForm (fbBadges = [], fbBadgesCached = []) {
    return this.formBuilder.group({
      _id: [''],
      name: ['',[ Validators.required,this.isClean()]],
      location:['',[ Validators.required,this.isClean()]],
      description: ['', [Validators.required,this.isClean()]],
      makePrivate: [false],
      frequency: [''],
      turnCommentsOff: [false],
      turnEmailNotificationsOff: [false],
      timeFrame: ['', [Validators.min(1), Validators.max(365)]],
      makeLiveDate: [''],
      launchDate: [''],
      expireDate: [''],
      recurrence: [''],
      status: [''],
      image: [''],
      category: ['', [Validators.required]],
      badges: this.formBuilder.array(fbBadges),
      badgesCached: this.formBuilder.array(fbBadgesCached)
    });
  }
  getBlankForm2(formBuilderArrayBadgesCached = []) {
    return this.formBuilder.group({
      badges: this.formBuilder.array(formBuilderArrayBadgesCached)
    });
  }


  isClean(){
    return control => {
      return !this.profanityFilter.check(control.value) ? null : { hasProfanity: true }
    }
  
  }

  /*addBadges(badges){
    const control = <FormArray>this.form.controls['badges'];
    for (let i in control){

    }
    control.push(this.formBuilder.control([]));
    this.form.controls['badges']['controls'][control.length - 1].patchValue(badge);
  }*/

  getParticipants(eventId) {
      this.eventService.getParticipants(eventId, ['participant'])
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((p) => {
        console.log(p)
        // For participants that still need an invite sent out
        this.participants = p.filter(e => !e.inviteSent).map(e => e.participant._id);
        // For participants that have been sent and invite or accepted to participate in the event
        this.participantsDisabled = p.filter(e => e.inviteSent).map(e => e.participant);
      });
  }
  getPromoters(eventId) {
    this.eventService.getPromoters(eventId, ['promoter'])
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((p) => {
        // For promoters that still need an invite sent out
        this.promoters = p.filter(e => !e.inviteSent).map(e => e.promoter._id);
        // For promoters that have been sent and invite or accepted to participate in the event
        this.promotersDisabled = p.filter(e => e.inviteSent).map(e => e.promoter);
      });
  }

  ngOnInit() {
    this.form = this.getBlankForm();

    this.secondFormGroup = this.getBlankForm2();

    this.route.url.pipe(takeUntil(this.destroySubject$)).subscribe(url => {
      if (url && url[1] && url[1].path) {
        this.getParticipants(url[1].path);
        this.getPromoters(url[1].path);
        this.event$ = this.eventService.getEvent(url[1].path, ['badges']).pipe(
          tap(data => {
            if (data) {
             
              if (data.user !== this.userLoggedIn._id) {
               
                this.router.navigate(['/events/view/'+data._id]);
              }
           
              this.settings = Object.assign(this.settings, data.settings);
              const fbBadges = data.badges.map(b => this.formBuilder.control([]));
              const fbBadgesCached = data.badgesCached.map(b => this.badgeService.getFormGroup());
              this.secondFormGroup = this.getBlankForm2(fbBadgesCached);
              this.secondFormGroup.patchValue(data);
              this.form = this.getBlankForm(fbBadges, fbBadgesCached);
              this.badges = data.badges;
              data.badges = data.badges.map(b => b._id);
              this.form.patchValue(data);
            }
          })
        );
      }
    });
  }

  selectionChanged(event) {
    /*if(this.selectedIndex === 0) {

    }*/
    this.selectedIndex = event.selectedIndex;
  }

  formSavedStep2(badgesCached) {
    this.form.value.badgesCached = badgesCached;
    const editEventStep1: Event = this.form.value as Event;
    this.eventService.updateEvent(editEventStep1).pipe(takeUntil(this.destroySubject$)).subscribe(event => {
      this.selectedIndex += 1;
      this.saving = false;
    }, error => this.saving = false);
  }

  onSubmit() {
    this.form.get('category').markAsTouched();
    if (this.form.valid) {
      this.saving = true;
      const editEvent: Event = this.form.value as Event;
      return this.badgePreviewService.getImageUrl('badge-canvas').then((image) => {
        editEvent.settings = this.settings;
        this.eventService.updateEvent(editEvent).pipe(takeUntil(this.destroySubject$)).subscribe(event => {
          // this.messageService.add('Event Saved Successfully');
          this.cachedEventImage = image;
          this.saving = false;
          this.selectedIndex += 1;
        });
      }).catch((e) => {
        console.error(e)
        this.saving = false;
        this.messageService.add('Oops.. something went wrong');
      });
    } else {
      this.messageService.add('Please fill out the required fields in the form correctly');
    }
  }

  makeLiveUpdateEvent(editEventStep1) {
    editEventStep1.image = this.cachedEventImage;
    editEventStep1.badgesCached = [];
    editEventStep1.status = 'active';
    editEventStep1.makeLiveDate = new Date();
    editEventStep1.launchDate = new Date();
    return this.eventService.makeLive(editEventStep1);
  }

  createBadges(subscriptions, editEventStep1){
    return forkJoin(subscriptions).pipe(
      flatMap((myBadges) => {
        editEventStep1.badges = myBadges.map(b => b['_id']);
        return this.makeLiveUpdateEvent(editEventStep1);
      })
    );
  }

  makeLiveForm() {
    const editEventStep1: Event = this.form.value as Event;
    const subscriptions: Array<any> = editEventStep1.badgesCached.map((badge, i) => {
      return this.badgeService.create(badge);
    });
    // First case is if the event needs to create badges, else just create without any badges and update the event.
    return subscriptions.length > 0 ? this.createBadges(subscriptions, editEventStep1) : this.makeLiveUpdateEvent(editEventStep1);
  }

  onSubmitThirdForm() {
    const msg = this.form.value.status === 'active' ?
      'Are you sure you want to save this event? Any additional badges added to this event cannot be modified. This action cannot be undone.' :
      'Are you sure you want to make live this event? You will not be able to modify the badges that are created with this event. This action cannot be undone.';
    const buttonMsg = this.form.value.status === 'active' ? 'Save' : 'Make Live';
    this.dialogService.openDialog(msg, buttonMsg)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((result) => {
        if (result) {
          this.saving = true;
          this.makeLiveForm().pipe(takeUntil(this.destroySubject$)).subscribe(() => {
            this.saving = false;
            this.router.navigateByUrl('/events');
          }, error => {
            console.log(error);
            this.saving = false;
            this.messageService.add('Oops.. something went wrong');
          });
        }
    });
  }

  getInviteSubscription(friend, type = 'participant') {
      if (friend.state === 'add' && type === 'participant') {
        return this.eventService.addParticipant(this.form.value._id, friend._id);
      } else if (friend.state === 'add' && type === 'promoter') {
        return this.eventService.addPromoter(this.form.value._id, friend._id);
      } else if (friend.state === 'remove' && type === 'participant') {
        return this.eventService.removeParticipant(this.form.value._id, friend._id);
      } else { // if (friend.state === 'remove' && type === 'promoter') {
        return this.eventService.removePromoter(this.form.value._id, friend._id);
      }
  }

  editParticipants(friend) {
    this.getInviteSubscription(friend, 'participant').pipe(takeUntil(this.destroySubject$)).subscribe();
  }
  editPromoters(friend) {
    this.getInviteSubscription(friend, 'promoter').pipe(takeUntil(this.destroySubject$)).subscribe();
  }

  isValid(control) {
    return this.form.controls[control].invalid && this.form.controls[control].touched;
  }
  ngOnDestroy() {
    this.destroySubject$.next();
  }

}
