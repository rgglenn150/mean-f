import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'event-category',
  templateUrl: './event-category.component.html',
  styleUrls: ['./event-category.component.css']
})
export class EventCategoryComponent implements OnInit {

  @Input() parentForm: FormGroup;
  @Input() formTemplate;
  constructor() { }

  categories = ['Retail', 'Sports', 'Family', 'Fraternity', 'School'];

  ngOnInit() { }

  isValid(control) {
    return this.parentForm.controls[control].invalid;
  }
}
