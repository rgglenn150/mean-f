import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'recurrence',
  templateUrl: './recurrence.component.html',
  styleUrls: ['./recurrence.component.css']
})
export class RecurrenceComponent implements OnInit {

  @Input() parentForm: FormGroup;
  recurrences = [];
  constructor() { }

  ngOnInit() {
    this.recurrences = ['just once', 'daily', 'weekly', 'monthly', 'yearly'];
  }
}
