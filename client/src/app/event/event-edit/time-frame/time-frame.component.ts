import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'time-frame',
  templateUrl: './time-frame.component.html',
  styleUrls: ['./time-frame.component.css']
})
export class TimeFrameComponent implements OnInit {

  @Input() parentForm: FormGroup;
  //timeFrames = [];
  constructor() { }

  ngOnInit() {
    //this.timeFrames = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
  }

  isValid(control) {
    return this.parentForm.controls[control].invalid && this.parentForm.controls[control].dirty;
  }

}
