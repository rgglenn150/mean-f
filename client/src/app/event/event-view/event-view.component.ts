import { Component, OnInit } from '@angular/core';
import {EventService} from '../event-service/event-service.service';
import {from, Subject} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {map, takeUntil, tap} from 'rxjs/operators';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-event-view',
  templateUrl: './event-view.component.html',
  styleUrls: ['./event-view.component.css']
})
export class EventViewComponent implements OnInit {

  destroySubject$: Subject<void> = new Subject();
  events$;
  badges$;
  badges;
  participants$;
  promoters$;
  participantInvite;
  promoterInvite;

  constructor(
    private eventService: EventService,
    private route: ActivatedRoute,
    private authService: AuthService
  ) {
  }

  ngOnInit() {
    this.events$ = this.authService ? this.eventService.getEvents() : this.eventService.getPublicEvents();
    let userId = this.authService.userId;
    // this.events$ = this.eventService.getEvents();
    this.route.url.pipe(takeUntil(this.destroySubject$)).subscribe(url => {
      if (url && url[2] && url[2].path) {
        this.participants$ = this.eventService.getParticipants(url[2].path, ['participant']).pipe(
          tap((f) => {
            this.participantInvite = f.filter(e => e.inviteSent && e.participant._id === userId);
          }),
          map(f => f.filter(e => e.accepted).map(e => e.participant))
        );
        this.promoters$ = this.eventService.getPromoters(url[2].path, ['promoter']).pipe(
          tap((f) => {
            this.promoterInvite = f.filter(e => e.inviteSent && e.promoter._id === userId);
          }),
          map(f => f.filter(e => e.accepted).map(e => e.promoter))
        );
        this.events$ = this.eventService.getEvent(url[2].path, ['badges', 'user']).pipe(
          tap(data => {
            //console.log(data.badges)
            this.badges$ = from(data.badges);
            this.badges = data.badges;
          }),
          map(data => {
            return [data];
          })
        );
      }
    });
  }
  ngOnDestroy() {
    this.destroySubject$.next();
  }
}
