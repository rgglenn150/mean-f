import { Injectable } from '@angular/core';
import { HandleError, HttpErrorHandler } from '../../http-error-handler.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Event } from '../event';
import { catchError, filter } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { any } from 'codelyzer/util/function';
import { RequestOptions, ResponseContentType } from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class EventService {
  BASE_URL = environment.protocal + environment.host + '/api/event';
  private handleError: HandleError;

  constructor(
    private http: HttpClient, private httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('EventService');
  }
  getEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(this.BASE_URL + '/')
      .pipe(
        catchError(this.handleError('getEvents', []))
      );
    // Add safe, URL encoded search parameter if there is a search term
    // const options = params: new HttpParams().set('symbol', symbol)} : {};
  }

  getPublicEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(this.BASE_URL + '/public')
      .pipe(
        catchError(this.handleError('getPublicEvents', []))
      );
    // Add safe, URL encoded search parameter if there is a search term
    // const options = params: new HttpParams().set('symbol', symbol)} : {};
  }

  searchEvents(field: string, search: string): Observable<Event[]> {
    const queryParams = field && search ? '?field=' + field + '&search=' + search : '';
    return this.http.get<Event[]>(this.BASE_URL + '/search' + queryParams)
      .pipe(
        catchError(this.handleError('getEvents', []))
      );
  }

  getEvent(id, populate = []): Observable<any> {
    const queryParams = populate.length > 0 ? '?populate=' + populate : '';
    const url = this.BASE_URL + '/' + id + queryParams;
    return this.http.get<any>(url)
      .pipe(
        catchError(this.handleError('getEvent'))
      );
    // Add safe, URL encoded search parameter if there is a search term
    // const options = params: new HttpParams().set('symbol', symbol)} : {};
  }
  createEvent(event: Event): Observable<Event> {
    return this.http.post<Event>(this.BASE_URL + '/', event)
      .pipe(
        catchError(this.handleError('create', event))
      );
  }
  updateEvent(event: Event): Observable<Event> {
    const eventClone = JSON.parse(JSON.stringify(event));
    if (eventClone.badgesCached && eventClone.badgesCached.length > 0) {
      eventClone.badgesCached = eventClone.badgesCached.map((b) => { delete b.image; return b; });
    }
    return this.http.put<Event>(this.BASE_URL + '/' + event._id, eventClone)
      .pipe(
        catchError(this.handleError('update', event))
      );
  }
  makeLive(event: Event): Observable<Event> {
    return this.http.put<Event>(this.BASE_URL + '/makeLive/' + event._id, event)
      .pipe(
        catchError(this.handleError('makeLive', event))
      );
  }
  getParticipants(eventId, populate = []) {
    const queryParams = populate.length > 0 ? '?populate=' + populate : '';
    const options = queryParams ?
      { params: new HttpParams().set('event', eventId).set('populate', populate.toString()) } :
      { params: new HttpParams().set('event', eventId) };
    return this.http.get<any>(this.BASE_URL + '/participant', options)
      .pipe(
        catchError(this.handleError('getEventParticipants'))
      );
  }
  addParticipant(eventId, participantId) {
    return this.http.post(this.BASE_URL + '/participant', {
      event: eventId,
      participant: participantId
    }).pipe(
      catchError(this.handleError('addParticipant'))
    );
  }
  updateParticipant(body, participantId) {
    return this.http.put(this.BASE_URL + '/participant/' + participantId, body).pipe(
      catchError(this.handleError('updateParticipant'))
    );
  }
  removeParticipant(eventId, participantId) {
    const options = {
      params: new HttpParams().set('event', eventId).set('participant', participantId),
      headers: new HttpHeaders({
        'Content-Type': 'text/plain'
      }),
      responseType: 'text' as 'json'
    };
    return this.http.delete(this.BASE_URL + '/participant', options).pipe(
      catchError(this.handleError('removeParticipant'))
    );
  }
  
  getPromoters(eventId, populate = []) {
    const queryParams = populate.length > 0 ? '?populate=' + populate : '';
    const options = queryParams ?
      { params: new HttpParams().set('event', eventId).set('populate', populate.toString()) } :
      { params: new HttpParams().set('event', eventId) };
    return this.http.get<any>(this.BASE_URL + '/promoter', options)
      .pipe(
        catchError(this.handleError('getEventPromoters'))
      );
  }
  addPromoter(eventId, promoterId) {
    return this.http.post(this.BASE_URL + '/promoter', {
      event: eventId,
      promoter: promoterId
    }).pipe(
      catchError(this.handleError('addPromoters'))
    );
  }
  updatePromoter(body, participantId) {
    return this.http.put(this.BASE_URL + '/promoter/' + participantId, body).pipe(
      catchError(this.handleError('updateParticipant'))
    );
  }
  removePromoter(eventId, promoterId) {
    const options = {
      params: new HttpParams().set('event', eventId).set('promoter', promoterId),
      headers: new HttpHeaders({
        'Content-Type': 'text/plain'
      }),
      responseType: 'text' as 'json'
    };
    return this.http.delete(this.BASE_URL + '/promoter', options).pipe(
      catchError(this.handleError('removePromoters'))
    );
  }
  getInvites(eventId, populate = []) {
    const queryParams = populate.length > 0 ? '?populate=' + populate : '';
    const options = queryParams ?
      { params: new HttpParams().set('event', eventId).set('populate', populate.toString()) } :
      { params: new HttpParams().set('event', eventId) };
    return this.http.get<any>(this.BASE_URL + '/invite', options)
      .pipe(
        catchError(this.handleError('getEventInvites'))
      );
  }
  addInvite(eventId, userId, type = 'participant') {
    return this.http.post(this.BASE_URL + '/invite', {
      event: eventId,
      user: userId,
      type: type
    }).pipe(
      catchError(this.handleError('AddEventInvites'))
    );
  }
  removeInvite(eventId, userId) {
    const options = {
      params: new HttpParams().set('event', eventId).set('user', userId),
      headers: new HttpHeaders({
        'Content-Type': 'text/plain'
      }),
      responseType: 'text' as 'json'
    };
    return this.http.delete(this.BASE_URL + '/invite', options).pipe(
      catchError(this.handleError('removeInvite'))
    );
  }
}
