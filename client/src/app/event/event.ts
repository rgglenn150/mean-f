export interface Event {
  _id: string;
  name: string;
  description: string;
  company: string;
  user: string;
  recurrence: string;
  timeFrame: number;
  rulesets: string;
  created?: Date;
  launchDate?: Date;
  expireDate?: Date;
  makePrivate: boolean;
  turnCommentsOff: boolean;
  turnEmailNotificationsOff: boolean;
  image: string;
  settings: object;
  category: string;
  status: string;
  badges?: Array<any>;
  badgesCached?: Array<any>;
  participants: number;
  promoters: number;
}
