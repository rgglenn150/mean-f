import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Observable, Subject } from 'rxjs';
import { EventService } from '../event-service/event-service.service';
import { Event } from '../event';

@Component({
  selector: 'app-public-event',
  templateUrl: './public-event.component.html',
  styleUrls: ['./public-event.component.css'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        display:'block',
        opacity: 1,
        backgroundColor: 'white'
      })),
      state('closed', style({
        height: '0px',
        opacity: 0,
        backgroundColor: 'white'
      })),
      transition('open => closed', [
        animate('0.3s')
      ]),
      transition('closed => open', [
        animate('0.3s')
      ])
    ]),
  ]
})
export class PublicEventComponent implements OnInit {




  destroySubject$: Subject<void> = new Subject();
  searchText;
  advancedSearchVisible = false;
  searchByDateFrom:string;
  events$: Observable<Event[]>;


  constructor(public auth: AuthService, private router: Router,  private eventService: EventService) {
 
    if (auth.authenticatedUser) {
      router.navigate(['/events']);
    }
  }

  ngOnInit() {
    this.events$ = this.eventService.getPublicEvents()
  }

  searchHandler(value) {

    if (value) {
      this.events$ = this.eventService.searchEvents("name", value);
    } else {
      this.events$ = this.eventService.getPublicEvents();
    }
  }

  searchDateFrom(value) {
    console.log(value)
    if (value) {
      this.events$ = this.eventService.searchEvents("date", value);
    } else {
      this.events$ = this.eventService.getPublicEvents();
    }
   
  }

  ngOnDestroy() {
    this.destroySubject$.next();
  }

}
