import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventAcceptComponent } from './event-accept.component';

describe('EventAcceptComponent', () => {
  let component: EventAcceptComponent;
  let fixture: ComponentFixture<EventAcceptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventAcceptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventAcceptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
