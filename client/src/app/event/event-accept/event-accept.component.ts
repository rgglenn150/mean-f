import {Component, Input, OnInit} from '@angular/core';
import {EventService} from '../event-service/event-service.service';

@Component({
  selector: 'app-event-accept',
  templateUrl: './event-accept.component.html',
  styleUrls: ['./event-accept.component.css']
})
export class EventAcceptComponent implements OnInit {

  @Input() participant; // pass in a participant or promoter from db.

  constructor(
    private eventService: EventService
  ) {}

  updateEventParticipant(accepted) {
    // If a participant was passed in, then run participant code, else run the promoter code.
    if (this.participant.participant) {
      this.eventService.updateParticipant({accepted: accepted}, this.participant._id).subscribe(e => this.participant = e);
    } else if (this.participant.promoter) {
      this.eventService.updatePromoter({accepted: accepted}, this.participant._id).subscribe(e => this.participant = e);
    }
  }

  going() {
    this.updateEventParticipant(true);
  }
  notGoing() {
    this.updateEventParticipant(false);
  }

  ngOnInit() {
  }

}
