import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import {BadgeService} from '../badge/badge.service';
import {Badge} from '../badge/badge';

@Component({
  selector: 'recent',
  templateUrl: './recent.component.html',
  styleUrls: ['./recent.component.css']
})
export class RecentComponent {
  constructor(private auth: AuthService, private badgeService: BadgeService) {}

  badges: Badge[];

  ngOnInit() {

  }
}
