import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user.component';
import { LinkedinComponent } from './login/linkedin.component';
import { PageNotFoundComponent } from './page-not-found.component';
import { BadgeCreateComponent } from './badge/badge-create/badge-create/badge-create.component';
import { BadgeSelectComponent } from './badge/badge-select/badge-select.component';
import { ForgotPasswordComponent } from './login/forgot-password/forgot-password.component';
import { RecoverPasswordComponent } from './login/recover-password/recover-password.component';
import { GiveComponent } from './give/give.component';
import { RecentComponent } from './recent/recent.component';
import { FriendComponent } from './friend/friend.component';
import { GlobalComponent } from './global/global.component';
import { FriendProfileComponent } from './friend/friend-profile/friend-profile.component';
import { CampaignHomeComponent } from './campaign/campaign-home/campaign-home.component';
import { EventHomeComponent } from './event/event-home/event-home.component';
import { EventEditComponent } from './event/event-edit/event-edit.component';
import { BadgeDesignerComponent } from './badge/badge-designer/badge-designer/badge-designer.component';
import { EventViewComponent } from './event/event-view/event-view.component';
import { CheckoutComponent } from './artist/checkout/checkout.component';
import { ProfileEditComponent } from "./profile/profile-edit/profile-edit.component";
import { SubAccountConfirmationComponent } from './sub-account/sub-account-confirmation/sub-account-confirmation.component';
import { PublicProfileComponent } from './profile/public-profile/public-profile.component';
import { NoAuthProfileComponent } from './profile/no-auth-profile/no-auth-profile.component';
import { ConfirmRegistrationComponent } from './register/confirm-registration/confirm-registration.component';
import { UserGuard } from './auth/user.guard';
import { AccountPendingComponent } from './profile/account-pending/account-pending.component';
import { OrganizationComponent } from './organization/organization.component';
import { OrganizationProfileComponent } from './organization/organization-profile/organization-profile.component';
import { PublicEventComponent } from './event/public-event/public-event.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

export const routes = [{
  path: 'me',
  component: HomeComponent,
  canActivate: [UserGuard]
},
{
  path: 'privacy-policy',
  component: PrivacyPolicyComponent
},
{
  path: 'organization',
  component: OrganizationComponent,
  canActivate: [UserGuard]
},
{
  path: 'organization/:_id',
  component: OrganizationProfileComponent,
  canActivate: [UserGuard]
},
{
  path: 'campaign',
  component: CampaignHomeComponent,
  canActivate: [UserGuard]
}, {
  path: 'badge-designer/:_id',
  component: BadgeDesignerComponent,
  canActivate: [UserGuard]
}, {
  path: 'events',
  component: EventHomeComponent,
  canActivate: [UserGuard]
},
{
  path: 'events/public',
  component: PublicEventComponent
},
{
  path: 'events/:_id',
  component: EventEditComponent,
  canActivate: [UserGuard]
},
{
  path: 'events/view/:_id',
  component: EventViewComponent

}, {
  path: 'create',
  component: BadgeSelectComponent,
  canActivate: [UserGuard]
}, {
  path: 'create/:_id',
  component: BadgeCreateComponent,
  canActivate: [UserGuard]
}, {
  path: 'checkout',
  component: CheckoutComponent,
  canActivate: [UserGuard]
}, {
  path: 'register',
  component: RegisterComponent
}, {
  path: 'register/:token',
  component: RegisterComponent
},
{
  path: 'sub-account/confirm/:confirmationToken',
  component: SubAccountConfirmationComponent
},
{
  path: 'account/confirm/:confirmationToken',
  component: ConfirmRegistrationComponent
}
  , {
  path: 'login',
  component: LoginComponent
}, {
  path: 'forgotpassword',
  component: ForgotPasswordComponent
}, {
  path: 'recoverpassword/:token',
  component: RecoverPasswordComponent
}, {
  path: 'give',
  component: GiveComponent,
  canActivate: [UserGuard]
}, {
  path: 'friends',
  component: FriendComponent,
  canActivate: [UserGuard]
}, {
  path: 'friends/:id',
  component: FriendProfileComponent,
  canActivate: [UserGuard]
}, {
  path: 'recent',
  component: RecentComponent,
  canActivate: [UserGuard]
}, {
  path: 'global',
  component: GlobalComponent
}, {
  path: 'profile',
  component: ProfileEditComponent,
  canActivate: [UserGuard]
}, {
  path: 'linkedin',
  component: LinkedinComponent
}, {
  path: '',
  redirectTo: '/me',
  pathMatch: 'full'
},
{
  path: ':username',
  component: PublicProfileComponent
},
{
  path: ':username/public',
  component: NoAuthProfileComponent
},
{
  path: 'account/pending',
  component: AccountPendingComponent
},
{
  path: '**',
  component: PageNotFoundComponent
}];
