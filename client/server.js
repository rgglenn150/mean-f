//Install express server
const express = require('express');
const path = require('path');

const app = express();

const angular_build_path = '/dist/badges-front-end';

// Serve only the static files form the dist directory
app.use(express.static(__dirname + angular_build_path));

app.get('/*', function(req,res) {

  res.sendFile(path.join(__dirname + angular_build_path + '/index.html'));
});

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 8080);
